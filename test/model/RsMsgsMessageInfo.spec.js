/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.RetroShareOpenApiWrapper);
  }
}(this, function(expect, RetroShareOpenApiWrapper) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('RsMsgsMessageInfo', function() {
    it('should create an instance of RsMsgsMessageInfo', function() {
      // uncomment below and update the code to test RsMsgsMessageInfo
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be.a(RetroShareOpenApiWrapper.RsMsgsMessageInfo);
    });

    it('should have the property msgId (base name: "msgId")', function() {
      // uncomment below and update the code to test the property msgId
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property rspeeridSrcId (base name: "rspeerid_srcId")', function() {
      // uncomment below and update the code to test the property rspeeridSrcId
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property rsgxsidSrcId (base name: "rsgxsid_srcId")', function() {
      // uncomment below and update the code to test the property rsgxsidSrcId
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property msgflags (base name: "msgflags")', function() {
      // uncomment below and update the code to test the property msgflags
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property rspeeridMsgto (base name: "rspeerid_msgto")', function() {
      // uncomment below and update the code to test the property rspeeridMsgto
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property rspeeridMsgcc (base name: "rspeerid_msgcc")', function() {
      // uncomment below and update the code to test the property rspeeridMsgcc
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property rspeeridMsgbcc (base name: "rspeerid_msgbcc")', function() {
      // uncomment below and update the code to test the property rspeeridMsgbcc
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property rsgxsidMsgto (base name: "rsgxsid_msgto")', function() {
      // uncomment below and update the code to test the property rsgxsidMsgto
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property rsgxsidMsgcc (base name: "rsgxsid_msgcc")', function() {
      // uncomment below and update the code to test the property rsgxsidMsgcc
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property rsgxsidMsgbcc (base name: "rsgxsid_msgbcc")', function() {
      // uncomment below and update the code to test the property rsgxsidMsgbcc
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property title (base name: "title")', function() {
      // uncomment below and update the code to test the property title
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property msg (base name: "msg")', function() {
      // uncomment below and update the code to test the property msg
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property attachTitle (base name: "attach_title")', function() {
      // uncomment below and update the code to test the property attachTitle
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property attachComment (base name: "attach_comment")', function() {
      // uncomment below and update the code to test the property attachComment
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property files (base name: "files")', function() {
      // uncomment below and update the code to test the property files
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property size (base name: "size")', function() {
      // uncomment below and update the code to test the property size
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property count (base name: "count")', function() {
      // uncomment below and update the code to test the property count
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

    it('should have the property ts (base name: "ts")', function() {
      // uncomment below and update the code to test the property ts
      //var instane = new RetroShareOpenApiWrapper.RsMsgsMessageInfo();
      //expect(instance).to.be();
    });

  });

}));
