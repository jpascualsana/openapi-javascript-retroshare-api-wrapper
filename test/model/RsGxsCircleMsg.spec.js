/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.RetroShareOpenApiWrapper);
  }
}(this, function(expect, RetroShareOpenApiWrapper) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new RetroShareOpenApiWrapper.RsGxsCircleMsg();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('RsGxsCircleMsg', function() {
    it('should create an instance of RsGxsCircleMsg', function() {
      // uncomment below and update the code to test RsGxsCircleMsg
      //var instane = new RetroShareOpenApiWrapper.RsGxsCircleMsg();
      //expect(instance).to.be.a(RetroShareOpenApiWrapper.RsGxsCircleMsg);
    });

    it('should have the property mMeta (base name: "mMeta")', function() {
      // uncomment below and update the code to test the property mMeta
      //var instane = new RetroShareOpenApiWrapper.RsGxsCircleMsg();
      //expect(instance).to.be();
    });

    it('should have the property stuff (base name: "stuff")', function() {
      // uncomment below and update the code to test the property stuff
      //var instane = new RetroShareOpenApiWrapper.RsGxsCircleMsg();
      //expect(instance).to.be();
    });

  });

}));
