/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.RetroShareOpenApiWrapper);
  }
}(this, function(expect, RetroShareOpenApiWrapper) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new RetroShareOpenApiWrapper.RsGroupMetaData();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('RsGroupMetaData', function() {
    it('should create an instance of RsGroupMetaData', function() {
      // uncomment below and update the code to test RsGroupMetaData
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be.a(RetroShareOpenApiWrapper.RsGroupMetaData);
    });

    it('should have the property mGroupId (base name: "mGroupId")', function() {
      // uncomment below and update the code to test the property mGroupId
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mGroupName (base name: "mGroupName")', function() {
      // uncomment below and update the code to test the property mGroupName
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mGroupFlags (base name: "mGroupFlags")', function() {
      // uncomment below and update the code to test the property mGroupFlags
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mSignFlags (base name: "mSignFlags")', function() {
      // uncomment below and update the code to test the property mSignFlags
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mPublishTs (base name: "mPublishTs")', function() {
      // uncomment below and update the code to test the property mPublishTs
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mAuthorId (base name: "mAuthorId")', function() {
      // uncomment below and update the code to test the property mAuthorId
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mCircleId (base name: "mCircleId")', function() {
      // uncomment below and update the code to test the property mCircleId
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mCircleType (base name: "mCircleType")', function() {
      // uncomment below and update the code to test the property mCircleType
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mAuthenFlags (base name: "mAuthenFlags")', function() {
      // uncomment below and update the code to test the property mAuthenFlags
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mParentGrpId (base name: "mParentGrpId")', function() {
      // uncomment below and update the code to test the property mParentGrpId
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mSubscribeFlags (base name: "mSubscribeFlags")', function() {
      // uncomment below and update the code to test the property mSubscribeFlags
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mPop (base name: "mPop")', function() {
      // uncomment below and update the code to test the property mPop
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mVisibleMsgCount (base name: "mVisibleMsgCount")', function() {
      // uncomment below and update the code to test the property mVisibleMsgCount
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mLastPost (base name: "mLastPost")', function() {
      // uncomment below and update the code to test the property mLastPost
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mGroupStatus (base name: "mGroupStatus")', function() {
      // uncomment below and update the code to test the property mGroupStatus
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mServiceString (base name: "mServiceString")', function() {
      // uncomment below and update the code to test the property mServiceString
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mOriginator (base name: "mOriginator")', function() {
      // uncomment below and update the code to test the property mOriginator
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

    it('should have the property mInternalCircle (base name: "mInternalCircle")', function() {
      // uncomment below and update the code to test the property mInternalCircle
      //var instane = new RetroShareOpenApiWrapper.RsGroupMetaData();
      //expect(instance).to.be();
    });

  });

}));
