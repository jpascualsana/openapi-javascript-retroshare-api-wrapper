/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import RsGroupMetaData from './RsGroupMetaData';

/**
 * The RespRsGxsChannelsGetChannelsSummaries model module.
 * @module model/RespRsGxsChannelsGetChannelsSummaries
 * @version 0.0.1
 */
class RespRsGxsChannelsGetChannelsSummaries {
    /**
     * Constructs a new <code>RespRsGxsChannelsGetChannelsSummaries</code>.
     * @alias module:model/RespRsGxsChannelsGetChannelsSummaries
     */
    constructor() { 
        
        RespRsGxsChannelsGetChannelsSummaries.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsGxsChannelsGetChannelsSummaries</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsGxsChannelsGetChannelsSummaries} obj Optional instance to populate.
     * @return {module:model/RespRsGxsChannelsGetChannelsSummaries} The populated <code>RespRsGxsChannelsGetChannelsSummaries</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsGxsChannelsGetChannelsSummaries();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
            if (data.hasOwnProperty('channels')) {
                obj['channels'] = ApiClient.convertToType(data['channels'], [RsGroupMetaData]);
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsGxsChannelsGetChannelsSummaries.prototype['retval'] = undefined;

/**
 * @member {Array.<module:model/RsGroupMetaData>} channels
 */
RespRsGxsChannelsGetChannelsSummaries.prototype['channels'] = undefined;






export default RespRsGxsChannelsGetChannelsSummaries;

