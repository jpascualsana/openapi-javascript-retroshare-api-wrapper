/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ReqRsAccountsExportIdentity model module.
 * @module model/ReqRsAccountsExportIdentity
 * @version 0.0.1
 */
class ReqRsAccountsExportIdentity {
    /**
     * Constructs a new <code>ReqRsAccountsExportIdentity</code>.
     * @alias module:model/ReqRsAccountsExportIdentity
     */
    constructor() { 
        
        ReqRsAccountsExportIdentity.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>ReqRsAccountsExportIdentity</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ReqRsAccountsExportIdentity} obj Optional instance to populate.
     * @return {module:model/ReqRsAccountsExportIdentity} The populated <code>ReqRsAccountsExportIdentity</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ReqRsAccountsExportIdentity();

            if (data.hasOwnProperty('filePath')) {
                obj['filePath'] = ApiClient.convertToType(data['filePath'], 'String');
            }
            if (data.hasOwnProperty('pgpId')) {
                obj['pgpId'] = ApiClient.convertToType(data['pgpId'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} filePath
 */
ReqRsAccountsExportIdentity.prototype['filePath'] = undefined;

/**
 * @member {String} pgpId
 */
ReqRsAccountsExportIdentity.prototype['pgpId'] = undefined;






export default ReqRsAccountsExportIdentity;

