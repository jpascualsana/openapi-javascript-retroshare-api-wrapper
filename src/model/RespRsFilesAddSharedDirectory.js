/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RespRsFilesAddSharedDirectory model module.
 * @module model/RespRsFilesAddSharedDirectory
 * @version 0.0.1
 */
class RespRsFilesAddSharedDirectory {
    /**
     * Constructs a new <code>RespRsFilesAddSharedDirectory</code>.
     * @alias module:model/RespRsFilesAddSharedDirectory
     */
    constructor() { 
        
        RespRsFilesAddSharedDirectory.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsFilesAddSharedDirectory</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsFilesAddSharedDirectory} obj Optional instance to populate.
     * @return {module:model/RespRsFilesAddSharedDirectory} The populated <code>RespRsFilesAddSharedDirectory</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsFilesAddSharedDirectory();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsFilesAddSharedDirectory.prototype['retval'] = undefined;






export default RespRsFilesAddSharedDirectory;

