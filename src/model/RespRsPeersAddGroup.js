/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RespRsPeersAddGroup model module.
 * @module model/RespRsPeersAddGroup
 * @version 0.0.1
 */
class RespRsPeersAddGroup {
    /**
     * Constructs a new <code>RespRsPeersAddGroup</code>.
     * @alias module:model/RespRsPeersAddGroup
     */
    constructor() { 
        
        RespRsPeersAddGroup.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsPeersAddGroup</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsPeersAddGroup} obj Optional instance to populate.
     * @return {module:model/RespRsPeersAddGroup} The populated <code>RespRsPeersAddGroup</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsPeersAddGroup();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsPeersAddGroup.prototype['retval'] = undefined;






export default RespRsPeersAddGroup;

