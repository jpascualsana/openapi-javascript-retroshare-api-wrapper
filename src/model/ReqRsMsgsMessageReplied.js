/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ReqRsMsgsMessageReplied model module.
 * @module model/ReqRsMsgsMessageReplied
 * @version 0.0.1
 */
class ReqRsMsgsMessageReplied {
    /**
     * Constructs a new <code>ReqRsMsgsMessageReplied</code>.
     * @alias module:model/ReqRsMsgsMessageReplied
     */
    constructor() { 
        
        ReqRsMsgsMessageReplied.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>ReqRsMsgsMessageReplied</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ReqRsMsgsMessageReplied} obj Optional instance to populate.
     * @return {module:model/ReqRsMsgsMessageReplied} The populated <code>ReqRsMsgsMessageReplied</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ReqRsMsgsMessageReplied();

            if (data.hasOwnProperty('msgId')) {
                obj['msgId'] = ApiClient.convertToType(data['msgId'], 'String');
            }
            if (data.hasOwnProperty('replied')) {
                obj['replied'] = ApiClient.convertToType(data['replied'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {String} msgId
 */
ReqRsMsgsMessageReplied.prototype['msgId'] = undefined;

/**
 * @member {Boolean} replied
 */
ReqRsMsgsMessageReplied.prototype['replied'] = undefined;






export default ReqRsMsgsMessageReplied;

