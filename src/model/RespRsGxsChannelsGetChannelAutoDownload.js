/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RespRsGxsChannelsGetChannelAutoDownload model module.
 * @module model/RespRsGxsChannelsGetChannelAutoDownload
 * @version 0.0.1
 */
class RespRsGxsChannelsGetChannelAutoDownload {
    /**
     * Constructs a new <code>RespRsGxsChannelsGetChannelAutoDownload</code>.
     * @alias module:model/RespRsGxsChannelsGetChannelAutoDownload
     */
    constructor() { 
        
        RespRsGxsChannelsGetChannelAutoDownload.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsGxsChannelsGetChannelAutoDownload</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsGxsChannelsGetChannelAutoDownload} obj Optional instance to populate.
     * @return {module:model/RespRsGxsChannelsGetChannelAutoDownload} The populated <code>RespRsGxsChannelsGetChannelAutoDownload</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsGxsChannelsGetChannelAutoDownload();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
            if (data.hasOwnProperty('enabled')) {
                obj['enabled'] = ApiClient.convertToType(data['enabled'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsGxsChannelsGetChannelAutoDownload.prototype['retval'] = undefined;

/**
 * @member {Boolean} enabled
 */
RespRsGxsChannelsGetChannelAutoDownload.prototype['enabled'] = undefined;






export default RespRsGxsChannelsGetChannelAutoDownload;

