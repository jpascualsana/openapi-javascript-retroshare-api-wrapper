/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
/**
* Enum class UsageCode.
* @enum {}
* @readonly
*/
export default class UsageCode {
    
        /**
         * value: 0
         * @const
         */
        "UNKNOWN_USAGE" = 0;

    
        /**
         * value: 1
         * @const
         */
        "GROUP_ADMIN_SIGNATURE_CREATION" = 1;

    
        /**
         * value: 2
         * @const
         */
        "GROUP_ADMIN_SIGNATURE_VALIDATION" = 2;

    
        /**
         * value: 3
         * @const
         */
        "GROUP_AUTHOR_SIGNATURE_CREATION" = 3;

    
        /**
         * value: 4
         * @const
         */
        "GROUP_AUTHOR_SIGNATURE_VALIDATION" = 4;

    
        /**
         * value: 5
         * @const
         */
        "MESSAGE_AUTHOR_SIGNATURE_CREATION" = 5;

    
        /**
         * value: 6
         * @const
         */
        "MESSAGE_AUTHOR_SIGNATURE_VALIDATION" = 6;

    
        /**
         * value: 7
         * @const
         */
        "GROUP_AUTHOR_KEEP_ALIVE" = 7;

    
        /**
         * value: 8
         * @const
         */
        "MESSAGE_AUTHOR_KEEP_ALIVE" = 8;

    
        /**
         * value: 9
         * @const
         */
        "CHAT_LOBBY_MSG_VALIDATION" = 9;

    
        /**
         * value: 10
         * @const
         */
        "GLOBAL_ROUTER_SIGNATURE_CHECK" = 10;

    
        /**
         * value: 11
         * @const
         */
        "GLOBAL_ROUTER_SIGNATURE_CREATION" = 11;

    
        /**
         * value: 12
         * @const
         */
        "GXS_TUNNEL_DH_SIGNATURE_CHECK" = 12;

    
        /**
         * value: 13
         * @const
         */
        "GXS_TUNNEL_DH_SIGNATURE_CREATION" = 13;

    
        /**
         * value: 14
         * @const
         */
        "IDENTITY_DATA_UPDATE" = 14;

    
        /**
         * value: 15
         * @const
         */
        "IDENTITY_GENERIC_SIGNATURE_CHECK" = 15;

    
        /**
         * value: 16
         * @const
         */
        "IDENTITY_GENERIC_SIGNATURE_CREATION" = 16;

    
        /**
         * value: 17
         * @const
         */
        "IDENTITY_GENERIC_ENCRYPTION" = 17;

    
        /**
         * value: 18
         * @const
         */
        "IDENTITY_GENERIC_DECRYPTION" = 18;

    
        /**
         * value: 19
         * @const
         */
        "CIRCLE_MEMBERSHIP_CHECK" = 19;

    

    /**
    * Returns a <code>UsageCode</code> enum value from a Javascript object name.
    * @param {Object} data The plain JavaScript object containing the name of the enum value.
    * @return {module:model/UsageCode} The enum <code>UsageCode</code> value.
    */
    static constructFromObject(object) {
        return object;
    }
}

