/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RespRsFilesFileDownloads model module.
 * @module model/RespRsFilesFileDownloads
 * @version 0.0.1
 */
class RespRsFilesFileDownloads {
    /**
     * Constructs a new <code>RespRsFilesFileDownloads</code>.
     * @alias module:model/RespRsFilesFileDownloads
     */
    constructor() { 
        
        RespRsFilesFileDownloads.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsFilesFileDownloads</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsFilesFileDownloads} obj Optional instance to populate.
     * @return {module:model/RespRsFilesFileDownloads} The populated <code>RespRsFilesFileDownloads</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsFilesFileDownloads();

            if (data.hasOwnProperty('hashs')) {
                obj['hashs'] = ApiClient.convertToType(data['hashs'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * @member {Array.<String>} hashs
 */
RespRsFilesFileDownloads.prototype['hashs'] = undefined;






export default RespRsFilesFileDownloads;

