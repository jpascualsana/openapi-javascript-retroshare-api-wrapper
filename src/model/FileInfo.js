/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import DwlSpeed from './DwlSpeed';
import TransferInfo from './TransferInfo';

/**
 * The FileInfo model module.
 * @module model/FileInfo
 * @version 0.0.1
 */
class FileInfo {
    /**
     * Constructs a new <code>FileInfo</code>.
     * @alias module:model/FileInfo
     */
    constructor() { 
        
        FileInfo.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>FileInfo</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/FileInfo} obj Optional instance to populate.
     * @return {module:model/FileInfo} The populated <code>FileInfo</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new FileInfo();

            if (data.hasOwnProperty('storage_permission_flags')) {
                obj['storage_permission_flags'] = ApiClient.convertToType(data['storage_permission_flags'], 'Number');
            }
            if (data.hasOwnProperty('transfer_info_flags')) {
                obj['transfer_info_flags'] = ApiClient.convertToType(data['transfer_info_flags'], 'Number');
            }
            if (data.hasOwnProperty('mId')) {
                obj['mId'] = ApiClient.convertToType(data['mId'], 'Number');
            }
            if (data.hasOwnProperty('searchId')) {
                obj['searchId'] = ApiClient.convertToType(data['searchId'], 'Number');
            }
            if (data.hasOwnProperty('path')) {
                obj['path'] = ApiClient.convertToType(data['path'], 'String');
            }
            if (data.hasOwnProperty('fname')) {
                obj['fname'] = ApiClient.convertToType(data['fname'], 'String');
            }
            if (data.hasOwnProperty('hash')) {
                obj['hash'] = ApiClient.convertToType(data['hash'], 'String');
            }
            if (data.hasOwnProperty('ext')) {
                obj['ext'] = ApiClient.convertToType(data['ext'], 'String');
            }
            if (data.hasOwnProperty('size')) {
                obj['size'] = ApiClient.convertToType(data['size'], 'Number');
            }
            if (data.hasOwnProperty('avail')) {
                obj['avail'] = ApiClient.convertToType(data['avail'], 'Number');
            }
            if (data.hasOwnProperty('rank')) {
                obj['rank'] = ApiClient.convertToType(data['rank'], 'Number');
            }
            if (data.hasOwnProperty('age')) {
                obj['age'] = ApiClient.convertToType(data['age'], 'Number');
            }
            if (data.hasOwnProperty('queue_position')) {
                obj['queue_position'] = ApiClient.convertToType(data['queue_position'], 'Number');
            }
            if (data.hasOwnProperty('transfered')) {
                obj['transfered'] = ApiClient.convertToType(data['transfered'], 'Number');
            }
            if (data.hasOwnProperty('tfRate')) {
                obj['tfRate'] = ApiClient.convertToType(data['tfRate'], 'Number');
            }
            if (data.hasOwnProperty('downloadStatus')) {
                obj['downloadStatus'] = ApiClient.convertToType(data['downloadStatus'], 'Number');
            }
            if (data.hasOwnProperty('peers')) {
                obj['peers'] = ApiClient.convertToType(data['peers'], [TransferInfo]);
            }
            if (data.hasOwnProperty('priority')) {
                obj['priority'] = DwlSpeed.constructFromObject(data['priority']);
            }
            if (data.hasOwnProperty('lastTS')) {
                obj['lastTS'] = ApiClient.convertToType(data['lastTS'], 'Number');
            }
            if (data.hasOwnProperty('parent_groups')) {
                obj['parent_groups'] = ApiClient.convertToType(data['parent_groups'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * @member {Number} storage_permission_flags
 */
FileInfo.prototype['storage_permission_flags'] = undefined;

/**
 * @member {Number} transfer_info_flags
 */
FileInfo.prototype['transfer_info_flags'] = undefined;

/**
 * @member {Number} mId
 */
FileInfo.prototype['mId'] = undefined;

/**
 * @member {Number} searchId
 */
FileInfo.prototype['searchId'] = undefined;

/**
 * @member {String} path
 */
FileInfo.prototype['path'] = undefined;

/**
 * @member {String} fname
 */
FileInfo.prototype['fname'] = undefined;

/**
 * @member {String} hash
 */
FileInfo.prototype['hash'] = undefined;

/**
 * @member {String} ext
 */
FileInfo.prototype['ext'] = undefined;

/**
 * @member {Number} size
 */
FileInfo.prototype['size'] = undefined;

/**
 * @member {Number} avail
 */
FileInfo.prototype['avail'] = undefined;

/**
 * @member {Number} rank
 */
FileInfo.prototype['rank'] = undefined;

/**
 * @member {Number} age
 */
FileInfo.prototype['age'] = undefined;

/**
 * @member {Number} queue_position
 */
FileInfo.prototype['queue_position'] = undefined;

/**
 * @member {Number} transfered
 */
FileInfo.prototype['transfered'] = undefined;

/**
 * @member {Number} tfRate
 */
FileInfo.prototype['tfRate'] = undefined;

/**
 * @member {Number} downloadStatus
 */
FileInfo.prototype['downloadStatus'] = undefined;

/**
 * @member {Array.<module:model/TransferInfo>} peers
 */
FileInfo.prototype['peers'] = undefined;

/**
 * @member {module:model/DwlSpeed} priority
 */
FileInfo.prototype['priority'] = undefined;

/**
 * @member {Number} lastTS
 */
FileInfo.prototype['lastTS'] = undefined;

/**
 * @member {Array.<String>} parent_groups
 */
FileInfo.prototype['parent_groups'] = undefined;






export default FileInfo;

