/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ReqRsMsgsMessageToTrash model module.
 * @module model/ReqRsMsgsMessageToTrash
 * @version 0.0.1
 */
class ReqRsMsgsMessageToTrash {
    /**
     * Constructs a new <code>ReqRsMsgsMessageToTrash</code>.
     * @alias module:model/ReqRsMsgsMessageToTrash
     */
    constructor() { 
        
        ReqRsMsgsMessageToTrash.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>ReqRsMsgsMessageToTrash</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ReqRsMsgsMessageToTrash} obj Optional instance to populate.
     * @return {module:model/ReqRsMsgsMessageToTrash} The populated <code>ReqRsMsgsMessageToTrash</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ReqRsMsgsMessageToTrash();

            if (data.hasOwnProperty('msgId')) {
                obj['msgId'] = ApiClient.convertToType(data['msgId'], 'String');
            }
            if (data.hasOwnProperty('bTrash')) {
                obj['bTrash'] = ApiClient.convertToType(data['bTrash'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {String} msgId
 */
ReqRsMsgsMessageToTrash.prototype['msgId'] = undefined;

/**
 * @member {Boolean} bTrash
 */
ReqRsMsgsMessageToTrash.prototype['bTrash'] = undefined;






export default ReqRsMsgsMessageToTrash;

