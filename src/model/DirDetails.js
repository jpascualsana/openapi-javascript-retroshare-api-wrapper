/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import DirStub from './DirStub';

/**
 * The DirDetails model module.
 * @module model/DirDetails
 * @version 0.0.1
 */
class DirDetails {
    /**
     * Constructs a new <code>DirDetails</code>.
     * @alias module:model/DirDetails
     */
    constructor() { 
        
        DirDetails.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>DirDetails</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/DirDetails} obj Optional instance to populate.
     * @return {module:model/DirDetails} The populated <code>DirDetails</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new DirDetails();

            if (data.hasOwnProperty('parent')) {
                obj['parent'] = ApiClient.convertToType(data['parent'], 'Number');
            }
            if (data.hasOwnProperty('prow')) {
                obj['prow'] = ApiClient.convertToType(data['prow'], 'Number');
            }
            if (data.hasOwnProperty('ref')) {
                obj['ref'] = ApiClient.convertToType(data['ref'], 'Number');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'Number');
            }
            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'String');
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('hash')) {
                obj['hash'] = ApiClient.convertToType(data['hash'], 'String');
            }
            if (data.hasOwnProperty('path')) {
                obj['path'] = ApiClient.convertToType(data['path'], 'String');
            }
            if (data.hasOwnProperty('count')) {
                obj['count'] = ApiClient.convertToType(data['count'], 'Number');
            }
            if (data.hasOwnProperty('mtime')) {
                obj['mtime'] = ApiClient.convertToType(data['mtime'], 'Number');
            }
            if (data.hasOwnProperty('flags')) {
                obj['flags'] = ApiClient.convertToType(data['flags'], 'Number');
            }
            if (data.hasOwnProperty('max_mtime')) {
                obj['max_mtime'] = ApiClient.convertToType(data['max_mtime'], 'Number');
            }
            if (data.hasOwnProperty('children')) {
                obj['children'] = ApiClient.convertToType(data['children'], [DirStub]);
            }
            if (data.hasOwnProperty('parent_groups')) {
                obj['parent_groups'] = ApiClient.convertToType(data['parent_groups'], ['String']);
            }
        }
        return obj;
    }


}

/**
 * @member {Number} parent
 */
DirDetails.prototype['parent'] = undefined;

/**
 * @member {Number} prow
 */
DirDetails.prototype['prow'] = undefined;

/**
 * @member {Number} ref
 */
DirDetails.prototype['ref'] = undefined;

/**
 * @member {Number} type
 */
DirDetails.prototype['type'] = undefined;

/**
 * @member {String} id
 */
DirDetails.prototype['id'] = undefined;

/**
 * @member {String} name
 */
DirDetails.prototype['name'] = undefined;

/**
 * @member {String} hash
 */
DirDetails.prototype['hash'] = undefined;

/**
 * @member {String} path
 */
DirDetails.prototype['path'] = undefined;

/**
 * @member {Number} count
 */
DirDetails.prototype['count'] = undefined;

/**
 * @member {Number} mtime
 */
DirDetails.prototype['mtime'] = undefined;

/**
 * @member {Number} flags
 */
DirDetails.prototype['flags'] = undefined;

/**
 * @member {Number} max_mtime
 */
DirDetails.prototype['max_mtime'] = undefined;

/**
 * @member {Array.<module:model/DirStub>} children
 */
DirDetails.prototype['children'] = undefined;

/**
 * @member {Array.<String>} parent_groups
 */
DirDetails.prototype['parent_groups'] = undefined;






export default DirDetails;

