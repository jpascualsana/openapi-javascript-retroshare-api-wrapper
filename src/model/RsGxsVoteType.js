/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
/**
* Enum class RsGxsVoteType.
* @enum {}
* @readonly
*/
export default class RsGxsVoteType {
    
        /**
         * value: 0
         * @const
         */
        "NONE" = 0;

    
        /**
         * value: 1
         * @const
         */
        "DOWN" = 1;

    
        /**
         * value: 2
         * @const
         */
        "UP" = 2;

    

    /**
    * Returns a <code>RsGxsVoteType</code> enum value from a Javascript object name.
    * @param {Object} data The plain JavaScript object containing the name of the enum value.
    * @return {module:model/RsGxsVoteType} The enum <code>RsGxsVoteType</code> value.
    */
    static constructFromObject(object) {
        return object;
    }
}

