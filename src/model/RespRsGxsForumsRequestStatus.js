/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import RsTokenServiceGxsRequestStatus from './RsTokenServiceGxsRequestStatus';

/**
 * The RespRsGxsForumsRequestStatus model module.
 * @module model/RespRsGxsForumsRequestStatus
 * @version 0.0.1
 */
class RespRsGxsForumsRequestStatus {
    /**
     * Constructs a new <code>RespRsGxsForumsRequestStatus</code>.
     * @alias module:model/RespRsGxsForumsRequestStatus
     */
    constructor() { 
        
        RespRsGxsForumsRequestStatus.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsGxsForumsRequestStatus</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsGxsForumsRequestStatus} obj Optional instance to populate.
     * @return {module:model/RespRsGxsForumsRequestStatus} The populated <code>RespRsGxsForumsRequestStatus</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsGxsForumsRequestStatus();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = RsTokenServiceGxsRequestStatus.constructFromObject(data['retval']);
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/RsTokenServiceGxsRequestStatus} retval
 */
RespRsGxsForumsRequestStatus.prototype['retval'] = undefined;






export default RespRsGxsForumsRequestStatus;

