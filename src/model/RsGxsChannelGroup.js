/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import RsGroupMetaData from './RsGroupMetaData';
import RsGxsImage from './RsGxsImage';

/**
 * The RsGxsChannelGroup model module.
 * @module model/RsGxsChannelGroup
 * @version 0.0.1
 */
class RsGxsChannelGroup {
    /**
     * Constructs a new <code>RsGxsChannelGroup</code>.
     * @alias module:model/RsGxsChannelGroup
     */
    constructor() { 
        
        RsGxsChannelGroup.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RsGxsChannelGroup</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RsGxsChannelGroup} obj Optional instance to populate.
     * @return {module:model/RsGxsChannelGroup} The populated <code>RsGxsChannelGroup</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RsGxsChannelGroup();

            if (data.hasOwnProperty('mMeta')) {
                obj['mMeta'] = RsGroupMetaData.constructFromObject(data['mMeta']);
            }
            if (data.hasOwnProperty('mDescription')) {
                obj['mDescription'] = ApiClient.convertToType(data['mDescription'], 'String');
            }
            if (data.hasOwnProperty('mImage')) {
                obj['mImage'] = RsGxsImage.constructFromObject(data['mImage']);
            }
            if (data.hasOwnProperty('mAutoDownload')) {
                obj['mAutoDownload'] = ApiClient.convertToType(data['mAutoDownload'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {module:model/RsGroupMetaData} mMeta
 */
RsGxsChannelGroup.prototype['mMeta'] = undefined;

/**
 * @member {String} mDescription
 */
RsGxsChannelGroup.prototype['mDescription'] = undefined;

/**
 * @member {module:model/RsGxsImage} mImage
 */
RsGxsChannelGroup.prototype['mImage'] = undefined;

/**
 * @member {Boolean} mAutoDownload
 */
RsGxsChannelGroup.prototype['mAutoDownload'] = undefined;






export default RsGxsChannelGroup;

