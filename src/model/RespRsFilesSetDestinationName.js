/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RespRsFilesSetDestinationName model module.
 * @module model/RespRsFilesSetDestinationName
 * @version 0.0.1
 */
class RespRsFilesSetDestinationName {
    /**
     * Constructs a new <code>RespRsFilesSetDestinationName</code>.
     * @alias module:model/RespRsFilesSetDestinationName
     */
    constructor() { 
        
        RespRsFilesSetDestinationName.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsFilesSetDestinationName</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsFilesSetDestinationName} obj Optional instance to populate.
     * @return {module:model/RespRsFilesSetDestinationName} The populated <code>RespRsFilesSetDestinationName</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsFilesSetDestinationName();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsFilesSetDestinationName.prototype['retval'] = undefined;






export default RespRsFilesSetDestinationName;

