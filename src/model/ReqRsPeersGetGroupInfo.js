/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ReqRsPeersGetGroupInfo model module.
 * @module model/ReqRsPeersGetGroupInfo
 * @version 0.0.1
 */
class ReqRsPeersGetGroupInfo {
    /**
     * Constructs a new <code>ReqRsPeersGetGroupInfo</code>.
     * @alias module:model/ReqRsPeersGetGroupInfo
     */
    constructor() { 
        
        ReqRsPeersGetGroupInfo.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>ReqRsPeersGetGroupInfo</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ReqRsPeersGetGroupInfo} obj Optional instance to populate.
     * @return {module:model/ReqRsPeersGetGroupInfo} The populated <code>ReqRsPeersGetGroupInfo</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ReqRsPeersGetGroupInfo();

            if (data.hasOwnProperty('groupId')) {
                obj['groupId'] = ApiClient.convertToType(data['groupId'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} groupId
 */
ReqRsPeersGetGroupInfo.prototype['groupId'] = undefined;






export default ReqRsPeersGetGroupInfo;

