/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import CompressedChunkMap from './CompressedChunkMap';

/**
 * The RespRsFilesFileUploadChunksDetails model module.
 * @module model/RespRsFilesFileUploadChunksDetails
 * @version 0.0.1
 */
class RespRsFilesFileUploadChunksDetails {
    /**
     * Constructs a new <code>RespRsFilesFileUploadChunksDetails</code>.
     * @alias module:model/RespRsFilesFileUploadChunksDetails
     */
    constructor() { 
        
        RespRsFilesFileUploadChunksDetails.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsFilesFileUploadChunksDetails</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsFilesFileUploadChunksDetails} obj Optional instance to populate.
     * @return {module:model/RespRsFilesFileUploadChunksDetails} The populated <code>RespRsFilesFileUploadChunksDetails</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsFilesFileUploadChunksDetails();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
            if (data.hasOwnProperty('map')) {
                obj['map'] = CompressedChunkMap.constructFromObject(data['map']);
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsFilesFileUploadChunksDetails.prototype['retval'] = undefined;

/**
 * @member {module:model/CompressedChunkMap} map
 */
RespRsFilesFileUploadChunksDetails.prototype['map'] = undefined;






export default RespRsFilesFileUploadChunksDetails;

