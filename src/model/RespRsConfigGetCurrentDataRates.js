/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RespRsConfigGetCurrentDataRates model module.
 * @module model/RespRsConfigGetCurrentDataRates
 * @version 0.0.1
 */
class RespRsConfigGetCurrentDataRates {
    /**
     * Constructs a new <code>RespRsConfigGetCurrentDataRates</code>.
     * @alias module:model/RespRsConfigGetCurrentDataRates
     */
    constructor() { 
        
        RespRsConfigGetCurrentDataRates.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsConfigGetCurrentDataRates</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsConfigGetCurrentDataRates} obj Optional instance to populate.
     * @return {module:model/RespRsConfigGetCurrentDataRates} The populated <code>RespRsConfigGetCurrentDataRates</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsConfigGetCurrentDataRates();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Number');
            }
            if (data.hasOwnProperty('inKb')) {
                obj['inKb'] = ApiClient.convertToType(data['inKb'], 'Number');
            }
            if (data.hasOwnProperty('outKb')) {
                obj['outKb'] = ApiClient.convertToType(data['outKb'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} retval
 */
RespRsConfigGetCurrentDataRates.prototype['retval'] = undefined;

/**
 * @member {Number} inKb
 */
RespRsConfigGetCurrentDataRates.prototype['inKb'] = undefined;

/**
 * @member {Number} outKb
 */
RespRsConfigGetCurrentDataRates.prototype['outKb'] = undefined;






export default RespRsConfigGetCurrentDataRates;

