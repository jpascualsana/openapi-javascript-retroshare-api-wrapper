/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RespRsPeersAssignPeerToGroup model module.
 * @module model/RespRsPeersAssignPeerToGroup
 * @version 0.0.1
 */
class RespRsPeersAssignPeerToGroup {
    /**
     * Constructs a new <code>RespRsPeersAssignPeerToGroup</code>.
     * @alias module:model/RespRsPeersAssignPeerToGroup
     */
    constructor() { 
        
        RespRsPeersAssignPeerToGroup.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsPeersAssignPeerToGroup</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsPeersAssignPeerToGroup} obj Optional instance to populate.
     * @return {module:model/RespRsPeersAssignPeerToGroup} The populated <code>RespRsPeersAssignPeerToGroup</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsPeersAssignPeerToGroup();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsPeersAssignPeerToGroup.prototype['retval'] = undefined;






export default RespRsPeersAssignPeerToGroup;

