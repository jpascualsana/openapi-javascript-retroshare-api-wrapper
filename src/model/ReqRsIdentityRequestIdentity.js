/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ReqRsIdentityRequestIdentity model module.
 * @module model/ReqRsIdentityRequestIdentity
 * @version 0.0.1
 */
class ReqRsIdentityRequestIdentity {
    /**
     * Constructs a new <code>ReqRsIdentityRequestIdentity</code>.
     * @alias module:model/ReqRsIdentityRequestIdentity
     */
    constructor() { 
        
        ReqRsIdentityRequestIdentity.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>ReqRsIdentityRequestIdentity</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ReqRsIdentityRequestIdentity} obj Optional instance to populate.
     * @return {module:model/ReqRsIdentityRequestIdentity} The populated <code>ReqRsIdentityRequestIdentity</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ReqRsIdentityRequestIdentity();

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} id
 */
ReqRsIdentityRequestIdentity.prototype['id'] = undefined;






export default ReqRsIdentityRequestIdentity;

