/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RespRsGxsChannelsGetChannelDownloadDirectory model module.
 * @module model/RespRsGxsChannelsGetChannelDownloadDirectory
 * @version 0.0.1
 */
class RespRsGxsChannelsGetChannelDownloadDirectory {
    /**
     * Constructs a new <code>RespRsGxsChannelsGetChannelDownloadDirectory</code>.
     * @alias module:model/RespRsGxsChannelsGetChannelDownloadDirectory
     */
    constructor() { 
        
        RespRsGxsChannelsGetChannelDownloadDirectory.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsGxsChannelsGetChannelDownloadDirectory</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsGxsChannelsGetChannelDownloadDirectory} obj Optional instance to populate.
     * @return {module:model/RespRsGxsChannelsGetChannelDownloadDirectory} The populated <code>RespRsGxsChannelsGetChannelDownloadDirectory</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsGxsChannelsGetChannelDownloadDirectory();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
            if (data.hasOwnProperty('directory')) {
                obj['directory'] = ApiClient.convertToType(data['directory'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsGxsChannelsGetChannelDownloadDirectory.prototype['retval'] = undefined;

/**
 * @member {String} directory
 */
RespRsGxsChannelsGetChannelDownloadDirectory.prototype['directory'] = undefined;






export default RespRsGxsChannelsGetChannelDownloadDirectory;

