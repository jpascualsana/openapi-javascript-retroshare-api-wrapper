/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ReqRsReputationsSetAutoPositiveOpinionForContacts model module.
 * @module model/ReqRsReputationsSetAutoPositiveOpinionForContacts
 * @version 0.0.1
 */
class ReqRsReputationsSetAutoPositiveOpinionForContacts {
    /**
     * Constructs a new <code>ReqRsReputationsSetAutoPositiveOpinionForContacts</code>.
     * @alias module:model/ReqRsReputationsSetAutoPositiveOpinionForContacts
     */
    constructor() { 
        
        ReqRsReputationsSetAutoPositiveOpinionForContacts.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>ReqRsReputationsSetAutoPositiveOpinionForContacts</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ReqRsReputationsSetAutoPositiveOpinionForContacts} obj Optional instance to populate.
     * @return {module:model/ReqRsReputationsSetAutoPositiveOpinionForContacts} The populated <code>ReqRsReputationsSetAutoPositiveOpinionForContacts</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ReqRsReputationsSetAutoPositiveOpinionForContacts();

            if (data.hasOwnProperty('b')) {
                obj['b'] = ApiClient.convertToType(data['b'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} b
 */
ReqRsReputationsSetAutoPositiveOpinionForContacts.prototype['b'] = undefined;






export default ReqRsReputationsSetAutoPositiveOpinionForContacts;

