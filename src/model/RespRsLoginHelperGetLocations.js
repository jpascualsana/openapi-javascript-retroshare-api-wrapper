/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import RsLoginHelperLocation from './RsLoginHelperLocation';

/**
 * The RespRsLoginHelperGetLocations model module.
 * @module model/RespRsLoginHelperGetLocations
 * @version 0.0.1
 */
class RespRsLoginHelperGetLocations {
    /**
     * Constructs a new <code>RespRsLoginHelperGetLocations</code>.
     * @alias module:model/RespRsLoginHelperGetLocations
     */
    constructor() { 
        
        RespRsLoginHelperGetLocations.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsLoginHelperGetLocations</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsLoginHelperGetLocations} obj Optional instance to populate.
     * @return {module:model/RespRsLoginHelperGetLocations} The populated <code>RespRsLoginHelperGetLocations</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsLoginHelperGetLocations();

            if (data.hasOwnProperty('locations')) {
                obj['locations'] = ApiClient.convertToType(data['locations'], [RsLoginHelperLocation]);
            }
        }
        return obj;
    }


}

/**
 * @member {Array.<module:model/RsLoginHelperLocation>} locations
 */
RespRsLoginHelperGetLocations.prototype['locations'] = undefined;






export default RespRsLoginHelperGetLocations;

