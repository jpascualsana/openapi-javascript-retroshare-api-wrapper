/**
 * RetroShare OpenApi wrapper
 * RetroShare OpenApi wrapper generated using Doxygen documentation
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import RsGxsComment from './RsGxsComment';

/**
 * The RespRsGxsChannelsCreateComment model module.
 * @module model/RespRsGxsChannelsCreateComment
 * @version 0.0.1
 */
class RespRsGxsChannelsCreateComment {
    /**
     * Constructs a new <code>RespRsGxsChannelsCreateComment</code>.
     * @alias module:model/RespRsGxsChannelsCreateComment
     */
    constructor() { 
        
        RespRsGxsChannelsCreateComment.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RespRsGxsChannelsCreateComment</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RespRsGxsChannelsCreateComment} obj Optional instance to populate.
     * @return {module:model/RespRsGxsChannelsCreateComment} The populated <code>RespRsGxsChannelsCreateComment</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RespRsGxsChannelsCreateComment();

            if (data.hasOwnProperty('retval')) {
                obj['retval'] = ApiClient.convertToType(data['retval'], 'Boolean');
            }
            if (data.hasOwnProperty('comment')) {
                obj['comment'] = RsGxsComment.constructFromObject(data['comment']);
            }
        }
        return obj;
    }


}

/**
 * @member {Boolean} retval
 */
RespRsGxsChannelsCreateComment.prototype['retval'] = undefined;

/**
 * @member {module:model/RsGxsComment} comment
 */
RespRsGxsChannelsCreateComment.prototype['comment'] = undefined;






export default RespRsGxsChannelsCreateComment;

