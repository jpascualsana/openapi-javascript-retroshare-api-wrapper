# RetroShareOpenApiWrapper.RsConfigDataRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mRateIn** | **Number** |  | [optional] 
**mRateMaxIn** | **Number** |  | [optional] 
**mAllocIn** | **Number** |  | [optional] 
**mAllocTs** | **Number** |  | [optional] 
**mRateOut** | **Number** |  | [optional] 
**mRateMaxOut** | **Number** |  | [optional] 
**mAllowedOut** | **Number** |  | [optional] 
**mAllowedTs** | **Number** |  | [optional] 
**mQueueIn** | **Number** |  | [optional] 
**mQueueOut** | **Number** |  | [optional] 


