# RetroShareOpenApiWrapper.ReqRsConfigSetOperatingMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**opMode** | **Number** |  | [optional] 


