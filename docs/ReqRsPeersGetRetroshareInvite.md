# RetroShareOpenApiWrapper.ReqRsPeersGetRetroshareInvite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] 
**includeSignatures** | **Boolean** |  | [optional] 
**includeExtraLocators** | **Boolean** |  | [optional] 


