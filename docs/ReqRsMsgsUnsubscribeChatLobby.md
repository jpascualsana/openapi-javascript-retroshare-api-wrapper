# RetroShareOpenApiWrapper.ReqRsMsgsUnsubscribeChatLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 


