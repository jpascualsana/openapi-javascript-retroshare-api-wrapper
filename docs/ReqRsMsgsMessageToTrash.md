# RetroShareOpenApiWrapper.ReqRsMsgsMessageToTrash

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**bTrash** | **Boolean** |  | [optional] 


