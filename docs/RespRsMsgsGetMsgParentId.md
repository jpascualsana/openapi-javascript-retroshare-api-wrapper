# RetroShareOpenApiWrapper.RespRsMsgsGetMsgParentId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**msgParentId** | **String** |  | [optional] 


