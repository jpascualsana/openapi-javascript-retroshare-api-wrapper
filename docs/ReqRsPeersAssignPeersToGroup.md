# RetroShareOpenApiWrapper.ReqRsPeersAssignPeersToGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] 
**peerIds** | **[String]** |  | [optional] 
**assign** | **Boolean** |  | [optional] 


