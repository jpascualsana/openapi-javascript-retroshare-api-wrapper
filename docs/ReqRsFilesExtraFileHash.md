# RetroShareOpenApiWrapper.ReqRsFilesExtraFileHash

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**localpath** | **String** |  | [optional] 
**period** | **Number** |  | [optional] 
**flags** | **Number** |  | [optional] 


