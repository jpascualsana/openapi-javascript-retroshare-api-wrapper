# RetroShareOpenApiWrapper.ReqRsPeersAddFriend

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] 
**gpgId** | **String** |  | [optional] 
**flags** | **Number** |  | [optional] 


