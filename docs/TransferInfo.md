# RetroShareOpenApiWrapper.TransferInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peerId** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**tfRate** | **Number** |  | [optional] 
**status** | **Number** |  | [optional] 
**transfered** | **Number** |  | [optional] 


