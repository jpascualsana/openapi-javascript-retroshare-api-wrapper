# RetroShareOpenApiWrapper.ReqRsIdentityGetIdDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 


