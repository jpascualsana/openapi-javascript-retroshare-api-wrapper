# RetroShareOpenApiWrapper.ReqJsonApiServerDecodeToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  | [optional] 


