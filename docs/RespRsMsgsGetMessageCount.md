# RetroShareOpenApiWrapper.RespRsMsgsGetMessageCount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nInbox** | **Number** |  | [optional] 
**nInboxNew** | **Number** |  | [optional] 
**nOutbox** | **Number** |  | [optional] 
**nDraftbox** | **Number** |  | [optional] 
**nSentbox** | **Number** |  | [optional] 
**nTrashbox** | **Number** |  | [optional] 


