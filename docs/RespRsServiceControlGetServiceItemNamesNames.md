# RetroShareOpenApiWrapper.RespRsServiceControlGetServiceItemNamesNames

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **Number** |  | [optional] 
**value** | **String** |  | [optional] 


