# RetroShareOpenApiWrapper.ReqRsMsgsSetDefaultIdentityForChatLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nick** | **String** |  | [optional] 


