# RetroShareOpenApiWrapper.DirStub

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**ref** | **Number** |  | [optional] 


