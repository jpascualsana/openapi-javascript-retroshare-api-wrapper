# RetroShareOpenApiWrapper.ReqRsIdentitySetAsRegularContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**isContact** | **Boolean** |  | [optional] 


