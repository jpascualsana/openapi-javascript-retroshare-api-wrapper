# RetroShareOpenApiWrapper.RespRsGxsCirclesGetCircleDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**details** | [**RsGxsCircleDetails**](RsGxsCircleDetails.md) |  | [optional] 


