# RetroShareOpenApiWrapper.ReqRsGxsChannelsCreatePostV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**mBody** | **String** |  | [optional] 
**files** | [**[RsGxsFile]**](RsGxsFile.md) |  | [optional] 
**thumbnail** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**origPostId** | **String** |  | [optional] 


