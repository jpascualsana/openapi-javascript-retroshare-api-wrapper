# RetroShareOpenApiWrapper.RespRsMsgsGetIdentityForChatLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**nick** | **String** |  | [optional] 


