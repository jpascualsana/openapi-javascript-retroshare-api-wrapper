# RetroShareOpenApiWrapper.RsMsgMetaData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mGroupId** | **String** |  | [optional] 
**mMsgId** | **String** |  | [optional] 
**mThreadId** | **String** |  | [optional] 
**mParentId** | **String** |  | [optional] 
**mOrigMsgId** | **String** |  | [optional] 
**mAuthorId** | **String** |  | [optional] 
**mMsgName** | **String** |  | [optional] 
**mPublishTs** | **Number** |  | [optional] 
**mMsgFlags** | **Number** |  | [optional] 
**mMsgStatus** | **Number** |  | [optional] 
**mChildTs** | **Number** |  | [optional] 
**mServiceString** | **String** |  | [optional] 


