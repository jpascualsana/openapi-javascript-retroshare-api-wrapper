# RetroShareOpenApiWrapper.RespRsGossipDiscoveryGetWaitingDiscCount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**sendCount** | **Number** |  | [optional] 
**recvCount** | **Number** |  | [optional] 


