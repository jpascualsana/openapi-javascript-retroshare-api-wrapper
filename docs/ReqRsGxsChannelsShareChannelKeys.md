# RetroShareOpenApiWrapper.ReqRsGxsChannelsShareChannelKeys

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**peers** | **[String]** |  | [optional] 


