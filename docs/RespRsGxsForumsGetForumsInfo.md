# RetroShareOpenApiWrapper.RespRsGxsForumsGetForumsInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**forumsInfo** | [**[RsGxsForumGroup]**](RsGxsForumGroup.md) |  | [optional] 


