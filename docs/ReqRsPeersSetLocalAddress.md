# RetroShareOpenApiWrapper.ReqRsPeersSetLocalAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] 
**addr** | **String** |  | [optional] 
**port** | **Number** |  | [optional] 


