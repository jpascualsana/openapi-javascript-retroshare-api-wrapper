# RetroShareOpenApiWrapper.ReqRsGxsChannelsCreateCommentV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**threadId** | **String** |  | [optional] 
**comment** | **String** |  | [optional] 
**authorId** | **String** |  | [optional] 
**parentId** | **String** |  | [optional] 
**origCommentId** | **String** |  | [optional] 


