# RetroShareOpenApiWrapper.ReqRsFilesFileDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**hintflags** | **Number** |  | [optional] 


