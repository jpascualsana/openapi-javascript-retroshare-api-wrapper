# RetroShareOpenApiWrapper.RespRsGossipDiscoveryGetPeerVersion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**version** | **String** |  | [optional] 


