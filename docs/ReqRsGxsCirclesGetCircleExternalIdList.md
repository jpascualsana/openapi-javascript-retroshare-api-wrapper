# RetroShareOpenApiWrapper.ReqRsGxsCirclesGetCircleExternalIdList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**circleIds** | **[String]** |  | [optional] 


