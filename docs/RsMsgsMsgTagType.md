# RetroShareOpenApiWrapper.RsMsgsMsgTagType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**types** | [**[RsMsgsMsgTagTypeTypes]**](RsMsgsMsgTagTypeTypes.md) |  | [optional] 


