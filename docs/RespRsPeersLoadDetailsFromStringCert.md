# RetroShareOpenApiWrapper.RespRsPeersLoadDetailsFromStringCert

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**certDetails** | [**RsPeerDetails**](RsPeerDetails.md) |  | [optional] 
**errorCode** | **Number** |  | [optional] 


