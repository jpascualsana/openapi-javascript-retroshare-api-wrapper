# RetroShareOpenApiWrapper.RespRsFilesIsHashBanned

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


