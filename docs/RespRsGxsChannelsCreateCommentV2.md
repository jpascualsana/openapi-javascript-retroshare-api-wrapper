# RetroShareOpenApiWrapper.RespRsGxsChannelsCreateCommentV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**commentMessageId** | **String** |  | [optional] 
**errorMessage** | **String** |  | [optional] 


