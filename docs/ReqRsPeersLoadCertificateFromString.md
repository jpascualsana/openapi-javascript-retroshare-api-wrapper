# RetroShareOpenApiWrapper.ReqRsPeersLoadCertificateFromString

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cert** | **String** |  | [optional] 


