# RetroShareOpenApiWrapper.ReqRsConfigSetMaxDataRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**downKb** | **Number** |  | [optional] 
**upKb** | **Number** |  | [optional] 


