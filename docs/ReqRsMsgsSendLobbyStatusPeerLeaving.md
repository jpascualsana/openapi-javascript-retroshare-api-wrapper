# RetroShareOpenApiWrapper.ReqRsMsgsSendLobbyStatusPeerLeaving

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 


