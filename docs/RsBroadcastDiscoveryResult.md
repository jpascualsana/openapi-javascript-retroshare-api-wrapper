# RetroShareOpenApiWrapper.RsBroadcastDiscoveryResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mSslId** | **String** |  | [optional] 
**mProfileName** | **String** |  | [optional] 
**mLocator** | [**RsUrl**](RsUrl.md) |  | [optional] 


