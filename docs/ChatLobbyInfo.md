# RetroShareOpenApiWrapper.ChatLobbyInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 
**lobbyName** | **String** |  | [optional] 
**lobbyTopic** | **String** |  | [optional] 
**participatingFriends** | **[String]** |  | [optional] 
**gxsId** | **String** |  | [optional] 
**lobbyFlags** | **Number** |  | [optional] 
**gxsIds** | [**[ChatLobbyInfoGxsIds]**](ChatLobbyInfoGxsIds.md) |  | [optional] 
**lastActivity** | **Number** |  | [optional] 


