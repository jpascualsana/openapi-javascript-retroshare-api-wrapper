# RetroShareOpenApiWrapper.ReqRsIdentityUpdateIdentity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identityData** | [**RsGxsIdGroup**](RsGxsIdGroup.md) |  | [optional] 


