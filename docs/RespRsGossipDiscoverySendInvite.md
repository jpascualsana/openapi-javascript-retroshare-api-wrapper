# RetroShareOpenApiWrapper.RespRsGossipDiscoverySendInvite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**errorMessage** | **String** |  | [optional] 


