# RetroShareOpenApiWrapper.RespRsGxsChannelsMarkRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


