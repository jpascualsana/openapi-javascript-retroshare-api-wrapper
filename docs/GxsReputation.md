# RetroShareOpenApiWrapper.GxsReputation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mOverallScore** | **Number** |  | [optional] 
**mIdScore** | **Number** |  | [optional] 
**mOwnOpinion** | **Number** |  | [optional] 
**mPeerOpinion** | **Number** |  | [optional] 


