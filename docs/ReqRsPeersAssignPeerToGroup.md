# RetroShareOpenApiWrapper.ReqRsPeersAssignPeerToGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] 
**peerId** | **String** |  | [optional] 
**assign** | **Boolean** |  | [optional] 


