# RetroShareOpenApiWrapper.RespRsServiceControlGetServicesAllowed

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**info** | [**RsPeerServiceInfo**](RsPeerServiceInfo.md) |  | [optional] 


