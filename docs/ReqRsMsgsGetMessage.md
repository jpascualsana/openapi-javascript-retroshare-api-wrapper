# RetroShareOpenApiWrapper.ReqRsMsgsGetMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 


