# RetroShareOpenApiWrapper.ReqRsMsgsSetIdentityForChatLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 
**nick** | **String** |  | [optional] 


