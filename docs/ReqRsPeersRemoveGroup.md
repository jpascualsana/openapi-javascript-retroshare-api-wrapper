# RetroShareOpenApiWrapper.ReqRsPeersRemoveGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] 


