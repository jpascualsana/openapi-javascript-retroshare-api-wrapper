# RetroShareOpenApiWrapper.ReqRsMsgsInvitePeerToLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 
**peerId** | **String** |  | [optional] 


