# RetroShareOpenApiWrapper.RespRsFilesUpdateShareFlags

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


