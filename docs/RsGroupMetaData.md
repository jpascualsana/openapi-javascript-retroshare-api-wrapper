# RetroShareOpenApiWrapper.RsGroupMetaData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mGroupId** | **String** |  | [optional] 
**mGroupName** | **String** |  | [optional] 
**mGroupFlags** | **Number** |  | [optional] 
**mSignFlags** | **Number** |  | [optional] 
**mPublishTs** | **Number** |  | [optional] 
**mAuthorId** | **String** |  | [optional] 
**mCircleId** | **String** |  | [optional] 
**mCircleType** | **Number** |  | [optional] 
**mAuthenFlags** | **Number** |  | [optional] 
**mParentGrpId** | **String** |  | [optional] 
**mSubscribeFlags** | **Number** |  | [optional] 
**mPop** | **Number** |  | [optional] 
**mVisibleMsgCount** | **Number** |  | [optional] 
**mLastPost** | **Number** |  | [optional] 
**mGroupStatus** | **Number** |  | [optional] 
**mServiceString** | **String** |  | [optional] 
**mOriginator** | **String** |  | [optional] 
**mInternalCircle** | **String** |  | [optional] 


