# RetroShareOpenApiWrapper.ReqRsMsgsSetCustomStateString

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statusString** | **String** |  | [optional] 


