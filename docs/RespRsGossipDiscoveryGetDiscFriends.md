# RetroShareOpenApiWrapper.RespRsGossipDiscoveryGetDiscFriends

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**friends** | **[String]** |  | [optional] 


