# RetroShareOpenApiWrapper.ReqRsGxsChannelsGetChannelContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**contentsIds** | **[String]** |  | [optional] 


