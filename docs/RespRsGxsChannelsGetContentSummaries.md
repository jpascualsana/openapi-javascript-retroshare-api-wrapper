# RetroShareOpenApiWrapper.RespRsGxsChannelsGetContentSummaries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**summaries** | [**[RsMsgMetaData]**](RsMsgMetaData.md) |  | [optional] 


