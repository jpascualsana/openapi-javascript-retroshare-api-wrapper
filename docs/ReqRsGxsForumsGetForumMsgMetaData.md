# RetroShareOpenApiWrapper.ReqRsGxsForumsGetForumMsgMetaData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forumId** | **String** |  | [optional] 


