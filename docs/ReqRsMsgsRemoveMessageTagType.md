# RetroShareOpenApiWrapper.ReqRsMsgsRemoveMessageTagType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tagId** | **Number** |  | [optional] 


