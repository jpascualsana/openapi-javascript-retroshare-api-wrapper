# RetroShareOpenApiWrapper.RespRsBroadcastDiscoveryGetDiscoveredPeers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | [**[RsBroadcastDiscoveryResult]**](RsBroadcastDiscoveryResult.md) |  | [optional] 


