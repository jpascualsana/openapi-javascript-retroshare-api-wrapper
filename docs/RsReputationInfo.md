# RetroShareOpenApiWrapper.RsReputationInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mOwnOpinion** | [**RsOpinion**](RsOpinion.md) |  | [optional] 
**mFriendsPositiveVotes** | **Number** |  | [optional] 
**mFriendsNegativeVotes** | **Number** |  | [optional] 
**mFriendAverageScore** | **Number** |  | [optional] 
**mOverallReputationLevel** | [**RsReputationLevel**](RsReputationLevel.md) |  | [optional] 


