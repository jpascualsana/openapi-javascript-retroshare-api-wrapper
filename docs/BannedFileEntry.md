# RetroShareOpenApiWrapper.BannedFileEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mFilename** | **String** |  | [optional] 
**mSize** | **Number** |  | [optional] 
**mBanTimeStamp** | **Number** |  | [optional] 


