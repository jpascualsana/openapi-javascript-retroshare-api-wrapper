# RetroShareOpenApiWrapper.RespRsGxsChannelsCreateVoteV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**voteId** | **String** |  | [optional] 
**errorMessage** | **String** |  | [optional] 


