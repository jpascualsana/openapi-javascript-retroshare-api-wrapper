# RetroShareOpenApiWrapper.ReqRsGxsCirclesInviteIdsToCircle

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identities** | **[String]** |  | [optional] 
**circleId** | **String** |  | [optional] 


