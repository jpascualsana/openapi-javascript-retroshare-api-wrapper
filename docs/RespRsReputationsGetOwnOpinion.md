# RetroShareOpenApiWrapper.RespRsReputationsGetOwnOpinion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**op** | [**RsOpinion**](RsOpinion.md) |  | [optional] 


