# RetroShareOpenApiWrapper.RespRsGxsForumsGetForumsSummaries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**forums** | [**[RsGroupMetaData]**](RsGroupMetaData.md) |  | [optional] 


