# RetroShareOpenApiWrapper.ReqRsIdentityGetIdentitiesInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ids** | **[String]** |  | [optional] 


