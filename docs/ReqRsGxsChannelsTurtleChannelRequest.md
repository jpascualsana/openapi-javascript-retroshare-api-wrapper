# RetroShareOpenApiWrapper.ReqRsGxsChannelsTurtleChannelRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**maxWait** | **Number** |  | [optional] 


