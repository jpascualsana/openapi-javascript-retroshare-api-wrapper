# RetroShareOpenApiWrapper.RespRsReputationsIsIdentityBanned

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


