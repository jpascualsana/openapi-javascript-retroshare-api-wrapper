# RetroShareOpenApiWrapper.RespRsMsgsGetChatLobbyInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**info** | [**ChatLobbyInfo**](ChatLobbyInfo.md) |  | [optional] 


