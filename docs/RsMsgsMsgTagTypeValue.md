# RetroShareOpenApiWrapper.RsMsgsMsgTagTypeValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first** | **String** |  | [optional] 
**second** | **Number** |  | [optional] 


