# RetroShareOpenApiWrapper.ReqRsMsgsClearChatLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**ChatId**](ChatId.md) |  | [optional] 


