# RetroShareOpenApiWrapper.RsGxsFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mName** | **String** |  | [optional] 
**mHash** | **String** |  | [optional] 
**mSize** | **Number** |  | [optional] 


