# RetroShareOpenApiWrapper.ReqRsFilesSetDestinationDirectory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**newPath** | **String** |  | [optional] 


