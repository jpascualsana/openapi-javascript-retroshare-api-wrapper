# RetroShareOpenApiWrapper.ReqRsIdentityIdentityFromBase64

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base64String** | **String** |  | [optional] 


