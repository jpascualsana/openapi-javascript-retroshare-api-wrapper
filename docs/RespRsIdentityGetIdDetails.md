# RetroShareOpenApiWrapper.RespRsIdentityGetIdDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**details** | [**RsIdentityDetails**](RsIdentityDetails.md) |  | [optional] 


