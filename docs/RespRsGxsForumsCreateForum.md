# RetroShareOpenApiWrapper.RespRsGxsForumsCreateForum

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**forum** | [**RsGxsForumGroup**](RsGxsForumGroup.md) |  | [optional] 


