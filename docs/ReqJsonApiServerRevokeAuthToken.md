# RetroShareOpenApiWrapper.ReqJsonApiServerRevokeAuthToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  | [optional] 


