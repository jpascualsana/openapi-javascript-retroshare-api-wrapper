# RetroShareOpenApiWrapper.ReqRsFilesSetDefaultChunkStrategy

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strategy** | [**FileChunksInfoChunkStrategy**](FileChunksInfoChunkStrategy.md) |  | [optional] 


