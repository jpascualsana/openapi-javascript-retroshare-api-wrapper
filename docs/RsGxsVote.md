# RetroShareOpenApiWrapper.RsGxsVote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] 
**mVoteType** | **Number** |  | [optional] 


