# RetroShareOpenApiWrapper.RespRsFilesRequestDirDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**details** | [**DirDetails**](DirDetails.md) |  | [optional] 


