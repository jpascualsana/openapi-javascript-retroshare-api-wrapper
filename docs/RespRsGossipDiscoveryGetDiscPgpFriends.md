# RetroShareOpenApiWrapper.RespRsGossipDiscoveryGetDiscPgpFriends

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**gpgFriends** | **[String]** |  | [optional] 


