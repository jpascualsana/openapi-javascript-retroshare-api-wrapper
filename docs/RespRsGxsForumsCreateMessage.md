# RetroShareOpenApiWrapper.RespRsGxsForumsCreateMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**message** | [**RsGxsForumMsg**](RsGxsForumMsg.md) |  | [optional] 


