# RetroShareOpenApiWrapper.RespRsAccountsGetPGPLogins

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Number** |  | [optional] 
**pgpIds** | **[String]** |  | [optional] 


