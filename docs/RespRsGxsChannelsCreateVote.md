# RetroShareOpenApiWrapper.RespRsGxsChannelsCreateVote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**vote** | [**RsGxsVote**](RsGxsVote.md) |  | [optional] 


