# RetroShareOpenApiWrapper.ReqRsBanListEnableIPFiltering

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **Boolean** |  | [optional] 


