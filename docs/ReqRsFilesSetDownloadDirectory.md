# RetroShareOpenApiWrapper.ReqRsFilesSetDownloadDirectory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** |  | [optional] 


