# RetroShareOpenApiWrapper.RespRsGxsChannelsCreateComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**comment** | [**RsGxsComment**](RsGxsComment.md) |  | [optional] 


