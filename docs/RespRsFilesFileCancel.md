# RetroShareOpenApiWrapper.RespRsFilesFileCancel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


