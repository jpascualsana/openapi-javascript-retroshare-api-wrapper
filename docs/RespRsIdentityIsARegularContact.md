# RetroShareOpenApiWrapper.RespRsIdentityIsARegularContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


