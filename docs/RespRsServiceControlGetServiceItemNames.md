# RetroShareOpenApiWrapper.RespRsServiceControlGetServiceItemNames

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**names** | [**[RespRsServiceControlGetServiceItemNamesNames]**](RespRsServiceControlGetServiceItemNamesNames.md) |  | [optional] 


