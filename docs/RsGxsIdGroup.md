# RetroShareOpenApiWrapper.RsGxsIdGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] 
**mPgpIdHash** | **String** |  | [optional] 
**mPgpIdSign** | **String** |  | [optional] 
**mRecognTags** | **[String]** |  | [optional] 
**mImage** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**mLastUsageTS** | **Number** |  | [optional] 
**mPgpKnown** | **Boolean** |  | [optional] 
**mIsAContact** | **Boolean** |  | [optional] 
**mPgpId** | **String** |  | [optional] 
**mReputation** | [**GxsReputation**](GxsReputation.md) |  | [optional] 


