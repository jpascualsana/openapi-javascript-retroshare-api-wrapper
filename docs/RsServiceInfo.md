# RetroShareOpenApiWrapper.RsServiceInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mServiceName** | **String** |  | [optional] 
**mServiceType** | **Number** |  | [optional] 
**mVersionMajor** | **Number** |  | [optional] 
**mVersionMinor** | **Number** |  | [optional] 
**mMinVersionMajor** | **Number** |  | [optional] 
**mMinVersionMinor** | **Number** |  | [optional] 


