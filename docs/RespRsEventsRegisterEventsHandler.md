# RetroShareOpenApiWrapper.RespRsEventsRegisterEventsHandler

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**hId** | **Number** |  | [optional] 


