# RetroShareOpenApiWrapper.RespRsReputationsOverallReputationLevel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | [**RsReputationLevel**](RsReputationLevel.md) |  | [optional] 


