# RetroShareOpenApiWrapper.RespRsGxsChannelsCreateChannelV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**channelId** | **String** |  | [optional] 
**errorMessage** | **String** |  | [optional] 


