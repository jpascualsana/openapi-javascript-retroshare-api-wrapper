# RetroShareOpenApiWrapper.RespRsReputationsIsNodeBanned

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


