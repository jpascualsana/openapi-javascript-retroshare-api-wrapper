# RetroShareOpenApiWrapper.RespRsFilesFileClearCompleted

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


