# RetroShareOpenApiWrapper.ReqRsPeersGetGroupInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] 


