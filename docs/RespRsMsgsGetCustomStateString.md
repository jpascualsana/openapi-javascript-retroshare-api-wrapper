# RetroShareOpenApiWrapper.RespRsMsgsGetCustomStateString

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **String** |  | [optional] 


