# RetroShareOpenApiWrapper.RespRsLoginHelperCreateLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**location** | [**RsLoginHelperLocation**](RsLoginHelperLocation.md) |  | [optional] 
**errorMessage** | **String** |  | [optional] 


