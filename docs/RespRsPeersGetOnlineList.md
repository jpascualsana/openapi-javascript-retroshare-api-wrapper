# RetroShareOpenApiWrapper.RespRsPeersGetOnlineList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**sslIds** | **[String]** |  | [optional] 


