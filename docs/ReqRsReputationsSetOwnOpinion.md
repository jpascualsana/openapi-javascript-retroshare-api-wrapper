# RetroShareOpenApiWrapper.ReqRsReputationsSetOwnOpinion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**op** | [**RsOpinion**](RsOpinion.md) |  | [optional] 


