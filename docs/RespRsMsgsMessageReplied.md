# RetroShareOpenApiWrapper.RespRsMsgsMessageReplied

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


