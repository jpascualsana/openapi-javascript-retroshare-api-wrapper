# RetroShareOpenApiWrapper.ReqRsGxsChannelsGetChannelDownloadDirectory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 


