# RetroShareOpenApiWrapper.ReqJsonApiServerRequestNewTokenAutorization

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  | [optional] 


