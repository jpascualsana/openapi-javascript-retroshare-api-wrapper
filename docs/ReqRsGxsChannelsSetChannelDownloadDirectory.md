# RetroShareOpenApiWrapper.ReqRsGxsChannelsSetChannelDownloadDirectory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**directory** | **String** |  | [optional] 


