# RetroShareOpenApiWrapper.ReqRsMsgsMessageForwarded

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**forwarded** | **Boolean** |  | [optional] 


