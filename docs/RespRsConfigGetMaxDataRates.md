# RetroShareOpenApiWrapper.RespRsConfigGetMaxDataRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Number** |  | [optional] 
**inKb** | **Number** |  | [optional] 
**outKb** | **Number** |  | [optional] 


