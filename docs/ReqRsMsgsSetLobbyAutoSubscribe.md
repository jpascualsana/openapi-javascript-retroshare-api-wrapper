# RetroShareOpenApiWrapper.ReqRsMsgsSetLobbyAutoSubscribe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 
**autoSubscribe** | **Boolean** |  | [optional] 


