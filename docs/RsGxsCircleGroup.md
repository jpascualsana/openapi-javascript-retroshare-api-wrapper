# RetroShareOpenApiWrapper.RsGxsCircleGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] 
**mLocalFriends** | **[String]** |  | [optional] 
**mInvitedMembers** | **[String]** |  | [optional] 
**mSubCircles** | **[String]** |  | [optional] 


