# RetroShareOpenApiWrapper.ReqRsFilesSetFreeDiskSpaceLimit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**minimumFreeMB** | **Number** |  | [optional] 


