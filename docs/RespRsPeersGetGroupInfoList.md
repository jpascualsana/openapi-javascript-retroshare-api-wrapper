# RetroShareOpenApiWrapper.RespRsPeersGetGroupInfoList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**groupInfoList** | [**[RsGroupInfo]**](RsGroupInfo.md) |  | [optional] 


