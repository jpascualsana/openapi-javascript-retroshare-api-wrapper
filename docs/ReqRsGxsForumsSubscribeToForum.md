# RetroShareOpenApiWrapper.ReqRsGxsForumsSubscribeToForum

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forumId** | **String** |  | [optional] 
**subscribe** | **Boolean** |  | [optional] 


