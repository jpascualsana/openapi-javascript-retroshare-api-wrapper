# RetroShareOpenApiWrapper.ReqRsMsgsMessageDelete

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 


