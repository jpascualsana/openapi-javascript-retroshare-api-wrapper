# RetroShareOpenApiWrapper.RespRsPeersAcceptInvite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


