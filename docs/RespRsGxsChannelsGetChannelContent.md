# RetroShareOpenApiWrapper.RespRsGxsChannelsGetChannelContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**posts** | [**[RsGxsChannelPost]**](RsGxsChannelPost.md) |  | [optional] 
**comments** | [**[RsGxsComment]**](RsGxsComment.md) |  | [optional] 


