# RetroShareOpenApiWrapper.SharedDirInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** |  | [optional] 
**virtualname** | **String** |  | [optional] 
**shareflags** | **Number** |  | [optional] 
**parentGroups** | **[String]** |  | [optional] 


