# RetroShareOpenApiWrapper.RsGxsForumGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] 
**mDescription** | **String** |  | [optional] 
**mAdminList** | **String** |  | [optional] 
**mPinnedPosts** | **String** |  | [optional] 


