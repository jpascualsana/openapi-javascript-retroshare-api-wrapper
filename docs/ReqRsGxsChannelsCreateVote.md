# RetroShareOpenApiWrapper.ReqRsGxsChannelsCreateVote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vote** | [**RsGxsVote**](RsGxsVote.md) |  | [optional] 


