# RetroShareOpenApiWrapper.RsIdentityDetailsMUseCases

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | [**RsIdentityUsage**](RsIdentityUsage.md) |  | [optional] 
**value** | **Number** |  | [optional] 


