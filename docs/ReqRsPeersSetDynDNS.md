# RetroShareOpenApiWrapper.ReqRsPeersSetDynDNS

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] 
**addr** | **String** |  | [optional] 


