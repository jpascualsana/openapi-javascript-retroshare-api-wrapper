# RetroShareOpenApiWrapper.RsInitLoadCertificateStatus

## Enum


* `OK` (value: `0`)

* `ERR_ALREADY_RUNNING` (value: `1`)

* `ERR_CANT_ACQUIRE_LOCK` (value: `2`)

* `ERR_UNKOWN` (value: `3`)


