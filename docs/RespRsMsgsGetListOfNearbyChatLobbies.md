# RetroShareOpenApiWrapper.RespRsMsgsGetListOfNearbyChatLobbies

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**publicLobbies** | [**[VisibleChatLobbyRecord]**](VisibleChatLobbyRecord.md) |  | [optional] 


