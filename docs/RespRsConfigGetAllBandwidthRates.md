# RetroShareOpenApiWrapper.RespRsConfigGetAllBandwidthRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Number** |  | [optional] 
**ratemap** | [**[RespRsConfigGetAllBandwidthRatesRatemap]**](RespRsConfigGetAllBandwidthRatesRatemap.md) |  | [optional] 


