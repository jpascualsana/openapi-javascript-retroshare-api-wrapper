# RetroShareOpenApiWrapper.ReqRsGxsCirclesEditCircle

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cData** | [**RsGxsCircleGroup**](RsGxsCircleGroup.md) |  | [optional] 


