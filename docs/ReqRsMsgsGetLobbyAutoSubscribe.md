# RetroShareOpenApiWrapper.ReqRsMsgsGetLobbyAutoSubscribe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 


