# RetroShareOpenApiWrapper.RespRsConfigGetTrafficInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Number** |  | [optional] 
**outLst** | [**[RSTrafficClue]**](RSTrafficClue.md) |  | [optional] 
**inLst** | [**[RSTrafficClue]**](RSTrafficClue.md) |  | [optional] 


