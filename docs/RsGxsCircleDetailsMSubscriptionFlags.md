# RetroShareOpenApiWrapper.RsGxsCircleDetailsMSubscriptionFlags

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | [optional] 
**value** | **Number** |  | [optional] 


