# RetroShareOpenApiWrapper.FileInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**storagePermissionFlags** | **Number** |  | [optional] 
**transferInfoFlags** | **Number** |  | [optional] 
**mId** | **Number** |  | [optional] 
**searchId** | **Number** |  | [optional] 
**path** | **String** |  | [optional] 
**fname** | **String** |  | [optional] 
**hash** | **String** |  | [optional] 
**ext** | **String** |  | [optional] 
**size** | **Number** |  | [optional] 
**avail** | **Number** |  | [optional] 
**rank** | **Number** |  | [optional] 
**age** | **Number** |  | [optional] 
**queuePosition** | **Number** |  | [optional] 
**transfered** | **Number** |  | [optional] 
**tfRate** | **Number** |  | [optional] 
**downloadStatus** | **Number** |  | [optional] 
**peers** | [**[TransferInfo]**](TransferInfo.md) |  | [optional] 
**priority** | [**DwlSpeed**](DwlSpeed.md) |  | [optional] 
**lastTS** | **Number** |  | [optional] 
**parentGroups** | **[String]** |  | [optional] 


