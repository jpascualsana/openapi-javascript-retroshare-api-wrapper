# RetroShareOpenApiWrapper.RespRsPeersLoadCertificateFromString

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**sslId** | **String** |  | [optional] 
**pgpId** | **String** |  | [optional] 
**errorString** | **String** |  | [optional] 


