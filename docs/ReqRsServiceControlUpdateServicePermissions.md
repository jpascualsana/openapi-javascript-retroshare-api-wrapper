# RetroShareOpenApiWrapper.ReqRsServiceControlUpdateServicePermissions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serviceId** | **Number** |  | [optional] 
**permissions** | [**RsServicePermissions**](RsServicePermissions.md) |  | [optional] 


