# RetroShareOpenApiWrapper.ReqRsFilesRequestDirDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**handle** | **Number** |  | [optional] 
**flags** | **Number** |  | [optional] 


