# RetroShareOpenApiWrapper.RespRsAccountsExportIdentityToString

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**data** | **String** |  | [optional] 
**errorMsg** | **String** |  | [optional] 


