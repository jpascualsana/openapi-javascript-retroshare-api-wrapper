# RetroShareOpenApiWrapper.RsMsgsMsgInfoSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**srcId** | **String** |  | [optional] 
**msgflags** | **Number** |  | [optional] 
**msgtags** | **[Number]** |  | [optional] 
**title** | **String** |  | [optional] 
**count** | **Number** |  | [optional] 
**ts** | **Number** |  | [optional] 


