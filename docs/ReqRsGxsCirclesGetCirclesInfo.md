# RetroShareOpenApiWrapper.ReqRsGxsCirclesGetCirclesInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**circlesIds** | **[String]** |  | [optional] 


