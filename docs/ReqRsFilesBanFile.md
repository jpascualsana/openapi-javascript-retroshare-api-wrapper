# RetroShareOpenApiWrapper.ReqRsFilesBanFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**realFileHash** | **String** |  | [optional] 
**filename** | **String** |  | [optional] 
**fileSize** | **Number** |  | [optional] 


