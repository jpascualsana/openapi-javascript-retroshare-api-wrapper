# RetroShareOpenApiWrapper.DirDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | **Number** |  | [optional] 
**prow** | **Number** |  | [optional] 
**ref** | **Number** |  | [optional] 
**type** | **Number** |  | [optional] 
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**hash** | **String** |  | [optional] 
**path** | **String** |  | [optional] 
**count** | **Number** |  | [optional] 
**mtime** | **Number** |  | [optional] 
**flags** | **Number** |  | [optional] 
**maxMtime** | **Number** |  | [optional] 
**children** | [**[DirStub]**](DirStub.md) |  | [optional] 
**parentGroups** | **[String]** |  | [optional] 


