# RetroShareOpenApiWrapper.RespRsGxsForumsGetForumContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**msgs** | [**[RsGxsForumMsg]**](RsGxsForumMsg.md) |  | [optional] 


