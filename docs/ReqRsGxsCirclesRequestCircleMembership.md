# RetroShareOpenApiWrapper.ReqRsGxsCirclesRequestCircleMembership

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ownGxsId** | **String** |  | [optional] 
**circleId** | **String** |  | [optional] 


