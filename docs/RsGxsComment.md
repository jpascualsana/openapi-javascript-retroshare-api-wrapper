# RetroShareOpenApiWrapper.RsGxsComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] 
**mComment** | **String** |  | [optional] 
**mUpVotes** | **Number** |  | [optional] 
**mDownVotes** | **Number** |  | [optional] 
**mScore** | **Number** |  | [optional] 
**mOwnVote** | **Number** |  | [optional] 
**mVotes** | [**[RsGxsVote]**](RsGxsVote.md) |  | [optional] 


