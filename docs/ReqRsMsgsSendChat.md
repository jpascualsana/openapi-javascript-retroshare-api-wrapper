# RetroShareOpenApiWrapper.ReqRsMsgsSendChat

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**ChatId**](ChatId.md) |  | [optional] 
**msg** | **String** |  | [optional] 


