# RetroShareOpenApiWrapper.ReqRsGxsChannelsCreateVoteV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**postId** | **String** |  | [optional] 
**commentId** | **String** |  | [optional] 
**authorId** | **String** |  | [optional] 
**vote** | [**RsGxsVoteType**](RsGxsVoteType.md) |  | [optional] 


