# RetroShareOpenApiWrapper.RespRsGxsChannelsGetChannelAutoDownload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**enabled** | **Boolean** |  | [optional] 


