# RetroShareOpenApiWrapper.ReqRsMsgsSetMessageTagType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tagId** | **Number** |  | [optional] 
**text** | **String** |  | [optional] 
**rgbColor** | **Number** |  | [optional] 


