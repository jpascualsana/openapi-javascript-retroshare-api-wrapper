# RetroShareOpenApiWrapper.RespRsPeersIsOnline

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


