# RetroShareOpenApiWrapper.DefaultApi

All URIs are relative to *http://127.0.0.1:9092*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jsonApiServerAuthorizeToken**](DefaultApi.md#jsonApiServerAuthorizeToken) | **POST** /jsonApiServer/authorizeToken | Add new auth token to the authorized set
[**jsonApiServerDecodeToken**](DefaultApi.md#jsonApiServerDecodeToken) | **POST** /jsonApiServer/decodeToken | Get decoded version of the given encoded token
[**jsonApiServerEncondeToken**](DefaultApi.md#jsonApiServerEncondeToken) | **POST** /jsonApiServer/encondeToken | Get encoded version of the given decoded token
[**jsonApiServerGetAuthorizedTokens**](DefaultApi.md#jsonApiServerGetAuthorizedTokens) | **POST** /jsonApiServer/getAuthorizedTokens | Get uthorized tokens
[**jsonApiServerIsAuthTokenValid**](DefaultApi.md#jsonApiServerIsAuthTokenValid) | **POST** /jsonApiServer/isAuthTokenValid | Check if given JSON API auth token is authorized
[**jsonApiServerRequestNewTokenAutorization**](DefaultApi.md#jsonApiServerRequestNewTokenAutorization) | **POST** /jsonApiServer/requestNewTokenAutorization | This function should be used by JSON API clients that aren&#39;t authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.
[**jsonApiServerRevokeAuthToken**](DefaultApi.md#jsonApiServerRevokeAuthToken) | **POST** /jsonApiServer/revokeAuthToken | Revoke given auth token
[**jsonApiServerVersion**](DefaultApi.md#jsonApiServerVersion) | **POST** /jsonApiServer/version | Write version information to given paramethers
[**rsAccountsExportIdentity**](DefaultApi.md#rsAccountsExportIdentity) | **POST** /rsAccounts/ExportIdentity | Export full encrypted PGP identity to file
[**rsAccountsExportIdentityToString**](DefaultApi.md#rsAccountsExportIdentityToString) | **POST** /rsAccounts/exportIdentityToString | Export full encrypted PGP identity to string
[**rsAccountsGetCurrentAccountId**](DefaultApi.md#rsAccountsGetCurrentAccountId) | **POST** /rsAccounts/getCurrentAccountId | Get current account id. Beware that an account may be selected without actually logging in.
[**rsAccountsGetPGPLogins**](DefaultApi.md#rsAccountsGetPGPLogins) | **POST** /rsAccounts/GetPGPLogins | Get available PGP identities id list
[**rsAccountsImportIdentity**](DefaultApi.md#rsAccountsImportIdentity) | **POST** /rsAccounts/ImportIdentity | Import full encrypted PGP identity from file
[**rsAccountsImportIdentityFromString**](DefaultApi.md#rsAccountsImportIdentityFromString) | **POST** /rsAccounts/importIdentityFromString | Import full encrypted PGP identity from string
[**rsBanListEnableIPFiltering**](DefaultApi.md#rsBanListEnableIPFiltering) | **POST** /rsBanList/enableIPFiltering | Enable or disable IP filtering service
[**rsBanListIpFilteringEnabled**](DefaultApi.md#rsBanListIpFilteringEnabled) | **POST** /rsBanList/ipFilteringEnabled | Get ip filtering service status
[**rsBroadcastDiscoveryGetDiscoveredPeers**](DefaultApi.md#rsBroadcastDiscoveryGetDiscoveredPeers) | **POST** /rsBroadcastDiscovery/getDiscoveredPeers | Get potential peers that have been discovered up until now
[**rsConfigGetAllBandwidthRates**](DefaultApi.md#rsConfigGetAllBandwidthRates) | **POST** /rsConfig/getAllBandwidthRates | getAllBandwidthRates get the bandwidth rates for all peers
[**rsConfigGetConfigNetStatus**](DefaultApi.md#rsConfigGetConfigNetStatus) | **POST** /rsConfig/getConfigNetStatus | getConfigNetStatus return the net status
[**rsConfigGetCurrentDataRates**](DefaultApi.md#rsConfigGetCurrentDataRates) | **POST** /rsConfig/GetCurrentDataRates | GetCurrentDataRates get current upload and download rates
[**rsConfigGetMaxDataRates**](DefaultApi.md#rsConfigGetMaxDataRates) | **POST** /rsConfig/GetMaxDataRates | GetMaxDataRates get maximum upload and download rates
[**rsConfigGetOperatingMode**](DefaultApi.md#rsConfigGetOperatingMode) | **POST** /rsConfig/getOperatingMode | getOperatingMode get current operating mode
[**rsConfigGetTotalBandwidthRates**](DefaultApi.md#rsConfigGetTotalBandwidthRates) | **POST** /rsConfig/getTotalBandwidthRates | getTotalBandwidthRates returns the current bandwidths rates
[**rsConfigGetTrafficInfo**](DefaultApi.md#rsConfigGetTrafficInfo) | **POST** /rsConfig/getTrafficInfo | getTrafficInfo returns a list of all tracked traffic clues
[**rsConfigSetMaxDataRates**](DefaultApi.md#rsConfigSetMaxDataRates) | **POST** /rsConfig/SetMaxDataRates | SetMaxDataRates set maximum upload and download rates
[**rsConfigSetOperatingMode**](DefaultApi.md#rsConfigSetOperatingMode) | **POST** /rsConfig/setOperatingMode | setOperatingMode set the current oprating mode
[**rsControlIsReady**](DefaultApi.md#rsControlIsReady) | **POST** /rsControl/isReady | Check if core is fully ready, true only after
[**rsControlRsGlobalShutDown**](DefaultApi.md#rsControlRsGlobalShutDown) | **POST** /rsControl/rsGlobalShutDown | Turn off RetroShare
[**rsEventsRegisterEventsHandler**](DefaultApi.md#rsEventsRegisterEventsHandler) | **POST** /rsEvents/registerEventsHandler | This method is asynchronous. Register events handler Every time an event is dispatced the registered events handlers will get their method handleEvent called with the event passed as paramether.
[**rsFilesAddSharedDirectory**](DefaultApi.md#rsFilesAddSharedDirectory) | **POST** /rsFiles/addSharedDirectory | Add shared directory
[**rsFilesAlreadyHaveFile**](DefaultApi.md#rsFilesAlreadyHaveFile) | **POST** /rsFiles/alreadyHaveFile | Check if we already have a file
[**rsFilesBanFile**](DefaultApi.md#rsFilesBanFile) | **POST** /rsFiles/banFile | Ban unwanted file from being, searched and forwarded by this node
[**rsFilesDefaultChunkStrategy**](DefaultApi.md#rsFilesDefaultChunkStrategy) | **POST** /rsFiles/defaultChunkStrategy | Get default chunk strategy
[**rsFilesExtraFileHash**](DefaultApi.md#rsFilesExtraFileHash) | **POST** /rsFiles/ExtraFileHash | Add file to extra shared file list
[**rsFilesExtraFileRemove**](DefaultApi.md#rsFilesExtraFileRemove) | **POST** /rsFiles/ExtraFileRemove | Remove file from extra fila shared list
[**rsFilesExtraFileStatus**](DefaultApi.md#rsFilesExtraFileStatus) | **POST** /rsFiles/ExtraFileStatus | Get extra file information
[**rsFilesFileCancel**](DefaultApi.md#rsFilesFileCancel) | **POST** /rsFiles/FileCancel | Cancel file downloading
[**rsFilesFileClearCompleted**](DefaultApi.md#rsFilesFileClearCompleted) | **POST** /rsFiles/FileClearCompleted | Clear completed downloaded files list
[**rsFilesFileControl**](DefaultApi.md#rsFilesFileControl) | **POST** /rsFiles/FileControl | Controls file transfer
[**rsFilesFileDetails**](DefaultApi.md#rsFilesFileDetails) | **POST** /rsFiles/FileDetails | Get file details
[**rsFilesFileDownloadChunksDetails**](DefaultApi.md#rsFilesFileDownloadChunksDetails) | **POST** /rsFiles/FileDownloadChunksDetails | Get chunk details about the downloaded file with given hash.
[**rsFilesFileDownloads**](DefaultApi.md#rsFilesFileDownloads) | **POST** /rsFiles/FileDownloads | Get incoming files list
[**rsFilesFileRequest**](DefaultApi.md#rsFilesFileRequest) | **POST** /rsFiles/FileRequest | Initiate downloading of a file
[**rsFilesFileUploadChunksDetails**](DefaultApi.md#rsFilesFileUploadChunksDetails) | **POST** /rsFiles/FileUploadChunksDetails | Get details about the upload with given hash
[**rsFilesFileUploads**](DefaultApi.md#rsFilesFileUploads) | **POST** /rsFiles/FileUploads | Get outgoing files list
[**rsFilesForceDirectoryCheck**](DefaultApi.md#rsFilesForceDirectoryCheck) | **POST** /rsFiles/ForceDirectoryCheck | Force shared directories check
[**rsFilesFreeDiskSpaceLimit**](DefaultApi.md#rsFilesFreeDiskSpaceLimit) | **POST** /rsFiles/freeDiskSpaceLimit | Get free disk space limit
[**rsFilesGetDownloadDirectory**](DefaultApi.md#rsFilesGetDownloadDirectory) | **POST** /rsFiles/getDownloadDirectory | Get default complete downloads directory
[**rsFilesGetFileData**](DefaultApi.md#rsFilesGetFileData) | **POST** /rsFiles/getFileData | Provides file data for the gui, media streaming or rpc clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.
[**rsFilesGetPartialsDirectory**](DefaultApi.md#rsFilesGetPartialsDirectory) | **POST** /rsFiles/getPartialsDirectory | Get partial downloads directory
[**rsFilesGetPrimaryBannedFilesList**](DefaultApi.md#rsFilesGetPrimaryBannedFilesList) | **POST** /rsFiles/getPrimaryBannedFilesList | Get list of banned files
[**rsFilesGetSharedDirectories**](DefaultApi.md#rsFilesGetSharedDirectories) | **POST** /rsFiles/getSharedDirectories | Get list of current shared directories
[**rsFilesIsHashBanned**](DefaultApi.md#rsFilesIsHashBanned) | **POST** /rsFiles/isHashBanned | Check if a file is on banned list
[**rsFilesRemoveSharedDirectory**](DefaultApi.md#rsFilesRemoveSharedDirectory) | **POST** /rsFiles/removeSharedDirectory | Remove directory from shared list
[**rsFilesRequestDirDetails**](DefaultApi.md#rsFilesRequestDirDetails) | **POST** /rsFiles/requestDirDetails | Request directory details, subsequent multiple call may be used to explore a whole directory tree.
[**rsFilesSetChunkStrategy**](DefaultApi.md#rsFilesSetChunkStrategy) | **POST** /rsFiles/setChunkStrategy | Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading
[**rsFilesSetDefaultChunkStrategy**](DefaultApi.md#rsFilesSetDefaultChunkStrategy) | **POST** /rsFiles/setDefaultChunkStrategy | Set default chunk strategy
[**rsFilesSetDestinationDirectory**](DefaultApi.md#rsFilesSetDestinationDirectory) | **POST** /rsFiles/setDestinationDirectory | Set destination directory for given file
[**rsFilesSetDestinationName**](DefaultApi.md#rsFilesSetDestinationName) | **POST** /rsFiles/setDestinationName | Set name for dowloaded file
[**rsFilesSetDownloadDirectory**](DefaultApi.md#rsFilesSetDownloadDirectory) | **POST** /rsFiles/setDownloadDirectory | Set default complete downloads directory
[**rsFilesSetFreeDiskSpaceLimit**](DefaultApi.md#rsFilesSetFreeDiskSpaceLimit) | **POST** /rsFiles/setFreeDiskSpaceLimit | Set minimum free disk space limit
[**rsFilesSetPartialsDirectory**](DefaultApi.md#rsFilesSetPartialsDirectory) | **POST** /rsFiles/setPartialsDirectory | Set partial downloads directory
[**rsFilesSetSharedDirectories**](DefaultApi.md#rsFilesSetSharedDirectories) | **POST** /rsFiles/setSharedDirectories | Set shared directories
[**rsFilesTurtleSearchRequest**](DefaultApi.md#rsFilesTurtleSearchRequest) | **POST** /rsFiles/turtleSearchRequest | This method is asynchronous. Request remote files search
[**rsFilesUnbanFile**](DefaultApi.md#rsFilesUnbanFile) | **POST** /rsFiles/unbanFile | Remove file from unwanted list
[**rsFilesUpdateShareFlags**](DefaultApi.md#rsFilesUpdateShareFlags) | **POST** /rsFiles/updateShareFlags | Updates shared directory sharing flags. The directory should be already shared!
[**rsGossipDiscoveryGetDiscFriends**](DefaultApi.md#rsGossipDiscoveryGetDiscFriends) | **POST** /rsGossipDiscovery/getDiscFriends | getDiscFriends get a list of all friends of a given friend
[**rsGossipDiscoveryGetDiscPgpFriends**](DefaultApi.md#rsGossipDiscoveryGetDiscPgpFriends) | **POST** /rsGossipDiscovery/getDiscPgpFriends | getDiscPgpFriends get a list of all friends of a given friend
[**rsGossipDiscoveryGetPeerVersion**](DefaultApi.md#rsGossipDiscoveryGetPeerVersion) | **POST** /rsGossipDiscovery/getPeerVersion | getPeerVersion get the version string of a peer.
[**rsGossipDiscoveryGetWaitingDiscCount**](DefaultApi.md#rsGossipDiscoveryGetWaitingDiscCount) | **POST** /rsGossipDiscovery/getWaitingDiscCount | getWaitingDiscCount get the number of queued discovery packets.
[**rsGossipDiscoveryRequestInvite**](DefaultApi.md#rsGossipDiscoveryRequestInvite) | **POST** /rsGossipDiscovery/requestInvite | Request RetroShare certificate to given peer
[**rsGossipDiscoverySendInvite**](DefaultApi.md#rsGossipDiscoverySendInvite) | **POST** /rsGossipDiscovery/sendInvite | Send RetroShare invite to given peer
[**rsGxsChannelsCreateChannel**](DefaultApi.md#rsGxsChannelsCreateChannel) | **POST** /rsGxsChannels/createChannel | Deprecated{ substituted by createChannelV2 }
[**rsGxsChannelsCreateChannelV2**](DefaultApi.md#rsGxsChannelsCreateChannelV2) | **POST** /rsGxsChannels/createChannelV2 | Create channel. Blocking API.
[**rsGxsChannelsCreateComment**](DefaultApi.md#rsGxsChannelsCreateComment) | **POST** /rsGxsChannels/createComment | Deprecated
[**rsGxsChannelsCreateCommentV2**](DefaultApi.md#rsGxsChannelsCreateCommentV2) | **POST** /rsGxsChannels/createCommentV2 | Add a comment on a post or on another comment. Blocking API.
[**rsGxsChannelsCreatePost**](DefaultApi.md#rsGxsChannelsCreatePost) | **POST** /rsGxsChannels/createPost | Deprecated
[**rsGxsChannelsCreatePostV2**](DefaultApi.md#rsGxsChannelsCreatePostV2) | **POST** /rsGxsChannels/createPostV2 | Create channel post. Blocking API.
[**rsGxsChannelsCreateVote**](DefaultApi.md#rsGxsChannelsCreateVote) | **POST** /rsGxsChannels/createVote | Deprecated
[**rsGxsChannelsCreateVoteV2**](DefaultApi.md#rsGxsChannelsCreateVoteV2) | **POST** /rsGxsChannels/createVoteV2 | Create a vote
[**rsGxsChannelsEditChannel**](DefaultApi.md#rsGxsChannelsEditChannel) | **POST** /rsGxsChannels/editChannel | Edit channel details.
[**rsGxsChannelsExtraFileHash**](DefaultApi.md#rsGxsChannelsExtraFileHash) | **POST** /rsGxsChannels/ExtraFileHash | Share extra file Can be used to share extra file attached to a channel post
[**rsGxsChannelsExtraFileRemove**](DefaultApi.md#rsGxsChannelsExtraFileRemove) | **POST** /rsGxsChannels/ExtraFileRemove | Remove extra file from shared files
[**rsGxsChannelsGetChannelAutoDownload**](DefaultApi.md#rsGxsChannelsGetChannelAutoDownload) | **POST** /rsGxsChannels/getChannelAutoDownload | Get auto-download option value for given channel
[**rsGxsChannelsGetChannelContent**](DefaultApi.md#rsGxsChannelsGetChannelContent) | **POST** /rsGxsChannels/getChannelContent | Get channel contents
[**rsGxsChannelsGetChannelDownloadDirectory**](DefaultApi.md#rsGxsChannelsGetChannelDownloadDirectory) | **POST** /rsGxsChannels/getChannelDownloadDirectory | Get download directory for the given channel
[**rsGxsChannelsGetChannelsInfo**](DefaultApi.md#rsGxsChannelsGetChannelsInfo) | **POST** /rsGxsChannels/getChannelsInfo | Get channels information (description, thumbnail...). Blocking API.
[**rsGxsChannelsGetChannelsSummaries**](DefaultApi.md#rsGxsChannelsGetChannelsSummaries) | **POST** /rsGxsChannels/getChannelsSummaries | Get channels summaries list. Blocking API.
[**rsGxsChannelsGetContentSummaries**](DefaultApi.md#rsGxsChannelsGetContentSummaries) | **POST** /rsGxsChannels/getContentSummaries | Get channel content summaries
[**rsGxsChannelsLocalSearchRequest**](DefaultApi.md#rsGxsChannelsLocalSearchRequest) | **POST** /rsGxsChannels/localSearchRequest | This method is asynchronous. Search local channels
[**rsGxsChannelsMarkRead**](DefaultApi.md#rsGxsChannelsMarkRead) | **POST** /rsGxsChannels/markRead | Toggle post read status. Blocking API.
[**rsGxsChannelsRequestStatus**](DefaultApi.md#rsGxsChannelsRequestStatus) | **POST** /rsGxsChannels/requestStatus | null
[**rsGxsChannelsSetChannelAutoDownload**](DefaultApi.md#rsGxsChannelsSetChannelAutoDownload) | **POST** /rsGxsChannels/setChannelAutoDownload | Enable or disable auto-download for given channel. Blocking API
[**rsGxsChannelsSetChannelDownloadDirectory**](DefaultApi.md#rsGxsChannelsSetChannelDownloadDirectory) | **POST** /rsGxsChannels/setChannelDownloadDirectory | Set download directory for the given channel. Blocking API.
[**rsGxsChannelsShareChannelKeys**](DefaultApi.md#rsGxsChannelsShareChannelKeys) | **POST** /rsGxsChannels/shareChannelKeys | Share channel publishing key This can be used to authorize other peers to post on the channel
[**rsGxsChannelsSubscribeToChannel**](DefaultApi.md#rsGxsChannelsSubscribeToChannel) | **POST** /rsGxsChannels/subscribeToChannel | Subscrbe to a channel. Blocking API
[**rsGxsChannelsTurtleChannelRequest**](DefaultApi.md#rsGxsChannelsTurtleChannelRequest) | **POST** /rsGxsChannels/turtleChannelRequest | This method is asynchronous. Request remote channel
[**rsGxsChannelsTurtleSearchRequest**](DefaultApi.md#rsGxsChannelsTurtleSearchRequest) | **POST** /rsGxsChannels/turtleSearchRequest | This method is asynchronous. Request remote channels search
[**rsGxsCirclesCancelCircleMembership**](DefaultApi.md#rsGxsCirclesCancelCircleMembership) | **POST** /rsGxsCircles/cancelCircleMembership | Leave given circle
[**rsGxsCirclesCreateCircle**](DefaultApi.md#rsGxsCirclesCreateCircle) | **POST** /rsGxsCircles/createCircle | Create new circle
[**rsGxsCirclesEditCircle**](DefaultApi.md#rsGxsCirclesEditCircle) | **POST** /rsGxsCircles/editCircle | Edit own existing circle
[**rsGxsCirclesGetCircleDetails**](DefaultApi.md#rsGxsCirclesGetCircleDetails) | **POST** /rsGxsCircles/getCircleDetails | Get circle details. Memory cached
[**rsGxsCirclesGetCircleExternalIdList**](DefaultApi.md#rsGxsCirclesGetCircleExternalIdList) | **POST** /rsGxsCircles/getCircleExternalIdList | Get list of known external circles ids. Memory cached
[**rsGxsCirclesGetCircleRequests**](DefaultApi.md#rsGxsCirclesGetCircleRequests) | **POST** /rsGxsCircles/getCircleRequests | Get circle requests
[**rsGxsCirclesGetCirclesInfo**](DefaultApi.md#rsGxsCirclesGetCirclesInfo) | **POST** /rsGxsCircles/getCirclesInfo | Get circles information
[**rsGxsCirclesGetCirclesSummaries**](DefaultApi.md#rsGxsCirclesGetCirclesSummaries) | **POST** /rsGxsCircles/getCirclesSummaries | Get circles summaries list.
[**rsGxsCirclesInviteIdsToCircle**](DefaultApi.md#rsGxsCirclesInviteIdsToCircle) | **POST** /rsGxsCircles/inviteIdsToCircle | Invite identities to circle
[**rsGxsCirclesRequestCircleMembership**](DefaultApi.md#rsGxsCirclesRequestCircleMembership) | **POST** /rsGxsCircles/requestCircleMembership | Request circle membership, or accept circle invitation
[**rsGxsCirclesRequestStatus**](DefaultApi.md#rsGxsCirclesRequestStatus) | **POST** /rsGxsCircles/requestStatus | null
[**rsGxsForumsCreateForum**](DefaultApi.md#rsGxsForumsCreateForum) | **POST** /rsGxsForums/createForum | Deprecated
[**rsGxsForumsCreateForumV2**](DefaultApi.md#rsGxsForumsCreateForumV2) | **POST** /rsGxsForums/createForumV2 | Create forum.
[**rsGxsForumsCreateMessage**](DefaultApi.md#rsGxsForumsCreateMessage) | **POST** /rsGxsForums/createMessage | Deprecated
[**rsGxsForumsCreatePost**](DefaultApi.md#rsGxsForumsCreatePost) | **POST** /rsGxsForums/createPost | Create a post on the given forum.
[**rsGxsForumsEditForum**](DefaultApi.md#rsGxsForumsEditForum) | **POST** /rsGxsForums/editForum | Edit forum details.
[**rsGxsForumsGetForumContent**](DefaultApi.md#rsGxsForumsGetForumContent) | **POST** /rsGxsForums/getForumContent | Get specific list of messages from a single forums. Blocking API
[**rsGxsForumsGetForumMsgMetaData**](DefaultApi.md#rsGxsForumsGetForumMsgMetaData) | **POST** /rsGxsForums/getForumMsgMetaData | Get message metadatas for a specific forum. Blocking API
[**rsGxsForumsGetForumsInfo**](DefaultApi.md#rsGxsForumsGetForumsInfo) | **POST** /rsGxsForums/getForumsInfo | Get forums information (description, thumbnail...). Blocking API.
[**rsGxsForumsGetForumsSummaries**](DefaultApi.md#rsGxsForumsGetForumsSummaries) | **POST** /rsGxsForums/getForumsSummaries | Get forums summaries list. Blocking API.
[**rsGxsForumsMarkRead**](DefaultApi.md#rsGxsForumsMarkRead) | **POST** /rsGxsForums/markRead | Toggle message read status. Blocking API.
[**rsGxsForumsRequestStatus**](DefaultApi.md#rsGxsForumsRequestStatus) | **POST** /rsGxsForums/requestStatus | null
[**rsGxsForumsSubscribeToForum**](DefaultApi.md#rsGxsForumsSubscribeToForum) | **POST** /rsGxsForums/subscribeToForum | Subscrbe to a forum. Blocking API
[**rsIdentityAutoAddFriendIdsAsContact**](DefaultApi.md#rsIdentityAutoAddFriendIdsAsContact) | **POST** /rsIdentity/autoAddFriendIdsAsContact | Check if automatic signed by friend identity contact flagging is enabled
[**rsIdentityCreateIdentity**](DefaultApi.md#rsIdentityCreateIdentity) | **POST** /rsIdentity/createIdentity | Create a new identity
[**rsIdentityDeleteBannedNodesThreshold**](DefaultApi.md#rsIdentityDeleteBannedNodesThreshold) | **POST** /rsIdentity/deleteBannedNodesThreshold | Get number of days after which delete a banned identities
[**rsIdentityDeleteIdentity**](DefaultApi.md#rsIdentityDeleteIdentity) | **POST** /rsIdentity/deleteIdentity | Locally delete given identity
[**rsIdentityGetIdDetails**](DefaultApi.md#rsIdentityGetIdDetails) | **POST** /rsIdentity/getIdDetails | Get identity details, from the cache
[**rsIdentityGetIdentitiesInfo**](DefaultApi.md#rsIdentityGetIdentitiesInfo) | **POST** /rsIdentity/getIdentitiesInfo | Get identities information (name, avatar...). Blocking API.
[**rsIdentityGetIdentitiesSummaries**](DefaultApi.md#rsIdentityGetIdentitiesSummaries) | **POST** /rsIdentity/getIdentitiesSummaries | Get identities summaries list.
[**rsIdentityGetLastUsageTS**](DefaultApi.md#rsIdentityGetLastUsageTS) | **POST** /rsIdentity/getLastUsageTS | Get last seen usage time of given identity
[**rsIdentityGetOwnPseudonimousIds**](DefaultApi.md#rsIdentityGetOwnPseudonimousIds) | **POST** /rsIdentity/getOwnPseudonimousIds | Get own pseudonimous (unsigned) ids
[**rsIdentityGetOwnSignedIds**](DefaultApi.md#rsIdentityGetOwnSignedIds) | **POST** /rsIdentity/getOwnSignedIds | Get own signed ids
[**rsIdentityIdentityFromBase64**](DefaultApi.md#rsIdentityIdentityFromBase64) | **POST** /rsIdentity/identityFromBase64 | Import identity from base64 representation
[**rsIdentityIdentityToBase64**](DefaultApi.md#rsIdentityIdentityToBase64) | **POST** /rsIdentity/identityToBase64 | Get base64 representation of an identity
[**rsIdentityIsARegularContact**](DefaultApi.md#rsIdentityIsARegularContact) | **POST** /rsIdentity/isARegularContact | Check if an identity is contact
[**rsIdentityIsOwnId**](DefaultApi.md#rsIdentityIsOwnId) | **POST** /rsIdentity/isOwnId | Check if an id is own
[**rsIdentityRequestIdentity**](DefaultApi.md#rsIdentityRequestIdentity) | **POST** /rsIdentity/requestIdentity | request details of a not yet known identity to the network
[**rsIdentityRequestStatus**](DefaultApi.md#rsIdentityRequestStatus) | **POST** /rsIdentity/requestStatus | null
[**rsIdentitySetAsRegularContact**](DefaultApi.md#rsIdentitySetAsRegularContact) | **POST** /rsIdentity/setAsRegularContact | Set/unset identity as contact
[**rsIdentitySetAutoAddFriendIdsAsContact**](DefaultApi.md#rsIdentitySetAutoAddFriendIdsAsContact) | **POST** /rsIdentity/setAutoAddFriendIdsAsContact | Toggle automatic flagging signed by friends identity as contact
[**rsIdentitySetDeleteBannedNodesThreshold**](DefaultApi.md#rsIdentitySetDeleteBannedNodesThreshold) | **POST** /rsIdentity/setDeleteBannedNodesThreshold | Set number of days after which delete a banned identities
[**rsIdentityUpdateIdentity**](DefaultApi.md#rsIdentityUpdateIdentity) | **POST** /rsIdentity/updateIdentity | Update identity data (name, avatar...)
[**rsLoginHelperAttemptLogin**](DefaultApi.md#rsLoginHelperAttemptLogin) | **POST** /rsLoginHelper/attemptLogin | Normal way to attempt login
[**rsLoginHelperCollectEntropy**](DefaultApi.md#rsLoginHelperCollectEntropy) | **POST** /rsLoginHelper/collectEntropy | Feed extra entropy to the crypto libraries
[**rsLoginHelperCreateLocation**](DefaultApi.md#rsLoginHelperCreateLocation) | **POST** /rsLoginHelper/createLocation | Creates a new RetroShare location, and log in once is created
[**rsLoginHelperGetLocations**](DefaultApi.md#rsLoginHelperGetLocations) | **POST** /rsLoginHelper/getLocations | Get locations and associated information
[**rsLoginHelperIsLoggedIn**](DefaultApi.md#rsLoginHelperIsLoggedIn) | **POST** /rsLoginHelper/isLoggedIn | Check if RetroShare is already logged in, this usually return true after a successfull
[**rsMsgsAcceptLobbyInvite**](DefaultApi.md#rsMsgsAcceptLobbyInvite) | **POST** /rsMsgs/acceptLobbyInvite | acceptLobbyInvite accept a chat invite
[**rsMsgsClearChatLobby**](DefaultApi.md#rsMsgsClearChatLobby) | **POST** /rsMsgs/clearChatLobby | clearChatLobby clear a chat lobby
[**rsMsgsCreateChatLobby**](DefaultApi.md#rsMsgsCreateChatLobby) | **POST** /rsMsgs/createChatLobby | createChatLobby create a new chat lobby
[**rsMsgsDenyLobbyInvite**](DefaultApi.md#rsMsgsDenyLobbyInvite) | **POST** /rsMsgs/denyLobbyInvite | denyLobbyInvite deny a chat lobby invite
[**rsMsgsGetChatLobbyInfo**](DefaultApi.md#rsMsgsGetChatLobbyInfo) | **POST** /rsMsgs/getChatLobbyInfo | getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.
[**rsMsgsGetChatLobbyList**](DefaultApi.md#rsMsgsGetChatLobbyList) | **POST** /rsMsgs/getChatLobbyList | getChatLobbyList get ids of subscribed lobbies
[**rsMsgsGetCustomStateString**](DefaultApi.md#rsMsgsGetCustomStateString) | **POST** /rsMsgs/getCustomStateString | getCustomStateString get the custom status message from a peer
[**rsMsgsGetDefaultIdentityForChatLobby**](DefaultApi.md#rsMsgsGetDefaultIdentityForChatLobby) | **POST** /rsMsgs/getDefaultIdentityForChatLobby | getDefaultIdentityForChatLobby get the default identity used for chat lobbies
[**rsMsgsGetIdentityForChatLobby**](DefaultApi.md#rsMsgsGetIdentityForChatLobby) | **POST** /rsMsgs/getIdentityForChatLobby | getIdentityForChatLobby
[**rsMsgsGetListOfNearbyChatLobbies**](DefaultApi.md#rsMsgsGetListOfNearbyChatLobbies) | **POST** /rsMsgs/getListOfNearbyChatLobbies | getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed
[**rsMsgsGetLobbyAutoSubscribe**](DefaultApi.md#rsMsgsGetLobbyAutoSubscribe) | **POST** /rsMsgs/getLobbyAutoSubscribe | getLobbyAutoSubscribe get current value of auto subscribe
[**rsMsgsGetMaxMessageSecuritySize**](DefaultApi.md#rsMsgsGetMaxMessageSecuritySize) | **POST** /rsMsgs/getMaxMessageSecuritySize | getMaxMessageSecuritySize get the maximum size of a chta message
[**rsMsgsGetMessage**](DefaultApi.md#rsMsgsGetMessage) | **POST** /rsMsgs/getMessage | getMessage
[**rsMsgsGetMessageCount**](DefaultApi.md#rsMsgsGetMessageCount) | **POST** /rsMsgs/getMessageCount | getMessageCount
[**rsMsgsGetMessageSummaries**](DefaultApi.md#rsMsgsGetMessageSummaries) | **POST** /rsMsgs/getMessageSummaries | getMessageSummaries
[**rsMsgsGetMessageTag**](DefaultApi.md#rsMsgsGetMessageTag) | **POST** /rsMsgs/getMessageTag | getMessageTag
[**rsMsgsGetMessageTagTypes**](DefaultApi.md#rsMsgsGetMessageTagTypes) | **POST** /rsMsgs/getMessageTagTypes | getMessageTagTypes
[**rsMsgsGetMsgParentId**](DefaultApi.md#rsMsgsGetMsgParentId) | **POST** /rsMsgs/getMsgParentId | getMsgParentId
[**rsMsgsGetPendingChatLobbyInvites**](DefaultApi.md#rsMsgsGetPendingChatLobbyInvites) | **POST** /rsMsgs/getPendingChatLobbyInvites | getPendingChatLobbyInvites get a list of all pending chat lobby invites
[**rsMsgsInvitePeerToLobby**](DefaultApi.md#rsMsgsInvitePeerToLobby) | **POST** /rsMsgs/invitePeerToLobby | invitePeerToLobby invite a peer to join a lobby
[**rsMsgsJoinVisibleChatLobby**](DefaultApi.md#rsMsgsJoinVisibleChatLobby) | **POST** /rsMsgs/joinVisibleChatLobby | joinVisibleChatLobby join a lobby that is visible
[**rsMsgsMessageDelete**](DefaultApi.md#rsMsgsMessageDelete) | **POST** /rsMsgs/MessageDelete | MessageDelete
[**rsMsgsMessageForwarded**](DefaultApi.md#rsMsgsMessageForwarded) | **POST** /rsMsgs/MessageForwarded | MessageForwarded
[**rsMsgsMessageLoadEmbeddedImages**](DefaultApi.md#rsMsgsMessageLoadEmbeddedImages) | **POST** /rsMsgs/MessageLoadEmbeddedImages | MessageLoadEmbeddedImages
[**rsMsgsMessageRead**](DefaultApi.md#rsMsgsMessageRead) | **POST** /rsMsgs/MessageRead | MessageRead
[**rsMsgsMessageReplied**](DefaultApi.md#rsMsgsMessageReplied) | **POST** /rsMsgs/MessageReplied | MessageReplied
[**rsMsgsMessageSend**](DefaultApi.md#rsMsgsMessageSend) | **POST** /rsMsgs/MessageSend | MessageSend
[**rsMsgsMessageStar**](DefaultApi.md#rsMsgsMessageStar) | **POST** /rsMsgs/MessageStar | MessageStar
[**rsMsgsMessageToDraft**](DefaultApi.md#rsMsgsMessageToDraft) | **POST** /rsMsgs/MessageToDraft | MessageToDraft
[**rsMsgsMessageToTrash**](DefaultApi.md#rsMsgsMessageToTrash) | **POST** /rsMsgs/MessageToTrash | MessageToTrash
[**rsMsgsRemoveMessageTagType**](DefaultApi.md#rsMsgsRemoveMessageTagType) | **POST** /rsMsgs/removeMessageTagType | removeMessageTagType
[**rsMsgsResetMessageStandardTagTypes**](DefaultApi.md#rsMsgsResetMessageStandardTagTypes) | **POST** /rsMsgs/resetMessageStandardTagTypes | resetMessageStandardTagTypes
[**rsMsgsSendChat**](DefaultApi.md#rsMsgsSendChat) | **POST** /rsMsgs/sendChat | sendChat send a chat message to a given id
[**rsMsgsSendLobbyStatusPeerLeaving**](DefaultApi.md#rsMsgsSendLobbyStatusPeerLeaving) | **POST** /rsMsgs/sendLobbyStatusPeerLeaving | sendLobbyStatusPeerLeaving notify friend nodes that we&#39;re leaving a subscribed lobby
[**rsMsgsSendStatusString**](DefaultApi.md#rsMsgsSendStatusString) | **POST** /rsMsgs/sendStatusString | sendStatusString send a status string
[**rsMsgsSetCustomStateString**](DefaultApi.md#rsMsgsSetCustomStateString) | **POST** /rsMsgs/setCustomStateString | setCustomStateString set your custom status message
[**rsMsgsSetDefaultIdentityForChatLobby**](DefaultApi.md#rsMsgsSetDefaultIdentityForChatLobby) | **POST** /rsMsgs/setDefaultIdentityForChatLobby | setDefaultIdentityForChatLobby set the default identity used for chat lobbies
[**rsMsgsSetIdentityForChatLobby**](DefaultApi.md#rsMsgsSetIdentityForChatLobby) | **POST** /rsMsgs/setIdentityForChatLobby | setIdentityForChatLobby set the chat identit
[**rsMsgsSetLobbyAutoSubscribe**](DefaultApi.md#rsMsgsSetLobbyAutoSubscribe) | **POST** /rsMsgs/setLobbyAutoSubscribe | setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby
[**rsMsgsSetMessageTag**](DefaultApi.md#rsMsgsSetMessageTag) | **POST** /rsMsgs/setMessageTag | setMessageTag set &#x3D;&#x3D; false &amp;&amp; tagId &#x3D;&#x3D; 0
[**rsMsgsSetMessageTagType**](DefaultApi.md#rsMsgsSetMessageTagType) | **POST** /rsMsgs/setMessageTagType | setMessageTagType
[**rsMsgsSystemMessage**](DefaultApi.md#rsMsgsSystemMessage) | **POST** /rsMsgs/SystemMessage | SystemMessage
[**rsMsgsUnsubscribeChatLobby**](DefaultApi.md#rsMsgsUnsubscribeChatLobby) | **POST** /rsMsgs/unsubscribeChatLobby | unsubscribeChatLobby leave a chat lobby
[**rsPeersAcceptInvite**](DefaultApi.md#rsPeersAcceptInvite) | **POST** /rsPeers/acceptInvite | Add trusted node from invite
[**rsPeersAddFriend**](DefaultApi.md#rsPeersAddFriend) | **POST** /rsPeers/addFriend | Add trusted node
[**rsPeersAddGroup**](DefaultApi.md#rsPeersAddGroup) | **POST** /rsPeers/addGroup | addGroup create a new group
[**rsPeersAddPeerLocator**](DefaultApi.md#rsPeersAddPeerLocator) | **POST** /rsPeers/addPeerLocator | Add URL locator for given peer
[**rsPeersAssignPeerToGroup**](DefaultApi.md#rsPeersAssignPeerToGroup) | **POST** /rsPeers/assignPeerToGroup | assignPeerToGroup add a peer to a group
[**rsPeersAssignPeersToGroup**](DefaultApi.md#rsPeersAssignPeersToGroup) | **POST** /rsPeers/assignPeersToGroup | assignPeersToGroup add a list of peers to a group
[**rsPeersConnectAttempt**](DefaultApi.md#rsPeersConnectAttempt) | **POST** /rsPeers/connectAttempt | Trigger connection attempt to given node
[**rsPeersEditGroup**](DefaultApi.md#rsPeersEditGroup) | **POST** /rsPeers/editGroup | editGroup edit an existing group
[**rsPeersGetFriendList**](DefaultApi.md#rsPeersGetFriendList) | **POST** /rsPeers/getFriendList | Get trusted peers list
[**rsPeersGetGPGId**](DefaultApi.md#rsPeersGetGPGId) | **POST** /rsPeers/getGPGId | Get PGP id for the given peer
[**rsPeersGetGroupInfo**](DefaultApi.md#rsPeersGetGroupInfo) | **POST** /rsPeers/getGroupInfo | getGroupInfo get group information to one group
[**rsPeersGetGroupInfoByName**](DefaultApi.md#rsPeersGetGroupInfoByName) | **POST** /rsPeers/getGroupInfoByName | getGroupInfoByName get group information by group name
[**rsPeersGetGroupInfoList**](DefaultApi.md#rsPeersGetGroupInfoList) | **POST** /rsPeers/getGroupInfoList | getGroupInfoList get list of all groups
[**rsPeersGetOnlineList**](DefaultApi.md#rsPeersGetOnlineList) | **POST** /rsPeers/getOnlineList | Get connected peers list
[**rsPeersGetPeerDetails**](DefaultApi.md#rsPeersGetPeerDetails) | **POST** /rsPeers/getPeerDetails | Get details details of the given peer
[**rsPeersGetPeersCount**](DefaultApi.md#rsPeersGetPeersCount) | **POST** /rsPeers/getPeersCount | Get peers count
[**rsPeersGetRetroshareInvite**](DefaultApi.md#rsPeersGetRetroshareInvite) | **POST** /rsPeers/GetRetroshareInvite | Get RetroShare invite of the given peer
[**rsPeersIsFriend**](DefaultApi.md#rsPeersIsFriend) | **POST** /rsPeers/isFriend | Check if given peer is a trusted node
[**rsPeersIsOnline**](DefaultApi.md#rsPeersIsOnline) | **POST** /rsPeers/isOnline | Check if there is an established connection to the given peer
[**rsPeersIsPgpFriend**](DefaultApi.md#rsPeersIsPgpFriend) | **POST** /rsPeers/isPgpFriend | Check if given PGP id is trusted
[**rsPeersLoadCertificateFromString**](DefaultApi.md#rsPeersLoadCertificateFromString) | **POST** /rsPeers/loadCertificateFromString | Import certificate into the keyring
[**rsPeersLoadDetailsFromStringCert**](DefaultApi.md#rsPeersLoadDetailsFromStringCert) | **POST** /rsPeers/loadDetailsFromStringCert | Examine certificate and get details without importing into the keyring
[**rsPeersRemoveFriend**](DefaultApi.md#rsPeersRemoveFriend) | **POST** /rsPeers/removeFriend | Revoke connection trust from to node
[**rsPeersRemoveFriendLocation**](DefaultApi.md#rsPeersRemoveFriendLocation) | **POST** /rsPeers/removeFriendLocation | Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust
[**rsPeersRemoveGroup**](DefaultApi.md#rsPeersRemoveGroup) | **POST** /rsPeers/removeGroup | removeGroup remove a group
[**rsPeersSetDynDNS**](DefaultApi.md#rsPeersSetDynDNS) | **POST** /rsPeers/setDynDNS | Set (dynamical) domain name associated to the given peer
[**rsPeersSetExtAddress**](DefaultApi.md#rsPeersSetExtAddress) | **POST** /rsPeers/setExtAddress | Set external IPv4 address for given peer
[**rsPeersSetLocalAddress**](DefaultApi.md#rsPeersSetLocalAddress) | **POST** /rsPeers/setLocalAddress | Set local IPv4 address for the given peer
[**rsPeersSetNetworkMode**](DefaultApi.md#rsPeersSetNetworkMode) | **POST** /rsPeers/setNetworkMode | Set network mode of the given peer
[**rsPeersSetVisState**](DefaultApi.md#rsPeersSetVisState) | **POST** /rsPeers/setVisState | set DHT and discovery modes
[**rsReputationsAutoPositiveOpinionForContacts**](DefaultApi.md#rsReputationsAutoPositiveOpinionForContacts) | **POST** /rsReputations/autoPositiveOpinionForContacts | check if giving automatic positive opinion when flagging as contact is enbaled
[**rsReputationsBanNode**](DefaultApi.md#rsReputationsBanNode) | **POST** /rsReputations/banNode | Enable automatic banning of all identities signed by the given node
[**rsReputationsGetOwnOpinion**](DefaultApi.md#rsReputationsGetOwnOpinion) | **POST** /rsReputations/getOwnOpinion | Get own opition about the given identity
[**rsReputationsGetReputationInfo**](DefaultApi.md#rsReputationsGetReputationInfo) | **POST** /rsReputations/getReputationInfo | Get reputation data of given identity
[**rsReputationsIsIdentityBanned**](DefaultApi.md#rsReputationsIsIdentityBanned) | **POST** /rsReputations/isIdentityBanned | This method allow fast checking if a GXS identity is banned.
[**rsReputationsIsNodeBanned**](DefaultApi.md#rsReputationsIsNodeBanned) | **POST** /rsReputations/isNodeBanned | Check if automatic banning of all identities signed by the given node is enabled
[**rsReputationsOverallReputationLevel**](DefaultApi.md#rsReputationsOverallReputationLevel) | **POST** /rsReputations/overallReputationLevel | Get overall reputation level of given identity
[**rsReputationsRememberBannedIdThreshold**](DefaultApi.md#rsReputationsRememberBannedIdThreshold) | **POST** /rsReputations/rememberBannedIdThreshold | Get number of days to wait before deleting a banned identity from local storage
[**rsReputationsSetAutoPositiveOpinionForContacts**](DefaultApi.md#rsReputationsSetAutoPositiveOpinionForContacts) | **POST** /rsReputations/setAutoPositiveOpinionForContacts | Enable giving automatic positive opinion when flagging as contact
[**rsReputationsSetOwnOpinion**](DefaultApi.md#rsReputationsSetOwnOpinion) | **POST** /rsReputations/setOwnOpinion | Set own opinion about the given identity
[**rsReputationsSetRememberBannedIdThreshold**](DefaultApi.md#rsReputationsSetRememberBannedIdThreshold) | **POST** /rsReputations/setRememberBannedIdThreshold | Set number of days to wait before deleting a banned identity from local storage
[**rsReputationsSetThresholdForRemotelyNegativeReputation**](DefaultApi.md#rsReputationsSetThresholdForRemotelyNegativeReputation) | **POST** /rsReputations/setThresholdForRemotelyNegativeReputation | Set threshold on remote reputation to consider it remotely negative
[**rsReputationsSetThresholdForRemotelyPositiveReputation**](DefaultApi.md#rsReputationsSetThresholdForRemotelyPositiveReputation) | **POST** /rsReputations/setThresholdForRemotelyPositiveReputation | Set threshold on remote reputation to consider it remotely positive
[**rsReputationsThresholdForRemotelyNegativeReputation**](DefaultApi.md#rsReputationsThresholdForRemotelyNegativeReputation) | **POST** /rsReputations/thresholdForRemotelyNegativeReputation | Get threshold on remote reputation to consider it remotely negative
[**rsReputationsThresholdForRemotelyPositiveReputation**](DefaultApi.md#rsReputationsThresholdForRemotelyPositiveReputation) | **POST** /rsReputations/thresholdForRemotelyPositiveReputation | Get threshold on remote reputation to consider it remotely negative
[**rsServiceControlGetOwnServices**](DefaultApi.md#rsServiceControlGetOwnServices) | **POST** /rsServiceControl/getOwnServices | get a map off all services.
[**rsServiceControlGetPeersConnected**](DefaultApi.md#rsServiceControlGetPeersConnected) | **POST** /rsServiceControl/getPeersConnected | getPeersConnected return peers using a service.
[**rsServiceControlGetServiceItemNames**](DefaultApi.md#rsServiceControlGetServiceItemNames) | **POST** /rsServiceControl/getServiceItemNames | getServiceItemNames return a map of service item names.
[**rsServiceControlGetServiceName**](DefaultApi.md#rsServiceControlGetServiceName) | **POST** /rsServiceControl/getServiceName | getServiceName lookup the name of a service.
[**rsServiceControlGetServicePermissions**](DefaultApi.md#rsServiceControlGetServicePermissions) | **POST** /rsServiceControl/getServicePermissions | getServicePermissions return permissions of one service.
[**rsServiceControlGetServicesAllowed**](DefaultApi.md#rsServiceControlGetServicesAllowed) | **POST** /rsServiceControl/getServicesAllowed | getServicesAllowed return a mpa with allowed service information.
[**rsServiceControlGetServicesProvided**](DefaultApi.md#rsServiceControlGetServicesProvided) | **POST** /rsServiceControl/getServicesProvided | getServicesProvided return services provided by a peer.
[**rsServiceControlUpdateServicePermissions**](DefaultApi.md#rsServiceControlUpdateServicePermissions) | **POST** /rsServiceControl/updateServicePermissions | updateServicePermissions update service permissions of one service.



## jsonApiServerAuthorizeToken

> RespJsonApiServerAuthorizeToken jsonApiServerAuthorizeToken(opts)

Add new auth token to the authorized set

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqJsonApiServerAuthorizeToken': {"token":"(string)toke to autorize decoded \n"} // ReqJsonApiServerAuthorizeToken | token: >              (string)toke to autorize decoded  
};
apiInstance.jsonApiServerAuthorizeToken(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqJsonApiServerAuthorizeToken** | [**ReqJsonApiServerAuthorizeToken**](ReqJsonApiServerAuthorizeToken.md)| token: &gt;              (string)toke to autorize decoded   | [optional] 

### Return type

[**RespJsonApiServerAuthorizeToken**](RespJsonApiServerAuthorizeToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## jsonApiServerDecodeToken

> RespJsonApiServerDecodeToken jsonApiServerDecodeToken(opts)

Get decoded version of the given encoded token

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqJsonApiServerDecodeToken': {"token":"(string)encoded \n"} // ReqJsonApiServerDecodeToken | token: >              (string)encoded  
};
apiInstance.jsonApiServerDecodeToken(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqJsonApiServerDecodeToken** | [**ReqJsonApiServerDecodeToken**](ReqJsonApiServerDecodeToken.md)| token: &gt;              (string)encoded   | [optional] 

### Return type

[**RespJsonApiServerDecodeToken**](RespJsonApiServerDecodeToken.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## jsonApiServerEncondeToken

> RespJsonApiServerEncondeToken jsonApiServerEncondeToken(opts)

Get encoded version of the given decoded token

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqJsonApiServerEncondeToken': {"token":"(string)decoded \n"} // ReqJsonApiServerEncondeToken | token: >              (string)decoded  
};
apiInstance.jsonApiServerEncondeToken(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqJsonApiServerEncondeToken** | [**ReqJsonApiServerEncondeToken**](ReqJsonApiServerEncondeToken.md)| token: &gt;              (string)decoded   | [optional] 

### Return type

[**RespJsonApiServerEncondeToken**](RespJsonApiServerEncondeToken.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## jsonApiServerGetAuthorizedTokens

> RespJsonApiServerGetAuthorizedTokens jsonApiServerGetAuthorizedTokens()

Get uthorized tokens

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.jsonApiServerGetAuthorizedTokens((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespJsonApiServerGetAuthorizedTokens**](RespJsonApiServerGetAuthorizedTokens.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## jsonApiServerIsAuthTokenValid

> RespJsonApiServerIsAuthTokenValid jsonApiServerIsAuthTokenValid(opts)

Check if given JSON API auth token is authorized

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqJsonApiServerIsAuthTokenValid': {"token":"(string)decoded \n"} // ReqJsonApiServerIsAuthTokenValid | token: >              (string)decoded  
};
apiInstance.jsonApiServerIsAuthTokenValid(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqJsonApiServerIsAuthTokenValid** | [**ReqJsonApiServerIsAuthTokenValid**](ReqJsonApiServerIsAuthTokenValid.md)| token: &gt;              (string)decoded   | [optional] 

### Return type

[**RespJsonApiServerIsAuthTokenValid**](RespJsonApiServerIsAuthTokenValid.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## jsonApiServerRequestNewTokenAutorization

> RespJsonApiServerRequestNewTokenAutorization jsonApiServerRequestNewTokenAutorization(opts)

This function should be used by JSON API clients that aren&#39;t authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqJsonApiServerRequestNewTokenAutorization': {"token":"(string)token to autorize \n"} // ReqJsonApiServerRequestNewTokenAutorization | token: >              (string)token to autorize  
};
apiInstance.jsonApiServerRequestNewTokenAutorization(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqJsonApiServerRequestNewTokenAutorization** | [**ReqJsonApiServerRequestNewTokenAutorization**](ReqJsonApiServerRequestNewTokenAutorization.md)| token: &gt;              (string)token to autorize   | [optional] 

### Return type

[**RespJsonApiServerRequestNewTokenAutorization**](RespJsonApiServerRequestNewTokenAutorization.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## jsonApiServerRevokeAuthToken

> RespJsonApiServerRevokeAuthToken jsonApiServerRevokeAuthToken(opts)

Revoke given auth token

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqJsonApiServerRevokeAuthToken': {"token":"(string)decoded \n"} // ReqJsonApiServerRevokeAuthToken | token: >              (string)decoded  
};
apiInstance.jsonApiServerRevokeAuthToken(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqJsonApiServerRevokeAuthToken** | [**ReqJsonApiServerRevokeAuthToken**](ReqJsonApiServerRevokeAuthToken.md)| token: &gt;              (string)decoded   | [optional] 

### Return type

[**RespJsonApiServerRevokeAuthToken**](RespJsonApiServerRevokeAuthToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## jsonApiServerVersion

> RespJsonApiServerVersion jsonApiServerVersion()

Write version information to given paramethers

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.jsonApiServerVersion((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespJsonApiServerVersion**](RespJsonApiServerVersion.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsAccountsExportIdentity

> RespRsAccountsExportIdentity rsAccountsExportIdentity(opts)

Export full encrypted PGP identity to file

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsAccountsExportIdentity': {"filePath":"(string)path of certificate file \n","pgpId":"(RsPgpId)PGP id to export \n"} // ReqRsAccountsExportIdentity | filePath: >              (string)path of certificate file          pgpId: >              (RsPgpId)PGP id to export  
};
apiInstance.rsAccountsExportIdentity(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsAccountsExportIdentity** | [**ReqRsAccountsExportIdentity**](ReqRsAccountsExportIdentity.md)| filePath: &gt;              (string)path of certificate file          pgpId: &gt;              (RsPgpId)PGP id to export   | [optional] 

### Return type

[**RespRsAccountsExportIdentity**](RespRsAccountsExportIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsAccountsExportIdentityToString

> RespRsAccountsExportIdentityToString rsAccountsExportIdentityToString(opts)

Export full encrypted PGP identity to string

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsAccountsExportIdentityToString': {"pgpId":"(RsPgpId)PGP id to export \n","includeSignatures":"(boolean)true to include signatures \n"} // ReqRsAccountsExportIdentityToString | pgpId: >              (RsPgpId)PGP id to export          includeSignatures: >              (boolean)true to include signatures  
};
apiInstance.rsAccountsExportIdentityToString(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsAccountsExportIdentityToString** | [**ReqRsAccountsExportIdentityToString**](ReqRsAccountsExportIdentityToString.md)| pgpId: &gt;              (RsPgpId)PGP id to export          includeSignatures: &gt;              (boolean)true to include signatures   | [optional] 

### Return type

[**RespRsAccountsExportIdentityToString**](RespRsAccountsExportIdentityToString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsAccountsGetCurrentAccountId

> RespRsAccountsGetCurrentAccountId rsAccountsGetCurrentAccountId()

Get current account id. Beware that an account may be selected without actually logging in.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsAccountsGetCurrentAccountId((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsAccountsGetCurrentAccountId**](RespRsAccountsGetCurrentAccountId.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsAccountsGetPGPLogins

> RespRsAccountsGetPGPLogins rsAccountsGetPGPLogins()

Get available PGP identities id list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsAccountsGetPGPLogins((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsAccountsGetPGPLogins**](RespRsAccountsGetPGPLogins.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsAccountsImportIdentity

> RespRsAccountsImportIdentity rsAccountsImportIdentity(opts)

Import full encrypted PGP identity from file

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsAccountsImportIdentity': {"filePath":"(string)path of certificate file \n"} // ReqRsAccountsImportIdentity | filePath: >              (string)path of certificate file  
};
apiInstance.rsAccountsImportIdentity(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsAccountsImportIdentity** | [**ReqRsAccountsImportIdentity**](ReqRsAccountsImportIdentity.md)| filePath: &gt;              (string)path of certificate file   | [optional] 

### Return type

[**RespRsAccountsImportIdentity**](RespRsAccountsImportIdentity.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsAccountsImportIdentityFromString

> RespRsAccountsImportIdentityFromString rsAccountsImportIdentityFromString(opts)

Import full encrypted PGP identity from string

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsAccountsImportIdentityFromString': {"data":"(string)certificate string \n"} // ReqRsAccountsImportIdentityFromString | data: >              (string)certificate string  
};
apiInstance.rsAccountsImportIdentityFromString(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsAccountsImportIdentityFromString** | [**ReqRsAccountsImportIdentityFromString**](ReqRsAccountsImportIdentityFromString.md)| data: &gt;              (string)certificate string   | [optional] 

### Return type

[**RespRsAccountsImportIdentityFromString**](RespRsAccountsImportIdentityFromString.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsBanListEnableIPFiltering

> Object rsBanListEnableIPFiltering(opts)

Enable or disable IP filtering service

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsBanListEnableIPFiltering': {"enable":"(boolean)pass true to enable, false to disable \n"} // ReqRsBanListEnableIPFiltering | enable: >              (boolean)pass true to enable, false to disable  
};
apiInstance.rsBanListEnableIPFiltering(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsBanListEnableIPFiltering** | [**ReqRsBanListEnableIPFiltering**](ReqRsBanListEnableIPFiltering.md)| enable: &gt;              (boolean)pass true to enable, false to disable   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsBanListIpFilteringEnabled

> RespRsBanListIpFilteringEnabled rsBanListIpFilteringEnabled()

Get ip filtering service status

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsBanListIpFilteringEnabled((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsBanListIpFilteringEnabled**](RespRsBanListIpFilteringEnabled.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsBroadcastDiscoveryGetDiscoveredPeers

> RespRsBroadcastDiscoveryGetDiscoveredPeers rsBroadcastDiscoveryGetDiscoveredPeers()

Get potential peers that have been discovered up until now

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsBroadcastDiscoveryGetDiscoveredPeers((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsBroadcastDiscoveryGetDiscoveredPeers**](RespRsBroadcastDiscoveryGetDiscoveredPeers.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsConfigGetAllBandwidthRates

> RespRsConfigGetAllBandwidthRates rsConfigGetAllBandwidthRates()

getAllBandwidthRates get the bandwidth rates for all peers

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsConfigGetAllBandwidthRates((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetAllBandwidthRates**](RespRsConfigGetAllBandwidthRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsConfigGetConfigNetStatus

> RespRsConfigGetConfigNetStatus rsConfigGetConfigNetStatus()

getConfigNetStatus return the net status

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsConfigGetConfigNetStatus((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetConfigNetStatus**](RespRsConfigGetConfigNetStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsConfigGetCurrentDataRates

> RespRsConfigGetCurrentDataRates rsConfigGetCurrentDataRates()

GetCurrentDataRates get current upload and download rates

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsConfigGetCurrentDataRates((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetCurrentDataRates**](RespRsConfigGetCurrentDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsConfigGetMaxDataRates

> RespRsConfigGetMaxDataRates rsConfigGetMaxDataRates()

GetMaxDataRates get maximum upload and download rates

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsConfigGetMaxDataRates((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetMaxDataRates**](RespRsConfigGetMaxDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsConfigGetOperatingMode

> RespRsConfigGetOperatingMode rsConfigGetOperatingMode()

getOperatingMode get current operating mode

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsConfigGetOperatingMode((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetOperatingMode**](RespRsConfigGetOperatingMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsConfigGetTotalBandwidthRates

> RespRsConfigGetTotalBandwidthRates rsConfigGetTotalBandwidthRates()

getTotalBandwidthRates returns the current bandwidths rates

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsConfigGetTotalBandwidthRates((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetTotalBandwidthRates**](RespRsConfigGetTotalBandwidthRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsConfigGetTrafficInfo

> RespRsConfigGetTrafficInfo rsConfigGetTrafficInfo()

getTrafficInfo returns a list of all tracked traffic clues

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsConfigGetTrafficInfo((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsConfigGetTrafficInfo**](RespRsConfigGetTrafficInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsConfigSetMaxDataRates

> RespRsConfigSetMaxDataRates rsConfigSetMaxDataRates(opts)

SetMaxDataRates set maximum upload and download rates

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsConfigSetMaxDataRates': {"downKb":"(integer)download rate in kB \n","upKb":"(integer)upload rate in kB \n"} // ReqRsConfigSetMaxDataRates | downKb: >              (integer)download rate in kB          upKb: >              (integer)upload rate in kB  
};
apiInstance.rsConfigSetMaxDataRates(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsConfigSetMaxDataRates** | [**ReqRsConfigSetMaxDataRates**](ReqRsConfigSetMaxDataRates.md)| downKb: &gt;              (integer)download rate in kB          upKb: &gt;              (integer)upload rate in kB   | [optional] 

### Return type

[**RespRsConfigSetMaxDataRates**](RespRsConfigSetMaxDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsConfigSetOperatingMode

> RespRsConfigSetOperatingMode rsConfigSetOperatingMode(opts)

setOperatingMode set the current oprating mode

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsConfigSetOperatingMode': {"opMode":"(integer)new opearting mode \n"} // ReqRsConfigSetOperatingMode | opMode: >              (integer)new opearting mode  
};
apiInstance.rsConfigSetOperatingMode(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsConfigSetOperatingMode** | [**ReqRsConfigSetOperatingMode**](ReqRsConfigSetOperatingMode.md)| opMode: &gt;              (integer)new opearting mode   | [optional] 

### Return type

[**RespRsConfigSetOperatingMode**](RespRsConfigSetOperatingMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsControlIsReady

> RespRsControlIsReady rsControlIsReady()

Check if core is fully ready, true only after

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsControlIsReady((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsControlIsReady**](RespRsControlIsReady.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsControlRsGlobalShutDown

> Object rsControlRsGlobalShutDown()

Turn off RetroShare

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsControlRsGlobalShutDown((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsEventsRegisterEventsHandler

> RespRsEventsRegisterEventsHandler rsEventsRegisterEventsHandler(opts)

This method is asynchronous. Register events handler Every time an event is dispatced the registered events handlers will get their method handleEvent called with the event passed as paramether.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsEventsRegisterEventsHandler': {"hId":"(RsEventsHandlerId_t)Optional storage for handler id, useful to eventually unregister the handler later. The value may be provided to the function call but must habe been generated with \n"} // ReqRsEventsRegisterEventsHandler | hId: >              (RsEventsHandlerId_t)Optional storage for handler id, useful to eventually unregister the handler later. The value may be provided to the function call but must habe been generated with  
};
apiInstance.rsEventsRegisterEventsHandler(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsEventsRegisterEventsHandler** | [**ReqRsEventsRegisterEventsHandler**](ReqRsEventsRegisterEventsHandler.md)| hId: &gt;              (RsEventsHandlerId_t)Optional storage for handler id, useful to eventually unregister the handler later. The value may be provided to the function call but must habe been generated with   | [optional] 

### Return type

[**RespRsEventsRegisterEventsHandler**](RespRsEventsRegisterEventsHandler.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesAddSharedDirectory

> RespRsFilesAddSharedDirectory rsFilesAddSharedDirectory(opts)

Add shared directory

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesAddSharedDirectory': {"dir":"(SharedDirInfo)directory to share with sharing options \n"} // ReqRsFilesAddSharedDirectory | dir: >              (SharedDirInfo)directory to share with sharing options  
};
apiInstance.rsFilesAddSharedDirectory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesAddSharedDirectory** | [**ReqRsFilesAddSharedDirectory**](ReqRsFilesAddSharedDirectory.md)| dir: &gt;              (SharedDirInfo)directory to share with sharing options   | [optional] 

### Return type

[**RespRsFilesAddSharedDirectory**](RespRsFilesAddSharedDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesAlreadyHaveFile

> RespRsFilesAlreadyHaveFile rsFilesAlreadyHaveFile(opts)

Check if we already have a file

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesAlreadyHaveFile': {"hash":"(RsFileHash)file identifier \n"} // ReqRsFilesAlreadyHaveFile | hash: >              (RsFileHash)file identifier  
};
apiInstance.rsFilesAlreadyHaveFile(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesAlreadyHaveFile** | [**ReqRsFilesAlreadyHaveFile**](ReqRsFilesAlreadyHaveFile.md)| hash: &gt;              (RsFileHash)file identifier   | [optional] 

### Return type

[**RespRsFilesAlreadyHaveFile**](RespRsFilesAlreadyHaveFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesBanFile

> RespRsFilesBanFile rsFilesBanFile(opts)

Ban unwanted file from being, searched and forwarded by this node

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesBanFile': {"realFileHash":"(RsFileHash)this is what will really enforce banning \n","filename":"(string)expected name of the file, for the user to read \n","fileSize":"(integer)expected file size, for the user to read \n"} // ReqRsFilesBanFile | realFileHash: >              (RsFileHash)this is what will really enforce banning          filename: >              (string)expected name of the file, for the user to read          fileSize: >              (integer)expected file size, for the user to read  
};
apiInstance.rsFilesBanFile(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesBanFile** | [**ReqRsFilesBanFile**](ReqRsFilesBanFile.md)| realFileHash: &gt;              (RsFileHash)this is what will really enforce banning          filename: &gt;              (string)expected name of the file, for the user to read          fileSize: &gt;              (integer)expected file size, for the user to read   | [optional] 

### Return type

[**RespRsFilesBanFile**](RespRsFilesBanFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesDefaultChunkStrategy

> RespRsFilesDefaultChunkStrategy rsFilesDefaultChunkStrategy()

Get default chunk strategy

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesDefaultChunkStrategy((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesDefaultChunkStrategy**](RespRsFilesDefaultChunkStrategy.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesExtraFileHash

> RespRsFilesExtraFileHash rsFilesExtraFileHash(opts)

Add file to extra shared file list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesExtraFileHash': {"localpath":"(string)path of the file \n","period":"(rstime_t)how much time the file will be kept in extra list in seconds \n","flags":"(TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING \n"} // ReqRsFilesExtraFileHash | localpath: >              (string)path of the file          period: >              (rstime_t)how much time the file will be kept in extra list in seconds          flags: >              (TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING  
};
apiInstance.rsFilesExtraFileHash(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesExtraFileHash** | [**ReqRsFilesExtraFileHash**](ReqRsFilesExtraFileHash.md)| localpath: &gt;              (string)path of the file          period: &gt;              (rstime_t)how much time the file will be kept in extra list in seconds          flags: &gt;              (TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING   | [optional] 

### Return type

[**RespRsFilesExtraFileHash**](RespRsFilesExtraFileHash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesExtraFileRemove

> RespRsFilesExtraFileRemove rsFilesExtraFileRemove(opts)

Remove file from extra fila shared list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesExtraFileRemove': {"hash":"(RsFileHash)hash of the file to remove \n"} // ReqRsFilesExtraFileRemove | hash: >              (RsFileHash)hash of the file to remove  
};
apiInstance.rsFilesExtraFileRemove(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesExtraFileRemove** | [**ReqRsFilesExtraFileRemove**](ReqRsFilesExtraFileRemove.md)| hash: &gt;              (RsFileHash)hash of the file to remove   | [optional] 

### Return type

[**RespRsFilesExtraFileRemove**](RespRsFilesExtraFileRemove.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesExtraFileStatus

> RespRsFilesExtraFileStatus rsFilesExtraFileStatus(opts)

Get extra file information

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesExtraFileStatus': {"localpath":"(string)path of the file \n"} // ReqRsFilesExtraFileStatus | localpath: >              (string)path of the file  
};
apiInstance.rsFilesExtraFileStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesExtraFileStatus** | [**ReqRsFilesExtraFileStatus**](ReqRsFilesExtraFileStatus.md)| localpath: &gt;              (string)path of the file   | [optional] 

### Return type

[**RespRsFilesExtraFileStatus**](RespRsFilesExtraFileStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesFileCancel

> RespRsFilesFileCancel rsFilesFileCancel(opts)

Cancel file downloading

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesFileCancel': {"hash":"(RsFileHash)None\n"} // ReqRsFilesFileCancel | hash: >              (RsFileHash)None 
};
apiInstance.rsFilesFileCancel(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesFileCancel** | [**ReqRsFilesFileCancel**](ReqRsFilesFileCancel.md)| hash: &gt;              (RsFileHash)None  | [optional] 

### Return type

[**RespRsFilesFileCancel**](RespRsFilesFileCancel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesFileClearCompleted

> RespRsFilesFileClearCompleted rsFilesFileClearCompleted()

Clear completed downloaded files list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesFileClearCompleted((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesFileClearCompleted**](RespRsFilesFileClearCompleted.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesFileControl

> RespRsFilesFileControl rsFilesFileControl(opts)

Controls file transfer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesFileControl': {"hash":"(RsFileHash)file identifier \n","flags":"(integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } } \n"} // ReqRsFilesFileControl | hash: >              (RsFileHash)file identifier          flags: >              (integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } }  
};
apiInstance.rsFilesFileControl(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesFileControl** | [**ReqRsFilesFileControl**](ReqRsFilesFileControl.md)| hash: &gt;              (RsFileHash)file identifier          flags: &gt;              (integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } }   | [optional] 

### Return type

[**RespRsFilesFileControl**](RespRsFilesFileControl.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesFileDetails

> RespRsFilesFileDetails rsFilesFileDetails(opts)

Get file details

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesFileDetails': {"hash":"(RsFileHash)file identifier \n","hintflags":"(FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL) \n"} // ReqRsFilesFileDetails | hash: >              (RsFileHash)file identifier          hintflags: >              (FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL)  
};
apiInstance.rsFilesFileDetails(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesFileDetails** | [**ReqRsFilesFileDetails**](ReqRsFilesFileDetails.md)| hash: &gt;              (RsFileHash)file identifier          hintflags: &gt;              (FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL)   | [optional] 

### Return type

[**RespRsFilesFileDetails**](RespRsFilesFileDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesFileDownloadChunksDetails

> RespRsFilesFileDownloadChunksDetails rsFilesFileDownloadChunksDetails(opts)

Get chunk details about the downloaded file with given hash.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesFileDownloadChunksDetails': {"hash":"(RsFileHash)file identifier \n"} // ReqRsFilesFileDownloadChunksDetails | hash: >              (RsFileHash)file identifier  
};
apiInstance.rsFilesFileDownloadChunksDetails(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesFileDownloadChunksDetails** | [**ReqRsFilesFileDownloadChunksDetails**](ReqRsFilesFileDownloadChunksDetails.md)| hash: &gt;              (RsFileHash)file identifier   | [optional] 

### Return type

[**RespRsFilesFileDownloadChunksDetails**](RespRsFilesFileDownloadChunksDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesFileDownloads

> RespRsFilesFileDownloads rsFilesFileDownloads()

Get incoming files list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesFileDownloads((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesFileDownloads**](RespRsFilesFileDownloads.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesFileRequest

> RespRsFilesFileRequest rsFilesFileRequest(opts)

Initiate downloading of a file

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesFileRequest': {"fileName":"(string)None\n","hash":"(RsFileHash)None\n","size":"(integer)None\n","destPath":"(string)in not empty specify a destination path \n","flags":"(TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING \n","srcIds":"(list<RsPeerId>)eventually specify known sources \n"} // ReqRsFilesFileRequest | fileName: >              (string)None         hash: >              (RsFileHash)None         size: >              (integer)None         destPath: >              (string)in not empty specify a destination path          flags: >              (TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING          srcIds: >              (list<RsPeerId>)eventually specify known sources  
};
apiInstance.rsFilesFileRequest(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesFileRequest** | [**ReqRsFilesFileRequest**](ReqRsFilesFileRequest.md)| fileName: &gt;              (string)None         hash: &gt;              (RsFileHash)None         size: &gt;              (integer)None         destPath: &gt;              (string)in not empty specify a destination path          flags: &gt;              (TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING          srcIds: &gt;              (list&lt;RsPeerId&gt;)eventually specify known sources   | [optional] 

### Return type

[**RespRsFilesFileRequest**](RespRsFilesFileRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesFileUploadChunksDetails

> RespRsFilesFileUploadChunksDetails rsFilesFileUploadChunksDetails(opts)

Get details about the upload with given hash

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesFileUploadChunksDetails': {"hash":"(RsFileHash)file identifier \n","peerId":"(RsPeerId)peer identifier \n"} // ReqRsFilesFileUploadChunksDetails | hash: >              (RsFileHash)file identifier          peerId: >              (RsPeerId)peer identifier  
};
apiInstance.rsFilesFileUploadChunksDetails(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesFileUploadChunksDetails** | [**ReqRsFilesFileUploadChunksDetails**](ReqRsFilesFileUploadChunksDetails.md)| hash: &gt;              (RsFileHash)file identifier          peerId: &gt;              (RsPeerId)peer identifier   | [optional] 

### Return type

[**RespRsFilesFileUploadChunksDetails**](RespRsFilesFileUploadChunksDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesFileUploads

> RespRsFilesFileUploads rsFilesFileUploads()

Get outgoing files list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesFileUploads((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesFileUploads**](RespRsFilesFileUploads.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesForceDirectoryCheck

> Object rsFilesForceDirectoryCheck()

Force shared directories check

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesForceDirectoryCheck((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesFreeDiskSpaceLimit

> RespRsFilesFreeDiskSpaceLimit rsFilesFreeDiskSpaceLimit()

Get free disk space limit

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesFreeDiskSpaceLimit((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesFreeDiskSpaceLimit**](RespRsFilesFreeDiskSpaceLimit.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesGetDownloadDirectory

> RespRsFilesGetDownloadDirectory rsFilesGetDownloadDirectory()

Get default complete downloads directory

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesGetDownloadDirectory((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesGetDownloadDirectory**](RespRsFilesGetDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesGetFileData

> RespRsFilesGetFileData rsFilesGetFileData(opts)

Provides file data for the gui, media streaming or rpc clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesGetFileData': {"hash":"(RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state. \n","offset":"(integer)where the desired block starts \n","requested_size":"(integer)size of pre-allocated data. Will be updated by the function. \n"} // ReqRsFilesGetFileData | hash: >              (RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state.          offset: >              (integer)where the desired block starts          requested_size: >              (integer)size of pre-allocated data. Will be updated by the function.  
};
apiInstance.rsFilesGetFileData(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesGetFileData** | [**ReqRsFilesGetFileData**](ReqRsFilesGetFileData.md)| hash: &gt;              (RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state.          offset: &gt;              (integer)where the desired block starts          requested_size: &gt;              (integer)size of pre-allocated data. Will be updated by the function.   | [optional] 

### Return type

[**RespRsFilesGetFileData**](RespRsFilesGetFileData.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesGetPartialsDirectory

> RespRsFilesGetPartialsDirectory rsFilesGetPartialsDirectory()

Get partial downloads directory

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesGetPartialsDirectory((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesGetPartialsDirectory**](RespRsFilesGetPartialsDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesGetPrimaryBannedFilesList

> RespRsFilesGetPrimaryBannedFilesList rsFilesGetPrimaryBannedFilesList()

Get list of banned files

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesGetPrimaryBannedFilesList((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesGetPrimaryBannedFilesList**](RespRsFilesGetPrimaryBannedFilesList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesGetSharedDirectories

> RespRsFilesGetSharedDirectories rsFilesGetSharedDirectories()

Get list of current shared directories

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsFilesGetSharedDirectories((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsFilesGetSharedDirectories**](RespRsFilesGetSharedDirectories.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsFilesIsHashBanned

> RespRsFilesIsHashBanned rsFilesIsHashBanned(opts)

Check if a file is on banned list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesIsHashBanned': {"hash":"(RsFileHash)hash of the file \n"} // ReqRsFilesIsHashBanned | hash: >              (RsFileHash)hash of the file  
};
apiInstance.rsFilesIsHashBanned(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesIsHashBanned** | [**ReqRsFilesIsHashBanned**](ReqRsFilesIsHashBanned.md)| hash: &gt;              (RsFileHash)hash of the file   | [optional] 

### Return type

[**RespRsFilesIsHashBanned**](RespRsFilesIsHashBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesRemoveSharedDirectory

> RespRsFilesRemoveSharedDirectory rsFilesRemoveSharedDirectory(opts)

Remove directory from shared list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesRemoveSharedDirectory': {"dir":"(string)Path of the directory to remove from shared list \n"} // ReqRsFilesRemoveSharedDirectory | dir: >              (string)Path of the directory to remove from shared list  
};
apiInstance.rsFilesRemoveSharedDirectory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesRemoveSharedDirectory** | [**ReqRsFilesRemoveSharedDirectory**](ReqRsFilesRemoveSharedDirectory.md)| dir: &gt;              (string)Path of the directory to remove from shared list   | [optional] 

### Return type

[**RespRsFilesRemoveSharedDirectory**](RespRsFilesRemoveSharedDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesRequestDirDetails

> RespRsFilesRequestDirDetails rsFilesRequestDirDetails(opts)

Request directory details, subsequent multiple call may be used to explore a whole directory tree.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesRequestDirDetails': {"handle":"(integer)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare. \n","flags":"(FileSearchFlags)file search flags RS_FILE_HINTS_* \n"} // ReqRsFilesRequestDirDetails | handle: >              (integer)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare.          flags: >              (FileSearchFlags)file search flags RS_FILE_HINTS_*  
};
apiInstance.rsFilesRequestDirDetails(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesRequestDirDetails** | [**ReqRsFilesRequestDirDetails**](ReqRsFilesRequestDirDetails.md)| handle: &gt;              (integer)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare.          flags: &gt;              (FileSearchFlags)file search flags RS_FILE_HINTS_*   | [optional] 

### Return type

[**RespRsFilesRequestDirDetails**](RespRsFilesRequestDirDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesSetChunkStrategy

> RespRsFilesSetChunkStrategy rsFilesSetChunkStrategy(opts)

Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesSetChunkStrategy': {"hash":"(RsFileHash)file identifier \n","newStrategy":"(FileChunksInfo_ChunkStrategy)None\n"} // ReqRsFilesSetChunkStrategy | hash: >              (RsFileHash)file identifier          newStrategy: >              (FileChunksInfo_ChunkStrategy)None 
};
apiInstance.rsFilesSetChunkStrategy(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesSetChunkStrategy** | [**ReqRsFilesSetChunkStrategy**](ReqRsFilesSetChunkStrategy.md)| hash: &gt;              (RsFileHash)file identifier          newStrategy: &gt;              (FileChunksInfo_ChunkStrategy)None  | [optional] 

### Return type

[**RespRsFilesSetChunkStrategy**](RespRsFilesSetChunkStrategy.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesSetDefaultChunkStrategy

> Object rsFilesSetDefaultChunkStrategy(opts)

Set default chunk strategy

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesSetDefaultChunkStrategy': {"strategy":"(FileChunksInfo_ChunkStrategy)None\n"} // ReqRsFilesSetDefaultChunkStrategy | strategy: >              (FileChunksInfo_ChunkStrategy)None 
};
apiInstance.rsFilesSetDefaultChunkStrategy(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesSetDefaultChunkStrategy** | [**ReqRsFilesSetDefaultChunkStrategy**](ReqRsFilesSetDefaultChunkStrategy.md)| strategy: &gt;              (FileChunksInfo_ChunkStrategy)None  | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesSetDestinationDirectory

> RespRsFilesSetDestinationDirectory rsFilesSetDestinationDirectory(opts)

Set destination directory for given file

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesSetDestinationDirectory': {"hash":"(RsFileHash)file identifier \n","newPath":"(string)None\n"} // ReqRsFilesSetDestinationDirectory | hash: >              (RsFileHash)file identifier          newPath: >              (string)None 
};
apiInstance.rsFilesSetDestinationDirectory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesSetDestinationDirectory** | [**ReqRsFilesSetDestinationDirectory**](ReqRsFilesSetDestinationDirectory.md)| hash: &gt;              (RsFileHash)file identifier          newPath: &gt;              (string)None  | [optional] 

### Return type

[**RespRsFilesSetDestinationDirectory**](RespRsFilesSetDestinationDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesSetDestinationName

> RespRsFilesSetDestinationName rsFilesSetDestinationName(opts)

Set name for dowloaded file

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesSetDestinationName': {"hash":"(RsFileHash)file identifier \n","newName":"(string)None\n"} // ReqRsFilesSetDestinationName | hash: >              (RsFileHash)file identifier          newName: >              (string)None 
};
apiInstance.rsFilesSetDestinationName(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesSetDestinationName** | [**ReqRsFilesSetDestinationName**](ReqRsFilesSetDestinationName.md)| hash: &gt;              (RsFileHash)file identifier          newName: &gt;              (string)None  | [optional] 

### Return type

[**RespRsFilesSetDestinationName**](RespRsFilesSetDestinationName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesSetDownloadDirectory

> RespRsFilesSetDownloadDirectory rsFilesSetDownloadDirectory(opts)

Set default complete downloads directory

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesSetDownloadDirectory': {"path":"(string)directory path \n"} // ReqRsFilesSetDownloadDirectory | path: >              (string)directory path  
};
apiInstance.rsFilesSetDownloadDirectory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesSetDownloadDirectory** | [**ReqRsFilesSetDownloadDirectory**](ReqRsFilesSetDownloadDirectory.md)| path: &gt;              (string)directory path   | [optional] 

### Return type

[**RespRsFilesSetDownloadDirectory**](RespRsFilesSetDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesSetFreeDiskSpaceLimit

> Object rsFilesSetFreeDiskSpaceLimit(opts)

Set minimum free disk space limit

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesSetFreeDiskSpaceLimit': {"minimumFreeMB":"(integer)minimum free space in MB \n"} // ReqRsFilesSetFreeDiskSpaceLimit | minimumFreeMB: >              (integer)minimum free space in MB  
};
apiInstance.rsFilesSetFreeDiskSpaceLimit(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesSetFreeDiskSpaceLimit** | [**ReqRsFilesSetFreeDiskSpaceLimit**](ReqRsFilesSetFreeDiskSpaceLimit.md)| minimumFreeMB: &gt;              (integer)minimum free space in MB   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesSetPartialsDirectory

> RespRsFilesSetPartialsDirectory rsFilesSetPartialsDirectory(opts)

Set partial downloads directory

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesSetPartialsDirectory': {"path":"(string)directory path \n"} // ReqRsFilesSetPartialsDirectory | path: >              (string)directory path  
};
apiInstance.rsFilesSetPartialsDirectory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesSetPartialsDirectory** | [**ReqRsFilesSetPartialsDirectory**](ReqRsFilesSetPartialsDirectory.md)| path: &gt;              (string)directory path   | [optional] 

### Return type

[**RespRsFilesSetPartialsDirectory**](RespRsFilesSetPartialsDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesSetSharedDirectories

> RespRsFilesSetSharedDirectories rsFilesSetSharedDirectories(opts)

Set shared directories

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesSetSharedDirectories': {"dirs":"(list<SharedDirInfo>)list of shared directories with share options \n"} // ReqRsFilesSetSharedDirectories | dirs: >              (list<SharedDirInfo>)list of shared directories with share options  
};
apiInstance.rsFilesSetSharedDirectories(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesSetSharedDirectories** | [**ReqRsFilesSetSharedDirectories**](ReqRsFilesSetSharedDirectories.md)| dirs: &gt;              (list&lt;SharedDirInfo&gt;)list of shared directories with share options   | [optional] 

### Return type

[**RespRsFilesSetSharedDirectories**](RespRsFilesSetSharedDirectories.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesTurtleSearchRequest

> RespRsFilesTurtleSearchRequest rsFilesTurtleSearchRequest(opts)

This method is asynchronous. Request remote files search

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesTurtleSearchRequest': {"matchString":"(string)string to look for in the search \n","maxWait":"(rstime_t)maximum wait time in seconds for search results \n"} // ReqRsFilesTurtleSearchRequest | matchString: >              (string)string to look for in the search          maxWait: >              (rstime_t)maximum wait time in seconds for search results  
};
apiInstance.rsFilesTurtleSearchRequest(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesTurtleSearchRequest** | [**ReqRsFilesTurtleSearchRequest**](ReqRsFilesTurtleSearchRequest.md)| matchString: &gt;              (string)string to look for in the search          maxWait: &gt;              (rstime_t)maximum wait time in seconds for search results   | [optional] 

### Return type

[**RespRsFilesTurtleSearchRequest**](RespRsFilesTurtleSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesUnbanFile

> RespRsFilesUnbanFile rsFilesUnbanFile(opts)

Remove file from unwanted list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesUnbanFile': {"realFileHash":"(RsFileHash)hash of the file \n"} // ReqRsFilesUnbanFile | realFileHash: >              (RsFileHash)hash of the file  
};
apiInstance.rsFilesUnbanFile(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesUnbanFile** | [**ReqRsFilesUnbanFile**](ReqRsFilesUnbanFile.md)| realFileHash: &gt;              (RsFileHash)hash of the file   | [optional] 

### Return type

[**RespRsFilesUnbanFile**](RespRsFilesUnbanFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsFilesUpdateShareFlags

> RespRsFilesUpdateShareFlags rsFilesUpdateShareFlags(opts)

Updates shared directory sharing flags. The directory should be already shared!

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsFilesUpdateShareFlags': {"dir":"(SharedDirInfo)Shared directory with updated sharing options \n"} // ReqRsFilesUpdateShareFlags | dir: >              (SharedDirInfo)Shared directory with updated sharing options  
};
apiInstance.rsFilesUpdateShareFlags(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsFilesUpdateShareFlags** | [**ReqRsFilesUpdateShareFlags**](ReqRsFilesUpdateShareFlags.md)| dir: &gt;              (SharedDirInfo)Shared directory with updated sharing options   | [optional] 

### Return type

[**RespRsFilesUpdateShareFlags**](RespRsFilesUpdateShareFlags.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGossipDiscoveryGetDiscFriends

> RespRsGossipDiscoveryGetDiscFriends rsGossipDiscoveryGetDiscFriends(opts)

getDiscFriends get a list of all friends of a given friend

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGossipDiscoveryGetDiscFriends': {"id":"(RsPeerId)peer to get the friends of \n"} // ReqRsGossipDiscoveryGetDiscFriends | id: >              (RsPeerId)peer to get the friends of  
};
apiInstance.rsGossipDiscoveryGetDiscFriends(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGossipDiscoveryGetDiscFriends** | [**ReqRsGossipDiscoveryGetDiscFriends**](ReqRsGossipDiscoveryGetDiscFriends.md)| id: &gt;              (RsPeerId)peer to get the friends of   | [optional] 

### Return type

[**RespRsGossipDiscoveryGetDiscFriends**](RespRsGossipDiscoveryGetDiscFriends.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGossipDiscoveryGetDiscPgpFriends

> RespRsGossipDiscoveryGetDiscPgpFriends rsGossipDiscoveryGetDiscPgpFriends(opts)

getDiscPgpFriends get a list of all friends of a given friend

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGossipDiscoveryGetDiscPgpFriends': {"pgpid":"(RsPgpId)peer to get the friends of \n"} // ReqRsGossipDiscoveryGetDiscPgpFriends | pgpid: >              (RsPgpId)peer to get the friends of  
};
apiInstance.rsGossipDiscoveryGetDiscPgpFriends(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGossipDiscoveryGetDiscPgpFriends** | [**ReqRsGossipDiscoveryGetDiscPgpFriends**](ReqRsGossipDiscoveryGetDiscPgpFriends.md)| pgpid: &gt;              (RsPgpId)peer to get the friends of   | [optional] 

### Return type

[**RespRsGossipDiscoveryGetDiscPgpFriends**](RespRsGossipDiscoveryGetDiscPgpFriends.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGossipDiscoveryGetPeerVersion

> RespRsGossipDiscoveryGetPeerVersion rsGossipDiscoveryGetPeerVersion(opts)

getPeerVersion get the version string of a peer.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGossipDiscoveryGetPeerVersion': {"id":"(RsPeerId)peer to get the version string of \n"} // ReqRsGossipDiscoveryGetPeerVersion | id: >              (RsPeerId)peer to get the version string of  
};
apiInstance.rsGossipDiscoveryGetPeerVersion(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGossipDiscoveryGetPeerVersion** | [**ReqRsGossipDiscoveryGetPeerVersion**](ReqRsGossipDiscoveryGetPeerVersion.md)| id: &gt;              (RsPeerId)peer to get the version string of   | [optional] 

### Return type

[**RespRsGossipDiscoveryGetPeerVersion**](RespRsGossipDiscoveryGetPeerVersion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGossipDiscoveryGetWaitingDiscCount

> RespRsGossipDiscoveryGetWaitingDiscCount rsGossipDiscoveryGetWaitingDiscCount()

getWaitingDiscCount get the number of queued discovery packets.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsGossipDiscoveryGetWaitingDiscCount((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsGossipDiscoveryGetWaitingDiscCount**](RespRsGossipDiscoveryGetWaitingDiscCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsGossipDiscoveryRequestInvite

> RespRsGossipDiscoveryRequestInvite rsGossipDiscoveryRequestInvite(opts)

Request RetroShare certificate to given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGossipDiscoveryRequestInvite': {"inviteId":"(RsPeerId)id of the peer of which request the invite \n","toSslId":"(RsPeerId)id of the destination of the request \n"} // ReqRsGossipDiscoveryRequestInvite | inviteId: >              (RsPeerId)id of the peer of which request the invite          toSslId: >              (RsPeerId)id of the destination of the request  
};
apiInstance.rsGossipDiscoveryRequestInvite(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGossipDiscoveryRequestInvite** | [**ReqRsGossipDiscoveryRequestInvite**](ReqRsGossipDiscoveryRequestInvite.md)| inviteId: &gt;              (RsPeerId)id of the peer of which request the invite          toSslId: &gt;              (RsPeerId)id of the destination of the request   | [optional] 

### Return type

[**RespRsGossipDiscoveryRequestInvite**](RespRsGossipDiscoveryRequestInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGossipDiscoverySendInvite

> RespRsGossipDiscoverySendInvite rsGossipDiscoverySendInvite(opts)

Send RetroShare invite to given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGossipDiscoverySendInvite': {"inviteId":"(RsPeerId)id of peer of which send the invite \n","toSslId":"(RsPeerId)ssl id of the destination peer \n"} // ReqRsGossipDiscoverySendInvite | inviteId: >              (RsPeerId)id of peer of which send the invite          toSslId: >              (RsPeerId)ssl id of the destination peer  
};
apiInstance.rsGossipDiscoverySendInvite(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGossipDiscoverySendInvite** | [**ReqRsGossipDiscoverySendInvite**](ReqRsGossipDiscoverySendInvite.md)| inviteId: &gt;              (RsPeerId)id of peer of which send the invite          toSslId: &gt;              (RsPeerId)ssl id of the destination peer   | [optional] 

### Return type

[**RespRsGossipDiscoverySendInvite**](RespRsGossipDiscoverySendInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsCreateChannel

> RespRsGxsChannelsCreateChannel rsGxsChannelsCreateChannel(opts)

Deprecated{ substituted by createChannelV2 }

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsCreateChannel': {"channel":"(RsGxsChannelGroup)Channel data (name, description...) \n"} // ReqRsGxsChannelsCreateChannel | channel: >              (RsGxsChannelGroup)Channel data (name, description...)  
};
apiInstance.rsGxsChannelsCreateChannel(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsCreateChannel** | [**ReqRsGxsChannelsCreateChannel**](ReqRsGxsChannelsCreateChannel.md)| channel: &gt;              (RsGxsChannelGroup)Channel data (name, description...)   | [optional] 

### Return type

[**RespRsGxsChannelsCreateChannel**](RespRsGxsChannelsCreateChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsCreateChannelV2

> RespRsGxsChannelsCreateChannelV2 rsGxsChannelsCreateChannelV2(opts)

Create channel. Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsCreateChannelV2': {"name":"(string)Name of the channel \n","description":"(string)Description of the channel \n","thumbnail":"(RsGxsImage)Optional image to show as channel thumbnail. \n","authorId":"(RsGxsId)Optional id of the author. Leave empty for an anonymous channel. \n","circleType":"(RsGxsCircleType)Optional visibility rule, default public. \n","circleId":"(RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise. \n"} // ReqRsGxsChannelsCreateChannelV2 | name: >              (string)Name of the channel          description: >              (string)Description of the channel          thumbnail: >              (RsGxsImage)Optional image to show as channel thumbnail.          authorId: >              (RsGxsId)Optional id of the author. Leave empty for an anonymous channel.          circleType: >              (RsGxsCircleType)Optional visibility rule, default public.          circleId: >              (RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise.  
};
apiInstance.rsGxsChannelsCreateChannelV2(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsCreateChannelV2** | [**ReqRsGxsChannelsCreateChannelV2**](ReqRsGxsChannelsCreateChannelV2.md)| name: &gt;              (string)Name of the channel          description: &gt;              (string)Description of the channel          thumbnail: &gt;              (RsGxsImage)Optional image to show as channel thumbnail.          authorId: &gt;              (RsGxsId)Optional id of the author. Leave empty for an anonymous channel.          circleType: &gt;              (RsGxsCircleType)Optional visibility rule, default public.          circleId: &gt;              (RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise.   | [optional] 

### Return type

[**RespRsGxsChannelsCreateChannelV2**](RespRsGxsChannelsCreateChannelV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsCreateComment

> RespRsGxsChannelsCreateComment rsGxsChannelsCreateComment(opts)

Deprecated

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsCreateComment': {"comment":"(RsGxsComment)None\n"} // ReqRsGxsChannelsCreateComment | comment: >              (RsGxsComment)None 
};
apiInstance.rsGxsChannelsCreateComment(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsCreateComment** | [**ReqRsGxsChannelsCreateComment**](ReqRsGxsChannelsCreateComment.md)| comment: &gt;              (RsGxsComment)None  | [optional] 

### Return type

[**RespRsGxsChannelsCreateComment**](RespRsGxsChannelsCreateComment.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsCreateCommentV2

> RespRsGxsChannelsCreateCommentV2 rsGxsChannelsCreateCommentV2(opts)

Add a comment on a post or on another comment. Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsCreateCommentV2': {"channelId":"(RsGxsGroupId)Id of the channel in which the comment is to be posted \n","threadId":"(RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed \n","comment":"(string)UTF-8 string containing the comment itself \n","authorId":"(RsGxsId)Id of the author of the comment \n","parentId":"(RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment. \n","origCommentId":"(RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created. \n"} // ReqRsGxsChannelsCreateCommentV2 | channelId: >              (RsGxsGroupId)Id of the channel in which the comment is to be posted          threadId: >              (RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed          comment: >              (string)UTF-8 string containing the comment itself          authorId: >              (RsGxsId)Id of the author of the comment          parentId: >              (RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment.          origCommentId: >              (RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created.  
};
apiInstance.rsGxsChannelsCreateCommentV2(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsCreateCommentV2** | [**ReqRsGxsChannelsCreateCommentV2**](ReqRsGxsChannelsCreateCommentV2.md)| channelId: &gt;              (RsGxsGroupId)Id of the channel in which the comment is to be posted          threadId: &gt;              (RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed          comment: &gt;              (string)UTF-8 string containing the comment itself          authorId: &gt;              (RsGxsId)Id of the author of the comment          parentId: &gt;              (RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment.          origCommentId: &gt;              (RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created.   | [optional] 

### Return type

[**RespRsGxsChannelsCreateCommentV2**](RespRsGxsChannelsCreateCommentV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsCreatePost

> RespRsGxsChannelsCreatePost rsGxsChannelsCreatePost(opts)

Deprecated

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsCreatePost': {"post":"(RsGxsChannelPost)None\n"} // ReqRsGxsChannelsCreatePost | post: >              (RsGxsChannelPost)None 
};
apiInstance.rsGxsChannelsCreatePost(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsCreatePost** | [**ReqRsGxsChannelsCreatePost**](ReqRsGxsChannelsCreatePost.md)| post: &gt;              (RsGxsChannelPost)None  | [optional] 

### Return type

[**RespRsGxsChannelsCreatePost**](RespRsGxsChannelsCreatePost.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsCreatePostV2

> RespRsGxsChannelsCreatePostV2 rsGxsChannelsCreatePostV2(opts)

Create channel post. Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsCreatePostV2': {"channelId":"(RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post. \n","title":"(string)Title of the post \n","mBody":"(string)Text content of the post \n","files":"(list<RsGxsFile>)Optional list of attached files. These are supposed to be already shared, \n","thumbnail":"(RsGxsImage)Optional thumbnail image for the post. \n","origPostId":"(RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created. \n"} // ReqRsGxsChannelsCreatePostV2 | channelId: >              (RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post.          title: >              (string)Title of the post          mBody: >              (string)Text content of the post          files: >              (list<RsGxsFile>)Optional list of attached files. These are supposed to be already shared,          thumbnail: >              (RsGxsImage)Optional thumbnail image for the post.          origPostId: >              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.  
};
apiInstance.rsGxsChannelsCreatePostV2(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsCreatePostV2** | [**ReqRsGxsChannelsCreatePostV2**](ReqRsGxsChannelsCreatePostV2.md)| channelId: &gt;              (RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post.          title: &gt;              (string)Title of the post          mBody: &gt;              (string)Text content of the post          files: &gt;              (list&lt;RsGxsFile&gt;)Optional list of attached files. These are supposed to be already shared,          thumbnail: &gt;              (RsGxsImage)Optional thumbnail image for the post.          origPostId: &gt;              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.   | [optional] 

### Return type

[**RespRsGxsChannelsCreatePostV2**](RespRsGxsChannelsCreatePostV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsCreateVote

> RespRsGxsChannelsCreateVote rsGxsChannelsCreateVote(opts)

Deprecated

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsCreateVote': {"vote":"(RsGxsVote)None\n"} // ReqRsGxsChannelsCreateVote | vote: >              (RsGxsVote)None 
};
apiInstance.rsGxsChannelsCreateVote(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsCreateVote** | [**ReqRsGxsChannelsCreateVote**](ReqRsGxsChannelsCreateVote.md)| vote: &gt;              (RsGxsVote)None  | [optional] 

### Return type

[**RespRsGxsChannelsCreateVote**](RespRsGxsChannelsCreateVote.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsCreateVoteV2

> RespRsGxsChannelsCreateVoteV2 rsGxsChannelsCreateVoteV2(opts)

Create a vote

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsCreateVoteV2': {"channelId":"(RsGxsGroupId)Id of the channel where to vote \n","postId":"(RsGxsMessageId)Id of the channel post of which a comment is voted. \n","commentId":"(RsGxsMessageId)Id of the comment that is voted \n","authorId":"(RsGxsId)Id of the author. Needs to be of an owned identity. \n","vote":"(RsGxsVoteType)Vote value, either \n"} // ReqRsGxsChannelsCreateVoteV2 | channelId: >              (RsGxsGroupId)Id of the channel where to vote          postId: >              (RsGxsMessageId)Id of the channel post of which a comment is voted.          commentId: >              (RsGxsMessageId)Id of the comment that is voted          authorId: >              (RsGxsId)Id of the author. Needs to be of an owned identity.          vote: >              (RsGxsVoteType)Vote value, either  
};
apiInstance.rsGxsChannelsCreateVoteV2(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsCreateVoteV2** | [**ReqRsGxsChannelsCreateVoteV2**](ReqRsGxsChannelsCreateVoteV2.md)| channelId: &gt;              (RsGxsGroupId)Id of the channel where to vote          postId: &gt;              (RsGxsMessageId)Id of the channel post of which a comment is voted.          commentId: &gt;              (RsGxsMessageId)Id of the comment that is voted          authorId: &gt;              (RsGxsId)Id of the author. Needs to be of an owned identity.          vote: &gt;              (RsGxsVoteType)Vote value, either   | [optional] 

### Return type

[**RespRsGxsChannelsCreateVoteV2**](RespRsGxsChannelsCreateVoteV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsEditChannel

> RespRsGxsChannelsEditChannel rsGxsChannelsEditChannel(opts)

Edit channel details.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsEditChannel': {"channel":"(RsGxsChannelGroup)Channel data (name, description...) with modifications \n"} // ReqRsGxsChannelsEditChannel | channel: >              (RsGxsChannelGroup)Channel data (name, description...) with modifications  
};
apiInstance.rsGxsChannelsEditChannel(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsEditChannel** | [**ReqRsGxsChannelsEditChannel**](ReqRsGxsChannelsEditChannel.md)| channel: &gt;              (RsGxsChannelGroup)Channel data (name, description...) with modifications   | [optional] 

### Return type

[**RespRsGxsChannelsEditChannel**](RespRsGxsChannelsEditChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsExtraFileHash

> RespRsGxsChannelsExtraFileHash rsGxsChannelsExtraFileHash(opts)

Share extra file Can be used to share extra file attached to a channel post

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsExtraFileHash': {"path":"(string)file path \n"} // ReqRsGxsChannelsExtraFileHash | path: >              (string)file path  
};
apiInstance.rsGxsChannelsExtraFileHash(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsExtraFileHash** | [**ReqRsGxsChannelsExtraFileHash**](ReqRsGxsChannelsExtraFileHash.md)| path: &gt;              (string)file path   | [optional] 

### Return type

[**RespRsGxsChannelsExtraFileHash**](RespRsGxsChannelsExtraFileHash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsExtraFileRemove

> RespRsGxsChannelsExtraFileRemove rsGxsChannelsExtraFileRemove(opts)

Remove extra file from shared files

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsExtraFileRemove': {"hash":"(RsFileHash)hash of the file to remove \n"} // ReqRsGxsChannelsExtraFileRemove | hash: >              (RsFileHash)hash of the file to remove  
};
apiInstance.rsGxsChannelsExtraFileRemove(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsExtraFileRemove** | [**ReqRsGxsChannelsExtraFileRemove**](ReqRsGxsChannelsExtraFileRemove.md)| hash: &gt;              (RsFileHash)hash of the file to remove   | [optional] 

### Return type

[**RespRsGxsChannelsExtraFileRemove**](RespRsGxsChannelsExtraFileRemove.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsGetChannelAutoDownload

> RespRsGxsChannelsGetChannelAutoDownload rsGxsChannelsGetChannelAutoDownload(opts)

Get auto-download option value for given channel

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsGetChannelAutoDownload': {"channelId":"(RsGxsGroupId)channel id \n"} // ReqRsGxsChannelsGetChannelAutoDownload | channelId: >              (RsGxsGroupId)channel id  
};
apiInstance.rsGxsChannelsGetChannelAutoDownload(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsGetChannelAutoDownload** | [**ReqRsGxsChannelsGetChannelAutoDownload**](ReqRsGxsChannelsGetChannelAutoDownload.md)| channelId: &gt;              (RsGxsGroupId)channel id   | [optional] 

### Return type

[**RespRsGxsChannelsGetChannelAutoDownload**](RespRsGxsChannelsGetChannelAutoDownload.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsGetChannelContent

> RespRsGxsChannelsGetChannelContent rsGxsChannelsGetChannelContent(opts)

Get channel contents

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsGetChannelContent': {"channelId":"(RsGxsGroupId)id of the channel of which the content is requested \n","contentsIds":"(set<RsGxsMessageId>)ids of requested contents \n"} // ReqRsGxsChannelsGetChannelContent | channelId: >              (RsGxsGroupId)id of the channel of which the content is requested          contentsIds: >              (set<RsGxsMessageId>)ids of requested contents  
};
apiInstance.rsGxsChannelsGetChannelContent(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsGetChannelContent** | [**ReqRsGxsChannelsGetChannelContent**](ReqRsGxsChannelsGetChannelContent.md)| channelId: &gt;              (RsGxsGroupId)id of the channel of which the content is requested          contentsIds: &gt;              (set&lt;RsGxsMessageId&gt;)ids of requested contents   | [optional] 

### Return type

[**RespRsGxsChannelsGetChannelContent**](RespRsGxsChannelsGetChannelContent.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsGetChannelDownloadDirectory

> RespRsGxsChannelsGetChannelDownloadDirectory rsGxsChannelsGetChannelDownloadDirectory(opts)

Get download directory for the given channel

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsGetChannelDownloadDirectory': {"channelId":"(RsGxsGroupId)id of the channel \n"} // ReqRsGxsChannelsGetChannelDownloadDirectory | channelId: >              (RsGxsGroupId)id of the channel  
};
apiInstance.rsGxsChannelsGetChannelDownloadDirectory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsGetChannelDownloadDirectory** | [**ReqRsGxsChannelsGetChannelDownloadDirectory**](ReqRsGxsChannelsGetChannelDownloadDirectory.md)| channelId: &gt;              (RsGxsGroupId)id of the channel   | [optional] 

### Return type

[**RespRsGxsChannelsGetChannelDownloadDirectory**](RespRsGxsChannelsGetChannelDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsGetChannelsInfo

> RespRsGxsChannelsGetChannelsInfo rsGxsChannelsGetChannelsInfo(opts)

Get channels information (description, thumbnail...). Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsGetChannelsInfo': {"chanIds":"(list<RsGxsGroupId>)ids of the channels of which to get the informations \n"} // ReqRsGxsChannelsGetChannelsInfo | chanIds: >              (list<RsGxsGroupId>)ids of the channels of which to get the informations  
};
apiInstance.rsGxsChannelsGetChannelsInfo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsGetChannelsInfo** | [**ReqRsGxsChannelsGetChannelsInfo**](ReqRsGxsChannelsGetChannelsInfo.md)| chanIds: &gt;              (list&lt;RsGxsGroupId&gt;)ids of the channels of which to get the informations   | [optional] 

### Return type

[**RespRsGxsChannelsGetChannelsInfo**](RespRsGxsChannelsGetChannelsInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsGetChannelsSummaries

> RespRsGxsChannelsGetChannelsSummaries rsGxsChannelsGetChannelsSummaries()

Get channels summaries list. Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsGxsChannelsGetChannelsSummaries((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsGxsChannelsGetChannelsSummaries**](RespRsGxsChannelsGetChannelsSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsGxsChannelsGetContentSummaries

> RespRsGxsChannelsGetContentSummaries rsGxsChannelsGetContentSummaries(opts)

Get channel content summaries

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsGetContentSummaries': {"channelId":"(RsGxsGroupId)id of the channel of which the content is requested \n"} // ReqRsGxsChannelsGetContentSummaries | channelId: >              (RsGxsGroupId)id of the channel of which the content is requested  
};
apiInstance.rsGxsChannelsGetContentSummaries(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsGetContentSummaries** | [**ReqRsGxsChannelsGetContentSummaries**](ReqRsGxsChannelsGetContentSummaries.md)| channelId: &gt;              (RsGxsGroupId)id of the channel of which the content is requested   | [optional] 

### Return type

[**RespRsGxsChannelsGetContentSummaries**](RespRsGxsChannelsGetContentSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsLocalSearchRequest

> RespRsGxsChannelsLocalSearchRequest rsGxsChannelsLocalSearchRequest(opts)

This method is asynchronous. Search local channels

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsLocalSearchRequest': {"matchString":"(string)string to look for in the search \n","maxWait":"(rstime_t)maximum wait time in seconds for search results \n"} // ReqRsGxsChannelsLocalSearchRequest | matchString: >              (string)string to look for in the search          maxWait: >              (rstime_t)maximum wait time in seconds for search results  
};
apiInstance.rsGxsChannelsLocalSearchRequest(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsLocalSearchRequest** | [**ReqRsGxsChannelsLocalSearchRequest**](ReqRsGxsChannelsLocalSearchRequest.md)| matchString: &gt;              (string)string to look for in the search          maxWait: &gt;              (rstime_t)maximum wait time in seconds for search results   | [optional] 

### Return type

[**RespRsGxsChannelsLocalSearchRequest**](RespRsGxsChannelsLocalSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsMarkRead

> RespRsGxsChannelsMarkRead rsGxsChannelsMarkRead(opts)

Toggle post read status. Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsMarkRead': {"postId":"(RsGxsGrpMsgIdPair)post identifier \n","read":"(boolean)true to mark as read, false to mark as unread \n"} // ReqRsGxsChannelsMarkRead | postId: >              (RsGxsGrpMsgIdPair)post identifier          read: >              (boolean)true to mark as read, false to mark as unread  
};
apiInstance.rsGxsChannelsMarkRead(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsMarkRead** | [**ReqRsGxsChannelsMarkRead**](ReqRsGxsChannelsMarkRead.md)| postId: &gt;              (RsGxsGrpMsgIdPair)post identifier          read: &gt;              (boolean)true to mark as read, false to mark as unread   | [optional] 

### Return type

[**RespRsGxsChannelsMarkRead**](RespRsGxsChannelsMarkRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsRequestStatus

> RespRsGxsChannelsRequestStatus rsGxsChannelsRequestStatus(opts)

null

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsRequestStatus': {"token":"(integer)None\n"} // ReqRsGxsChannelsRequestStatus | token: >              (integer)None 
};
apiInstance.rsGxsChannelsRequestStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsRequestStatus** | [**ReqRsGxsChannelsRequestStatus**](ReqRsGxsChannelsRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**RespRsGxsChannelsRequestStatus**](RespRsGxsChannelsRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsSetChannelAutoDownload

> RespRsGxsChannelsSetChannelAutoDownload rsGxsChannelsSetChannelAutoDownload(opts)

Enable or disable auto-download for given channel. Blocking API

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsSetChannelAutoDownload': {"channelId":"(RsGxsGroupId)channel id \n","enable":"(boolean)true to enable, false to disable \n"} // ReqRsGxsChannelsSetChannelAutoDownload | channelId: >              (RsGxsGroupId)channel id          enable: >              (boolean)true to enable, false to disable  
};
apiInstance.rsGxsChannelsSetChannelAutoDownload(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsSetChannelAutoDownload** | [**ReqRsGxsChannelsSetChannelAutoDownload**](ReqRsGxsChannelsSetChannelAutoDownload.md)| channelId: &gt;              (RsGxsGroupId)channel id          enable: &gt;              (boolean)true to enable, false to disable   | [optional] 

### Return type

[**RespRsGxsChannelsSetChannelAutoDownload**](RespRsGxsChannelsSetChannelAutoDownload.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsSetChannelDownloadDirectory

> RespRsGxsChannelsSetChannelDownloadDirectory rsGxsChannelsSetChannelDownloadDirectory(opts)

Set download directory for the given channel. Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsSetChannelDownloadDirectory': {"channelId":"(RsGxsGroupId)id of the channel \n","directory":"(string)path \n"} // ReqRsGxsChannelsSetChannelDownloadDirectory | channelId: >              (RsGxsGroupId)id of the channel          directory: >              (string)path  
};
apiInstance.rsGxsChannelsSetChannelDownloadDirectory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsSetChannelDownloadDirectory** | [**ReqRsGxsChannelsSetChannelDownloadDirectory**](ReqRsGxsChannelsSetChannelDownloadDirectory.md)| channelId: &gt;              (RsGxsGroupId)id of the channel          directory: &gt;              (string)path   | [optional] 

### Return type

[**RespRsGxsChannelsSetChannelDownloadDirectory**](RespRsGxsChannelsSetChannelDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsShareChannelKeys

> RespRsGxsChannelsShareChannelKeys rsGxsChannelsShareChannelKeys(opts)

Share channel publishing key This can be used to authorize other peers to post on the channel

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsShareChannelKeys': {"channelId":"(RsGxsGroupId)id of the channel \n","peers":"(set<RsPeerId>)peers to share the key with \n"} // ReqRsGxsChannelsShareChannelKeys | channelId: >              (RsGxsGroupId)id of the channel          peers: >              (set<RsPeerId>)peers to share the key with  
};
apiInstance.rsGxsChannelsShareChannelKeys(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsShareChannelKeys** | [**ReqRsGxsChannelsShareChannelKeys**](ReqRsGxsChannelsShareChannelKeys.md)| channelId: &gt;              (RsGxsGroupId)id of the channel          peers: &gt;              (set&lt;RsPeerId&gt;)peers to share the key with   | [optional] 

### Return type

[**RespRsGxsChannelsShareChannelKeys**](RespRsGxsChannelsShareChannelKeys.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsSubscribeToChannel

> RespRsGxsChannelsSubscribeToChannel rsGxsChannelsSubscribeToChannel(opts)

Subscrbe to a channel. Blocking API

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsSubscribeToChannel': {"channelId":"(RsGxsGroupId)Channel id \n","subscribe":"(boolean)true to subscribe, false to unsubscribe \n"} // ReqRsGxsChannelsSubscribeToChannel | channelId: >              (RsGxsGroupId)Channel id          subscribe: >              (boolean)true to subscribe, false to unsubscribe  
};
apiInstance.rsGxsChannelsSubscribeToChannel(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsSubscribeToChannel** | [**ReqRsGxsChannelsSubscribeToChannel**](ReqRsGxsChannelsSubscribeToChannel.md)| channelId: &gt;              (RsGxsGroupId)Channel id          subscribe: &gt;              (boolean)true to subscribe, false to unsubscribe   | [optional] 

### Return type

[**RespRsGxsChannelsSubscribeToChannel**](RespRsGxsChannelsSubscribeToChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsTurtleChannelRequest

> RespRsGxsChannelsTurtleChannelRequest rsGxsChannelsTurtleChannelRequest(opts)

This method is asynchronous. Request remote channel

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsTurtleChannelRequest': {"channelId":"(RsGxsGroupId)id of the channel to request to distants peers \n","maxWait":"(rstime_t)maximum wait time in seconds for search results \n"} // ReqRsGxsChannelsTurtleChannelRequest | channelId: >              (RsGxsGroupId)id of the channel to request to distants peers          maxWait: >              (rstime_t)maximum wait time in seconds for search results  
};
apiInstance.rsGxsChannelsTurtleChannelRequest(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsTurtleChannelRequest** | [**ReqRsGxsChannelsTurtleChannelRequest**](ReqRsGxsChannelsTurtleChannelRequest.md)| channelId: &gt;              (RsGxsGroupId)id of the channel to request to distants peers          maxWait: &gt;              (rstime_t)maximum wait time in seconds for search results   | [optional] 

### Return type

[**RespRsGxsChannelsTurtleChannelRequest**](RespRsGxsChannelsTurtleChannelRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsChannelsTurtleSearchRequest

> RespRsGxsChannelsTurtleSearchRequest rsGxsChannelsTurtleSearchRequest(opts)

This method is asynchronous. Request remote channels search

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsChannelsTurtleSearchRequest': {"matchString":"(string)string to look for in the search \n","maxWait":"(rstime_t)maximum wait time in seconds for search results \n"} // ReqRsGxsChannelsTurtleSearchRequest | matchString: >              (string)string to look for in the search          maxWait: >              (rstime_t)maximum wait time in seconds for search results  
};
apiInstance.rsGxsChannelsTurtleSearchRequest(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsChannelsTurtleSearchRequest** | [**ReqRsGxsChannelsTurtleSearchRequest**](ReqRsGxsChannelsTurtleSearchRequest.md)| matchString: &gt;              (string)string to look for in the search          maxWait: &gt;              (rstime_t)maximum wait time in seconds for search results   | [optional] 

### Return type

[**RespRsGxsChannelsTurtleSearchRequest**](RespRsGxsChannelsTurtleSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesCancelCircleMembership

> RespRsGxsCirclesCancelCircleMembership rsGxsCirclesCancelCircleMembership(opts)

Leave given circle

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesCancelCircleMembership': {"ownGxsId":"(RsGxsId)Own id to remove from the circle \n","circleId":"(RsGxsCircleId)Id of the circle to leave \n"} // ReqRsGxsCirclesCancelCircleMembership | ownGxsId: >              (RsGxsId)Own id to remove from the circle          circleId: >              (RsGxsCircleId)Id of the circle to leave  
};
apiInstance.rsGxsCirclesCancelCircleMembership(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesCancelCircleMembership** | [**ReqRsGxsCirclesCancelCircleMembership**](ReqRsGxsCirclesCancelCircleMembership.md)| ownGxsId: &gt;              (RsGxsId)Own id to remove from the circle          circleId: &gt;              (RsGxsCircleId)Id of the circle to leave   | [optional] 

### Return type

[**RespRsGxsCirclesCancelCircleMembership**](RespRsGxsCirclesCancelCircleMembership.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesCreateCircle

> RespRsGxsCirclesCreateCircle rsGxsCirclesCreateCircle(opts)

Create new circle

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesCreateCircle': {"cData":"(RsGxsCircleGroup)input name and flags of the circle, storage for generated circle data id etc. \n"} // ReqRsGxsCirclesCreateCircle | cData: >              (RsGxsCircleGroup)input name and flags of the circle, storage for generated circle data id etc.  
};
apiInstance.rsGxsCirclesCreateCircle(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesCreateCircle** | [**ReqRsGxsCirclesCreateCircle**](ReqRsGxsCirclesCreateCircle.md)| cData: &gt;              (RsGxsCircleGroup)input name and flags of the circle, storage for generated circle data id etc.   | [optional] 

### Return type

[**RespRsGxsCirclesCreateCircle**](RespRsGxsCirclesCreateCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesEditCircle

> RespRsGxsCirclesEditCircle rsGxsCirclesEditCircle(opts)

Edit own existing circle

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesEditCircle': {"cData":"(RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation. \n"} // ReqRsGxsCirclesEditCircle | cData: >              (RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation.  
};
apiInstance.rsGxsCirclesEditCircle(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesEditCircle** | [**ReqRsGxsCirclesEditCircle**](ReqRsGxsCirclesEditCircle.md)| cData: &gt;              (RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation.   | [optional] 

### Return type

[**RespRsGxsCirclesEditCircle**](RespRsGxsCirclesEditCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesGetCircleDetails

> RespRsGxsCirclesGetCircleDetails rsGxsCirclesGetCircleDetails(opts)

Get circle details. Memory cached

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesGetCircleDetails': {"id":"(RsGxsCircleId)Id of the circle \n"} // ReqRsGxsCirclesGetCircleDetails | id: >              (RsGxsCircleId)Id of the circle  
};
apiInstance.rsGxsCirclesGetCircleDetails(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesGetCircleDetails** | [**ReqRsGxsCirclesGetCircleDetails**](ReqRsGxsCirclesGetCircleDetails.md)| id: &gt;              (RsGxsCircleId)Id of the circle   | [optional] 

### Return type

[**RespRsGxsCirclesGetCircleDetails**](RespRsGxsCirclesGetCircleDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesGetCircleExternalIdList

> RespRsGxsCirclesGetCircleExternalIdList rsGxsCirclesGetCircleExternalIdList(opts)

Get list of known external circles ids. Memory cached

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesGetCircleExternalIdList': {"circleIds":"(list<RsGxsCircleId>)Storage for circles id list \n"} // ReqRsGxsCirclesGetCircleExternalIdList | circleIds: >              (list<RsGxsCircleId>)Storage for circles id list  
};
apiInstance.rsGxsCirclesGetCircleExternalIdList(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesGetCircleExternalIdList** | [**ReqRsGxsCirclesGetCircleExternalIdList**](ReqRsGxsCirclesGetCircleExternalIdList.md)| circleIds: &gt;              (list&lt;RsGxsCircleId&gt;)Storage for circles id list   | [optional] 

### Return type

[**RespRsGxsCirclesGetCircleExternalIdList**](RespRsGxsCirclesGetCircleExternalIdList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesGetCircleRequests

> RespRsGxsCirclesGetCircleRequests rsGxsCirclesGetCircleRequests(opts)

Get circle requests

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesGetCircleRequests': {"circleId":"(RsGxsGroupId)id of the circle of which the requests are requested \n"} // ReqRsGxsCirclesGetCircleRequests | circleId: >              (RsGxsGroupId)id of the circle of which the requests are requested  
};
apiInstance.rsGxsCirclesGetCircleRequests(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesGetCircleRequests** | [**ReqRsGxsCirclesGetCircleRequests**](ReqRsGxsCirclesGetCircleRequests.md)| circleId: &gt;              (RsGxsGroupId)id of the circle of which the requests are requested   | [optional] 

### Return type

[**RespRsGxsCirclesGetCircleRequests**](RespRsGxsCirclesGetCircleRequests.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesGetCirclesInfo

> RespRsGxsCirclesGetCirclesInfo rsGxsCirclesGetCirclesInfo(opts)

Get circles information

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesGetCirclesInfo': {"circlesIds":"(list<RsGxsGroupId>)ids of the circles of which to get the informations \n"} // ReqRsGxsCirclesGetCirclesInfo | circlesIds: >              (list<RsGxsGroupId>)ids of the circles of which to get the informations  
};
apiInstance.rsGxsCirclesGetCirclesInfo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesGetCirclesInfo** | [**ReqRsGxsCirclesGetCirclesInfo**](ReqRsGxsCirclesGetCirclesInfo.md)| circlesIds: &gt;              (list&lt;RsGxsGroupId&gt;)ids of the circles of which to get the informations   | [optional] 

### Return type

[**RespRsGxsCirclesGetCirclesInfo**](RespRsGxsCirclesGetCirclesInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesGetCirclesSummaries

> RespRsGxsCirclesGetCirclesSummaries rsGxsCirclesGetCirclesSummaries()

Get circles summaries list.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsGxsCirclesGetCirclesSummaries((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsGxsCirclesGetCirclesSummaries**](RespRsGxsCirclesGetCirclesSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsGxsCirclesInviteIdsToCircle

> RespRsGxsCirclesInviteIdsToCircle rsGxsCirclesInviteIdsToCircle(opts)

Invite identities to circle

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesInviteIdsToCircle': {"identities":"(set<RsGxsId>)ids of the identities to invite \n","circleId":"(RsGxsCircleId)Id of the circle you own and want to invite ids in \n"} // ReqRsGxsCirclesInviteIdsToCircle | identities: >              (set<RsGxsId>)ids of the identities to invite          circleId: >              (RsGxsCircleId)Id of the circle you own and want to invite ids in  
};
apiInstance.rsGxsCirclesInviteIdsToCircle(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesInviteIdsToCircle** | [**ReqRsGxsCirclesInviteIdsToCircle**](ReqRsGxsCirclesInviteIdsToCircle.md)| identities: &gt;              (set&lt;RsGxsId&gt;)ids of the identities to invite          circleId: &gt;              (RsGxsCircleId)Id of the circle you own and want to invite ids in   | [optional] 

### Return type

[**RespRsGxsCirclesInviteIdsToCircle**](RespRsGxsCirclesInviteIdsToCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesRequestCircleMembership

> RespRsGxsCirclesRequestCircleMembership rsGxsCirclesRequestCircleMembership(opts)

Request circle membership, or accept circle invitation

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesRequestCircleMembership': {"ownGxsId":"(RsGxsId)Id of own identity to introduce to the circle \n","circleId":"(RsGxsCircleId)Id of the circle to which ask for inclusion \n"} // ReqRsGxsCirclesRequestCircleMembership | ownGxsId: >              (RsGxsId)Id of own identity to introduce to the circle          circleId: >              (RsGxsCircleId)Id of the circle to which ask for inclusion  
};
apiInstance.rsGxsCirclesRequestCircleMembership(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesRequestCircleMembership** | [**ReqRsGxsCirclesRequestCircleMembership**](ReqRsGxsCirclesRequestCircleMembership.md)| ownGxsId: &gt;              (RsGxsId)Id of own identity to introduce to the circle          circleId: &gt;              (RsGxsCircleId)Id of the circle to which ask for inclusion   | [optional] 

### Return type

[**RespRsGxsCirclesRequestCircleMembership**](RespRsGxsCirclesRequestCircleMembership.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsCirclesRequestStatus

> RespRsGxsCirclesRequestStatus rsGxsCirclesRequestStatus(opts)

null

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsCirclesRequestStatus': {"token":"(integer)None\n"} // ReqRsGxsCirclesRequestStatus | token: >              (integer)None 
};
apiInstance.rsGxsCirclesRequestStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsCirclesRequestStatus** | [**ReqRsGxsCirclesRequestStatus**](ReqRsGxsCirclesRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**RespRsGxsCirclesRequestStatus**](RespRsGxsCirclesRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsCreateForum

> RespRsGxsForumsCreateForum rsGxsForumsCreateForum(opts)

Deprecated

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsCreateForum': {"forum":"(RsGxsForumGroup)Forum data (name, description...) \n"} // ReqRsGxsForumsCreateForum | forum: >              (RsGxsForumGroup)Forum data (name, description...)  
};
apiInstance.rsGxsForumsCreateForum(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsCreateForum** | [**ReqRsGxsForumsCreateForum**](ReqRsGxsForumsCreateForum.md)| forum: &gt;              (RsGxsForumGroup)Forum data (name, description...)   | [optional] 

### Return type

[**RespRsGxsForumsCreateForum**](RespRsGxsForumsCreateForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsCreateForumV2

> RespRsGxsForumsCreateForumV2 rsGxsForumsCreateForumV2(opts)

Create forum.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsCreateForumV2': {"name":"(string)Name of the forum \n","description":"(string)Optional description of the forum \n","authorId":"(RsGxsId)Optional id of the froum owner author \n","moderatorsIds":"(set<RsGxsId>)Optional list of forum moderators \n","circleType":"(RsGxsCircleType)Optional visibility rule, default public. \n","circleId":"(RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise. \n"} // ReqRsGxsForumsCreateForumV2 | name: >              (string)Name of the forum          description: >              (string)Optional description of the forum          authorId: >              (RsGxsId)Optional id of the froum owner author          moderatorsIds: >              (set<RsGxsId>)Optional list of forum moderators          circleType: >              (RsGxsCircleType)Optional visibility rule, default public.          circleId: >              (RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise.  
};
apiInstance.rsGxsForumsCreateForumV2(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsCreateForumV2** | [**ReqRsGxsForumsCreateForumV2**](ReqRsGxsForumsCreateForumV2.md)| name: &gt;              (string)Name of the forum          description: &gt;              (string)Optional description of the forum          authorId: &gt;              (RsGxsId)Optional id of the froum owner author          moderatorsIds: &gt;              (set&lt;RsGxsId&gt;)Optional list of forum moderators          circleType: &gt;              (RsGxsCircleType)Optional visibility rule, default public.          circleId: &gt;              (RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise.   | [optional] 

### Return type

[**RespRsGxsForumsCreateForumV2**](RespRsGxsForumsCreateForumV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsCreateMessage

> RespRsGxsForumsCreateMessage rsGxsForumsCreateMessage(opts)

Deprecated

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsCreateMessage': {"message":"(RsGxsForumMsg)None\n"} // ReqRsGxsForumsCreateMessage | message: >              (RsGxsForumMsg)None 
};
apiInstance.rsGxsForumsCreateMessage(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsCreateMessage** | [**ReqRsGxsForumsCreateMessage**](ReqRsGxsForumsCreateMessage.md)| message: &gt;              (RsGxsForumMsg)None  | [optional] 

### Return type

[**RespRsGxsForumsCreateMessage**](RespRsGxsForumsCreateMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsCreatePost

> RespRsGxsForumsCreatePost rsGxsForumsCreatePost(opts)

Create a post on the given forum.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsCreatePost': {"forumId":"(RsGxsGroupId)Id of the forum in which the post is to be submitted \n","title":"(string)UTF-8 string containing the title of the post \n","mBody":"(string)UTF-8 string containing the text of the post \n","authorId":"(RsGxsId)Id of the author of the comment \n","parentId":"(RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise. \n","origPostId":"(RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created. \n"} // ReqRsGxsForumsCreatePost | forumId: >              (RsGxsGroupId)Id of the forum in which the post is to be submitted          title: >              (string)UTF-8 string containing the title of the post          mBody: >              (string)UTF-8 string containing the text of the post          authorId: >              (RsGxsId)Id of the author of the comment          parentId: >              (RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise.          origPostId: >              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.  
};
apiInstance.rsGxsForumsCreatePost(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsCreatePost** | [**ReqRsGxsForumsCreatePost**](ReqRsGxsForumsCreatePost.md)| forumId: &gt;              (RsGxsGroupId)Id of the forum in which the post is to be submitted          title: &gt;              (string)UTF-8 string containing the title of the post          mBody: &gt;              (string)UTF-8 string containing the text of the post          authorId: &gt;              (RsGxsId)Id of the author of the comment          parentId: &gt;              (RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise.          origPostId: &gt;              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.   | [optional] 

### Return type

[**RespRsGxsForumsCreatePost**](RespRsGxsForumsCreatePost.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsEditForum

> RespRsGxsForumsEditForum rsGxsForumsEditForum(opts)

Edit forum details.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsEditForum': {"forum":"(RsGxsForumGroup)Forum data (name, description...) with modifications \n"} // ReqRsGxsForumsEditForum | forum: >              (RsGxsForumGroup)Forum data (name, description...) with modifications  
};
apiInstance.rsGxsForumsEditForum(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsEditForum** | [**ReqRsGxsForumsEditForum**](ReqRsGxsForumsEditForum.md)| forum: &gt;              (RsGxsForumGroup)Forum data (name, description...) with modifications   | [optional] 

### Return type

[**RespRsGxsForumsEditForum**](RespRsGxsForumsEditForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsGetForumContent

> RespRsGxsForumsGetForumContent rsGxsForumsGetForumContent(opts)

Get specific list of messages from a single forums. Blocking API

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsGetForumContent': {"forumId":"(RsGxsGroupId)id of the forum of which the content is requested \n","msgsIds":"(set<RsGxsMessageId>)list of message ids to request \n"} // ReqRsGxsForumsGetForumContent | forumId: >              (RsGxsGroupId)id of the forum of which the content is requested          msgsIds: >              (set<RsGxsMessageId>)list of message ids to request  
};
apiInstance.rsGxsForumsGetForumContent(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsGetForumContent** | [**ReqRsGxsForumsGetForumContent**](ReqRsGxsForumsGetForumContent.md)| forumId: &gt;              (RsGxsGroupId)id of the forum of which the content is requested          msgsIds: &gt;              (set&lt;RsGxsMessageId&gt;)list of message ids to request   | [optional] 

### Return type

[**RespRsGxsForumsGetForumContent**](RespRsGxsForumsGetForumContent.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsGetForumMsgMetaData

> RespRsGxsForumsGetForumMsgMetaData rsGxsForumsGetForumMsgMetaData(opts)

Get message metadatas for a specific forum. Blocking API

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsGetForumMsgMetaData': {"forumId":"(RsGxsGroupId)id of the forum of which the content is requested \n"} // ReqRsGxsForumsGetForumMsgMetaData | forumId: >              (RsGxsGroupId)id of the forum of which the content is requested  
};
apiInstance.rsGxsForumsGetForumMsgMetaData(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsGetForumMsgMetaData** | [**ReqRsGxsForumsGetForumMsgMetaData**](ReqRsGxsForumsGetForumMsgMetaData.md)| forumId: &gt;              (RsGxsGroupId)id of the forum of which the content is requested   | [optional] 

### Return type

[**RespRsGxsForumsGetForumMsgMetaData**](RespRsGxsForumsGetForumMsgMetaData.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsGetForumsInfo

> RespRsGxsForumsGetForumsInfo rsGxsForumsGetForumsInfo(opts)

Get forums information (description, thumbnail...). Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsGetForumsInfo': {"forumIds":"(list<RsGxsGroupId>)ids of the forums of which to get the informations \n"} // ReqRsGxsForumsGetForumsInfo | forumIds: >              (list<RsGxsGroupId>)ids of the forums of which to get the informations  
};
apiInstance.rsGxsForumsGetForumsInfo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsGetForumsInfo** | [**ReqRsGxsForumsGetForumsInfo**](ReqRsGxsForumsGetForumsInfo.md)| forumIds: &gt;              (list&lt;RsGxsGroupId&gt;)ids of the forums of which to get the informations   | [optional] 

### Return type

[**RespRsGxsForumsGetForumsInfo**](RespRsGxsForumsGetForumsInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsGetForumsSummaries

> RespRsGxsForumsGetForumsSummaries rsGxsForumsGetForumsSummaries()

Get forums summaries list. Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsGxsForumsGetForumsSummaries((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsGxsForumsGetForumsSummaries**](RespRsGxsForumsGetForumsSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsGxsForumsMarkRead

> RespRsGxsForumsMarkRead rsGxsForumsMarkRead(opts)

Toggle message read status. Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsMarkRead': {"messageId":"(RsGxsGrpMsgIdPair)post identifier \n","read":"(boolean)true to mark as read, false to mark as unread \n"} // ReqRsGxsForumsMarkRead | messageId: >              (RsGxsGrpMsgIdPair)post identifier          read: >              (boolean)true to mark as read, false to mark as unread  
};
apiInstance.rsGxsForumsMarkRead(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsMarkRead** | [**ReqRsGxsForumsMarkRead**](ReqRsGxsForumsMarkRead.md)| messageId: &gt;              (RsGxsGrpMsgIdPair)post identifier          read: &gt;              (boolean)true to mark as read, false to mark as unread   | [optional] 

### Return type

[**RespRsGxsForumsMarkRead**](RespRsGxsForumsMarkRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsRequestStatus

> RespRsGxsForumsRequestStatus rsGxsForumsRequestStatus(opts)

null

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsRequestStatus': {"token":"(integer)None\n"} // ReqRsGxsForumsRequestStatus | token: >              (integer)None 
};
apiInstance.rsGxsForumsRequestStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsRequestStatus** | [**ReqRsGxsForumsRequestStatus**](ReqRsGxsForumsRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**RespRsGxsForumsRequestStatus**](RespRsGxsForumsRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsGxsForumsSubscribeToForum

> RespRsGxsForumsSubscribeToForum rsGxsForumsSubscribeToForum(opts)

Subscrbe to a forum. Blocking API

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsGxsForumsSubscribeToForum': {"forumId":"(RsGxsGroupId)Forum id \n","subscribe":"(boolean)true to subscribe, false to unsubscribe \n"} // ReqRsGxsForumsSubscribeToForum | forumId: >              (RsGxsGroupId)Forum id          subscribe: >              (boolean)true to subscribe, false to unsubscribe  
};
apiInstance.rsGxsForumsSubscribeToForum(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsGxsForumsSubscribeToForum** | [**ReqRsGxsForumsSubscribeToForum**](ReqRsGxsForumsSubscribeToForum.md)| forumId: &gt;              (RsGxsGroupId)Forum id          subscribe: &gt;              (boolean)true to subscribe, false to unsubscribe   | [optional] 

### Return type

[**RespRsGxsForumsSubscribeToForum**](RespRsGxsForumsSubscribeToForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityAutoAddFriendIdsAsContact

> RespRsIdentityAutoAddFriendIdsAsContact rsIdentityAutoAddFriendIdsAsContact()

Check if automatic signed by friend identity contact flagging is enabled

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsIdentityAutoAddFriendIdsAsContact((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsIdentityAutoAddFriendIdsAsContact**](RespRsIdentityAutoAddFriendIdsAsContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsIdentityCreateIdentity

> RespRsIdentityCreateIdentity rsIdentityCreateIdentity(opts)

Create a new identity

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityCreateIdentity': {"name":"(string)Name of the identity \n","avatar":"(RsGxsImage)Image associated to the identity \n","pseudonimous":"(boolean)true for unsigned identity, false otherwise \n","pgpPassword":"(string)password to unlock PGP to sign identity, not implemented yet \n"} // ReqRsIdentityCreateIdentity | name: >              (string)Name of the identity          avatar: >              (RsGxsImage)Image associated to the identity          pseudonimous: >              (boolean)true for unsigned identity, false otherwise          pgpPassword: >              (string)password to unlock PGP to sign identity, not implemented yet  
};
apiInstance.rsIdentityCreateIdentity(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityCreateIdentity** | [**ReqRsIdentityCreateIdentity**](ReqRsIdentityCreateIdentity.md)| name: &gt;              (string)Name of the identity          avatar: &gt;              (RsGxsImage)Image associated to the identity          pseudonimous: &gt;              (boolean)true for unsigned identity, false otherwise          pgpPassword: &gt;              (string)password to unlock PGP to sign identity, not implemented yet   | [optional] 

### Return type

[**RespRsIdentityCreateIdentity**](RespRsIdentityCreateIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityDeleteBannedNodesThreshold

> RespRsIdentityDeleteBannedNodesThreshold rsIdentityDeleteBannedNodesThreshold()

Get number of days after which delete a banned identities

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsIdentityDeleteBannedNodesThreshold((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsIdentityDeleteBannedNodesThreshold**](RespRsIdentityDeleteBannedNodesThreshold.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsIdentityDeleteIdentity

> RespRsIdentityDeleteIdentity rsIdentityDeleteIdentity(opts)

Locally delete given identity

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityDeleteIdentity': {"id":"(RsGxsId)Id of the identity \n"} // ReqRsIdentityDeleteIdentity | id: >              (RsGxsId)Id of the identity  
};
apiInstance.rsIdentityDeleteIdentity(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityDeleteIdentity** | [**ReqRsIdentityDeleteIdentity**](ReqRsIdentityDeleteIdentity.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**RespRsIdentityDeleteIdentity**](RespRsIdentityDeleteIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityGetIdDetails

> RespRsIdentityGetIdDetails rsIdentityGetIdDetails(opts)

Get identity details, from the cache

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityGetIdDetails': {"id":"(RsGxsId)Id of the identity \n"} // ReqRsIdentityGetIdDetails | id: >              (RsGxsId)Id of the identity  
};
apiInstance.rsIdentityGetIdDetails(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityGetIdDetails** | [**ReqRsIdentityGetIdDetails**](ReqRsIdentityGetIdDetails.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**RespRsIdentityGetIdDetails**](RespRsIdentityGetIdDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityGetIdentitiesInfo

> RespRsIdentityGetIdentitiesInfo rsIdentityGetIdentitiesInfo(opts)

Get identities information (name, avatar...). Blocking API.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityGetIdentitiesInfo': {"ids":"(set<RsGxsId>)ids of the channels of which to get the informations \n"} // ReqRsIdentityGetIdentitiesInfo | ids: >              (set<RsGxsId>)ids of the channels of which to get the informations  
};
apiInstance.rsIdentityGetIdentitiesInfo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityGetIdentitiesInfo** | [**ReqRsIdentityGetIdentitiesInfo**](ReqRsIdentityGetIdentitiesInfo.md)| ids: &gt;              (set&lt;RsGxsId&gt;)ids of the channels of which to get the informations   | [optional] 

### Return type

[**RespRsIdentityGetIdentitiesInfo**](RespRsIdentityGetIdentitiesInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityGetIdentitiesSummaries

> RespRsIdentityGetIdentitiesSummaries rsIdentityGetIdentitiesSummaries()

Get identities summaries list.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsIdentityGetIdentitiesSummaries((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsIdentityGetIdentitiesSummaries**](RespRsIdentityGetIdentitiesSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsIdentityGetLastUsageTS

> RespRsIdentityGetLastUsageTS rsIdentityGetLastUsageTS(opts)

Get last seen usage time of given identity

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityGetLastUsageTS': {"id":"(RsGxsId)Id of the identity \n"} // ReqRsIdentityGetLastUsageTS | id: >              (RsGxsId)Id of the identity  
};
apiInstance.rsIdentityGetLastUsageTS(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityGetLastUsageTS** | [**ReqRsIdentityGetLastUsageTS**](ReqRsIdentityGetLastUsageTS.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**RespRsIdentityGetLastUsageTS**](RespRsIdentityGetLastUsageTS.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityGetOwnPseudonimousIds

> RespRsIdentityGetOwnPseudonimousIds rsIdentityGetOwnPseudonimousIds()

Get own pseudonimous (unsigned) ids

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsIdentityGetOwnPseudonimousIds((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsIdentityGetOwnPseudonimousIds**](RespRsIdentityGetOwnPseudonimousIds.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsIdentityGetOwnSignedIds

> RespRsIdentityGetOwnSignedIds rsIdentityGetOwnSignedIds()

Get own signed ids

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsIdentityGetOwnSignedIds((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsIdentityGetOwnSignedIds**](RespRsIdentityGetOwnSignedIds.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsIdentityIdentityFromBase64

> RespRsIdentityIdentityFromBase64 rsIdentityIdentityFromBase64(opts)

Import identity from base64 representation

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityIdentityFromBase64': {"base64String":"(string)base64 representation of the identity to import \n"} // ReqRsIdentityIdentityFromBase64 | base64String: >              (string)base64 representation of the identity to import  
};
apiInstance.rsIdentityIdentityFromBase64(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityIdentityFromBase64** | [**ReqRsIdentityIdentityFromBase64**](ReqRsIdentityIdentityFromBase64.md)| base64String: &gt;              (string)base64 representation of the identity to import   | [optional] 

### Return type

[**RespRsIdentityIdentityFromBase64**](RespRsIdentityIdentityFromBase64.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityIdentityToBase64

> RespRsIdentityIdentityToBase64 rsIdentityIdentityToBase64(opts)

Get base64 representation of an identity

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityIdentityToBase64': {"id":"(RsGxsId)Id of the identity \n"} // ReqRsIdentityIdentityToBase64 | id: >              (RsGxsId)Id of the identity  
};
apiInstance.rsIdentityIdentityToBase64(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityIdentityToBase64** | [**ReqRsIdentityIdentityToBase64**](ReqRsIdentityIdentityToBase64.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**RespRsIdentityIdentityToBase64**](RespRsIdentityIdentityToBase64.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityIsARegularContact

> RespRsIdentityIsARegularContact rsIdentityIsARegularContact(opts)

Check if an identity is contact

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityIsARegularContact': {"id":"(RsGxsId)Id of the identity \n"} // ReqRsIdentityIsARegularContact | id: >              (RsGxsId)Id of the identity  
};
apiInstance.rsIdentityIsARegularContact(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityIsARegularContact** | [**ReqRsIdentityIsARegularContact**](ReqRsIdentityIsARegularContact.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**RespRsIdentityIsARegularContact**](RespRsIdentityIsARegularContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityIsOwnId

> RespRsIdentityIsOwnId rsIdentityIsOwnId(opts)

Check if an id is own

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityIsOwnId': {"id":"(RsGxsId)Id to check \n"} // ReqRsIdentityIsOwnId | id: >              (RsGxsId)Id to check  
};
apiInstance.rsIdentityIsOwnId(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityIsOwnId** | [**ReqRsIdentityIsOwnId**](ReqRsIdentityIsOwnId.md)| id: &gt;              (RsGxsId)Id to check   | [optional] 

### Return type

[**RespRsIdentityIsOwnId**](RespRsIdentityIsOwnId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityRequestIdentity

> RespRsIdentityRequestIdentity rsIdentityRequestIdentity(opts)

request details of a not yet known identity to the network

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityRequestIdentity': {"id":"(RsGxsId)id of the identity to request \n"} // ReqRsIdentityRequestIdentity | id: >              (RsGxsId)id of the identity to request  
};
apiInstance.rsIdentityRequestIdentity(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityRequestIdentity** | [**ReqRsIdentityRequestIdentity**](ReqRsIdentityRequestIdentity.md)| id: &gt;              (RsGxsId)id of the identity to request   | [optional] 

### Return type

[**RespRsIdentityRequestIdentity**](RespRsIdentityRequestIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityRequestStatus

> RespRsIdentityRequestStatus rsIdentityRequestStatus(opts)

null

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityRequestStatus': {"token":"(integer)None\n"} // ReqRsIdentityRequestStatus | token: >              (integer)None 
};
apiInstance.rsIdentityRequestStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityRequestStatus** | [**ReqRsIdentityRequestStatus**](ReqRsIdentityRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**RespRsIdentityRequestStatus**](RespRsIdentityRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentitySetAsRegularContact

> RespRsIdentitySetAsRegularContact rsIdentitySetAsRegularContact(opts)

Set/unset identity as contact

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentitySetAsRegularContact': {"id":"(RsGxsId)Id of the identity \n","isContact":"(boolean)true to set, false to unset \n"} // ReqRsIdentitySetAsRegularContact | id: >              (RsGxsId)Id of the identity          isContact: >              (boolean)true to set, false to unset  
};
apiInstance.rsIdentitySetAsRegularContact(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentitySetAsRegularContact** | [**ReqRsIdentitySetAsRegularContact**](ReqRsIdentitySetAsRegularContact.md)| id: &gt;              (RsGxsId)Id of the identity          isContact: &gt;              (boolean)true to set, false to unset   | [optional] 

### Return type

[**RespRsIdentitySetAsRegularContact**](RespRsIdentitySetAsRegularContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentitySetAutoAddFriendIdsAsContact

> Object rsIdentitySetAutoAddFriendIdsAsContact(opts)

Toggle automatic flagging signed by friends identity as contact

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentitySetAutoAddFriendIdsAsContact': {"enabled":"(boolean)true to enable, false to disable \n"} // ReqRsIdentitySetAutoAddFriendIdsAsContact | enabled: >              (boolean)true to enable, false to disable  
};
apiInstance.rsIdentitySetAutoAddFriendIdsAsContact(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentitySetAutoAddFriendIdsAsContact** | [**ReqRsIdentitySetAutoAddFriendIdsAsContact**](ReqRsIdentitySetAutoAddFriendIdsAsContact.md)| enabled: &gt;              (boolean)true to enable, false to disable   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentitySetDeleteBannedNodesThreshold

> Object rsIdentitySetDeleteBannedNodesThreshold(opts)

Set number of days after which delete a banned identities

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentitySetDeleteBannedNodesThreshold': {"days":"(integer)number of days \n"} // ReqRsIdentitySetDeleteBannedNodesThreshold | days: >              (integer)number of days  
};
apiInstance.rsIdentitySetDeleteBannedNodesThreshold(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentitySetDeleteBannedNodesThreshold** | [**ReqRsIdentitySetDeleteBannedNodesThreshold**](ReqRsIdentitySetDeleteBannedNodesThreshold.md)| days: &gt;              (integer)number of days   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsIdentityUpdateIdentity

> RespRsIdentityUpdateIdentity rsIdentityUpdateIdentity(opts)

Update identity data (name, avatar...)

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsIdentityUpdateIdentity': {"identityData":"(RsGxsIdGroup)updated identiy data \n"} // ReqRsIdentityUpdateIdentity | identityData: >              (RsGxsIdGroup)updated identiy data  
};
apiInstance.rsIdentityUpdateIdentity(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsIdentityUpdateIdentity** | [**ReqRsIdentityUpdateIdentity**](ReqRsIdentityUpdateIdentity.md)| identityData: &gt;              (RsGxsIdGroup)updated identiy data   | [optional] 

### Return type

[**RespRsIdentityUpdateIdentity**](RespRsIdentityUpdateIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsLoginHelperAttemptLogin

> RespRsLoginHelperAttemptLogin rsLoginHelperAttemptLogin(opts)

Normal way to attempt login

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsLoginHelperAttemptLogin': {"account":"(RsPeerId)Id of the account to which attempt login \n","password":"(string)Password for the given account \n"} // ReqRsLoginHelperAttemptLogin | account: >              (RsPeerId)Id of the account to which attempt login          password: >              (string)Password for the given account  
};
apiInstance.rsLoginHelperAttemptLogin(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsLoginHelperAttemptLogin** | [**ReqRsLoginHelperAttemptLogin**](ReqRsLoginHelperAttemptLogin.md)| account: &gt;              (RsPeerId)Id of the account to which attempt login          password: &gt;              (string)Password for the given account   | [optional] 

### Return type

[**RespRsLoginHelperAttemptLogin**](RespRsLoginHelperAttemptLogin.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsLoginHelperCollectEntropy

> RespRsLoginHelperCollectEntropy rsLoginHelperCollectEntropy(opts)

Feed extra entropy to the crypto libraries

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsLoginHelperCollectEntropy': {"bytes":"(integer)number to feed to the entropy pool \n"} // ReqRsLoginHelperCollectEntropy | bytes: >              (integer)number to feed to the entropy pool  
};
apiInstance.rsLoginHelperCollectEntropy(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsLoginHelperCollectEntropy** | [**ReqRsLoginHelperCollectEntropy**](ReqRsLoginHelperCollectEntropy.md)| bytes: &gt;              (integer)number to feed to the entropy pool   | [optional] 

### Return type

[**RespRsLoginHelperCollectEntropy**](RespRsLoginHelperCollectEntropy.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsLoginHelperCreateLocation

> RespRsLoginHelperCreateLocation rsLoginHelperCreateLocation(opts)

Creates a new RetroShare location, and log in once is created

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsLoginHelperCreateLocation': {"location":"(RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location \n","password":"(string)to protect and unlock the associated PGP key \n","makeHidden":"(boolean)pass true to create an hidden location. UNTESTED! \n","makeAutoTor":"(boolean)pass true to create an automatically configured Tor hidden location. UNTESTED! \n"} // ReqRsLoginHelperCreateLocation | location: >              (RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location          password: >              (string)to protect and unlock the associated PGP key          makeHidden: >              (boolean)pass true to create an hidden location. UNTESTED!          makeAutoTor: >              (boolean)pass true to create an automatically configured Tor hidden location. UNTESTED!  
};
apiInstance.rsLoginHelperCreateLocation(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsLoginHelperCreateLocation** | [**ReqRsLoginHelperCreateLocation**](ReqRsLoginHelperCreateLocation.md)| location: &gt;              (RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location          password: &gt;              (string)to protect and unlock the associated PGP key          makeHidden: &gt;              (boolean)pass true to create an hidden location. UNTESTED!          makeAutoTor: &gt;              (boolean)pass true to create an automatically configured Tor hidden location. UNTESTED!   | [optional] 

### Return type

[**RespRsLoginHelperCreateLocation**](RespRsLoginHelperCreateLocation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsLoginHelperGetLocations

> RespRsLoginHelperGetLocations rsLoginHelperGetLocations()

Get locations and associated information

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsLoginHelperGetLocations((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsLoginHelperGetLocations**](RespRsLoginHelperGetLocations.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsLoginHelperIsLoggedIn

> RespRsLoginHelperIsLoggedIn rsLoginHelperIsLoggedIn()

Check if RetroShare is already logged in, this usually return true after a successfull

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsLoginHelperIsLoggedIn((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsLoginHelperIsLoggedIn**](RespRsLoginHelperIsLoggedIn.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsAcceptLobbyInvite

> RespRsMsgsAcceptLobbyInvite rsMsgsAcceptLobbyInvite(opts)

acceptLobbyInvite accept a chat invite

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsAcceptLobbyInvite': {"id":"(ChatLobbyId)chat lobby id you were invited into and you want to join \n","identity":"(RsGxsId)chat identity to use \n"} // ReqRsMsgsAcceptLobbyInvite | id: >              (ChatLobbyId)chat lobby id you were invited into and you want to join          identity: >              (RsGxsId)chat identity to use  
};
apiInstance.rsMsgsAcceptLobbyInvite(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsAcceptLobbyInvite** | [**ReqRsMsgsAcceptLobbyInvite**](ReqRsMsgsAcceptLobbyInvite.md)| id: &gt;              (ChatLobbyId)chat lobby id you were invited into and you want to join          identity: &gt;              (RsGxsId)chat identity to use   | [optional] 

### Return type

[**RespRsMsgsAcceptLobbyInvite**](RespRsMsgsAcceptLobbyInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsClearChatLobby

> Object rsMsgsClearChatLobby(opts)

clearChatLobby clear a chat lobby

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsClearChatLobby': {"id":"(ChatId)chat lobby id to clear \n"} // ReqRsMsgsClearChatLobby | id: >              (ChatId)chat lobby id to clear  
};
apiInstance.rsMsgsClearChatLobby(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsClearChatLobby** | [**ReqRsMsgsClearChatLobby**](ReqRsMsgsClearChatLobby.md)| id: &gt;              (ChatId)chat lobby id to clear   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsCreateChatLobby

> RespRsMsgsCreateChatLobby rsMsgsCreateChatLobby(opts)

createChatLobby create a new chat lobby

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsCreateChatLobby': {"lobby_name":"(string)lobby name \n","lobby_identity":"(RsGxsId)chat id to use for new lobby \n","lobby_topic":"(string)lobby toppic \n","invited_friends":"(set<RsPeerId>)list of friends to invite \n","lobby_privacy_type":"(ChatLobbyFlags)flag for new chat lobby \n"} // ReqRsMsgsCreateChatLobby | lobby_name: >              (string)lobby name          lobby_identity: >              (RsGxsId)chat id to use for new lobby          lobby_topic: >              (string)lobby toppic          invited_friends: >              (set<RsPeerId>)list of friends to invite          lobby_privacy_type: >              (ChatLobbyFlags)flag for new chat lobby  
};
apiInstance.rsMsgsCreateChatLobby(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsCreateChatLobby** | [**ReqRsMsgsCreateChatLobby**](ReqRsMsgsCreateChatLobby.md)| lobby_name: &gt;              (string)lobby name          lobby_identity: &gt;              (RsGxsId)chat id to use for new lobby          lobby_topic: &gt;              (string)lobby toppic          invited_friends: &gt;              (set&lt;RsPeerId&gt;)list of friends to invite          lobby_privacy_type: &gt;              (ChatLobbyFlags)flag for new chat lobby   | [optional] 

### Return type

[**RespRsMsgsCreateChatLobby**](RespRsMsgsCreateChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsDenyLobbyInvite

> Object rsMsgsDenyLobbyInvite(opts)

denyLobbyInvite deny a chat lobby invite

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsDenyLobbyInvite': {"id":"(ChatLobbyId)chat lobby id you were invited into \n"} // ReqRsMsgsDenyLobbyInvite | id: >              (ChatLobbyId)chat lobby id you were invited into  
};
apiInstance.rsMsgsDenyLobbyInvite(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsDenyLobbyInvite** | [**ReqRsMsgsDenyLobbyInvite**](ReqRsMsgsDenyLobbyInvite.md)| id: &gt;              (ChatLobbyId)chat lobby id you were invited into   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetChatLobbyInfo

> RespRsMsgsGetChatLobbyInfo rsMsgsGetChatLobbyInfo(opts)

getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsGetChatLobbyInfo': {"id":"(ChatLobbyId)id to get infos from \n"} // ReqRsMsgsGetChatLobbyInfo | id: >              (ChatLobbyId)id to get infos from  
};
apiInstance.rsMsgsGetChatLobbyInfo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsGetChatLobbyInfo** | [**ReqRsMsgsGetChatLobbyInfo**](ReqRsMsgsGetChatLobbyInfo.md)| id: &gt;              (ChatLobbyId)id to get infos from   | [optional] 

### Return type

[**RespRsMsgsGetChatLobbyInfo**](RespRsMsgsGetChatLobbyInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetChatLobbyList

> RespRsMsgsGetChatLobbyList rsMsgsGetChatLobbyList()

getChatLobbyList get ids of subscribed lobbies

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsMsgsGetChatLobbyList((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetChatLobbyList**](RespRsMsgsGetChatLobbyList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsGetCustomStateString

> RespRsMsgsGetCustomStateString rsMsgsGetCustomStateString(opts)

getCustomStateString get the custom status message from a peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsGetCustomStateString': {"peer_id":"(RsPeerId)peer id to the peer you want to get the status message from \n"} // ReqRsMsgsGetCustomStateString | peer_id: >              (RsPeerId)peer id to the peer you want to get the status message from  
};
apiInstance.rsMsgsGetCustomStateString(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsGetCustomStateString** | [**ReqRsMsgsGetCustomStateString**](ReqRsMsgsGetCustomStateString.md)| peer_id: &gt;              (RsPeerId)peer id to the peer you want to get the status message from   | [optional] 

### Return type

[**RespRsMsgsGetCustomStateString**](RespRsMsgsGetCustomStateString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetDefaultIdentityForChatLobby

> RespRsMsgsGetDefaultIdentityForChatLobby rsMsgsGetDefaultIdentityForChatLobby()

getDefaultIdentityForChatLobby get the default identity used for chat lobbies

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsMsgsGetDefaultIdentityForChatLobby((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetDefaultIdentityForChatLobby**](RespRsMsgsGetDefaultIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsGetIdentityForChatLobby

> RespRsMsgsGetIdentityForChatLobby rsMsgsGetIdentityForChatLobby(opts)

getIdentityForChatLobby

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsGetIdentityForChatLobby': {"lobby_id":"(ChatLobbyId)lobby to get the chat id from \n"} // ReqRsMsgsGetIdentityForChatLobby | lobby_id: >              (ChatLobbyId)lobby to get the chat id from  
};
apiInstance.rsMsgsGetIdentityForChatLobby(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsGetIdentityForChatLobby** | [**ReqRsMsgsGetIdentityForChatLobby**](ReqRsMsgsGetIdentityForChatLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby to get the chat id from   | [optional] 

### Return type

[**RespRsMsgsGetIdentityForChatLobby**](RespRsMsgsGetIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetListOfNearbyChatLobbies

> RespRsMsgsGetListOfNearbyChatLobbies rsMsgsGetListOfNearbyChatLobbies()

getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsMsgsGetListOfNearbyChatLobbies((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetListOfNearbyChatLobbies**](RespRsMsgsGetListOfNearbyChatLobbies.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsGetLobbyAutoSubscribe

> RespRsMsgsGetLobbyAutoSubscribe rsMsgsGetLobbyAutoSubscribe(opts)

getLobbyAutoSubscribe get current value of auto subscribe

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsGetLobbyAutoSubscribe': {"lobby_id":"(ChatLobbyId)lobby to get value from \n"} // ReqRsMsgsGetLobbyAutoSubscribe | lobby_id: >              (ChatLobbyId)lobby to get value from  
};
apiInstance.rsMsgsGetLobbyAutoSubscribe(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsGetLobbyAutoSubscribe** | [**ReqRsMsgsGetLobbyAutoSubscribe**](ReqRsMsgsGetLobbyAutoSubscribe.md)| lobby_id: &gt;              (ChatLobbyId)lobby to get value from   | [optional] 

### Return type

[**RespRsMsgsGetLobbyAutoSubscribe**](RespRsMsgsGetLobbyAutoSubscribe.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetMaxMessageSecuritySize

> RespRsMsgsGetMaxMessageSecuritySize rsMsgsGetMaxMessageSecuritySize(opts)

getMaxMessageSecuritySize get the maximum size of a chta message

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsGetMaxMessageSecuritySize': {"type":"(integer)chat type \n"} // ReqRsMsgsGetMaxMessageSecuritySize | type: >              (integer)chat type  
};
apiInstance.rsMsgsGetMaxMessageSecuritySize(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsGetMaxMessageSecuritySize** | [**ReqRsMsgsGetMaxMessageSecuritySize**](ReqRsMsgsGetMaxMessageSecuritySize.md)| type: &gt;              (integer)chat type   | [optional] 

### Return type

[**RespRsMsgsGetMaxMessageSecuritySize**](RespRsMsgsGetMaxMessageSecuritySize.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetMessage

> RespRsMsgsGetMessage rsMsgsGetMessage(opts)

getMessage

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsGetMessage': {"msgId":"(string)message ID to lookup \n"} // ReqRsMsgsGetMessage | msgId: >              (string)message ID to lookup  
};
apiInstance.rsMsgsGetMessage(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsGetMessage** | [**ReqRsMsgsGetMessage**](ReqRsMsgsGetMessage.md)| msgId: &gt;              (string)message ID to lookup   | [optional] 

### Return type

[**RespRsMsgsGetMessage**](RespRsMsgsGetMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetMessageCount

> RespRsMsgsGetMessageCount rsMsgsGetMessageCount()

getMessageCount

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsMsgsGetMessageCount((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetMessageCount**](RespRsMsgsGetMessageCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsGetMessageSummaries

> RespRsMsgsGetMessageSummaries rsMsgsGetMessageSummaries()

getMessageSummaries

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsMsgsGetMessageSummaries((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetMessageSummaries**](RespRsMsgsGetMessageSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsGetMessageTag

> RespRsMsgsGetMessageTag rsMsgsGetMessageTag(opts)

getMessageTag

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsGetMessageTag': {"msgId":"(string)None\n"} // ReqRsMsgsGetMessageTag | msgId: >              (string)None 
};
apiInstance.rsMsgsGetMessageTag(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsGetMessageTag** | [**ReqRsMsgsGetMessageTag**](ReqRsMsgsGetMessageTag.md)| msgId: &gt;              (string)None  | [optional] 

### Return type

[**RespRsMsgsGetMessageTag**](RespRsMsgsGetMessageTag.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetMessageTagTypes

> RespRsMsgsGetMessageTagTypes rsMsgsGetMessageTagTypes()

getMessageTagTypes

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsMsgsGetMessageTagTypes((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetMessageTagTypes**](RespRsMsgsGetMessageTagTypes.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsGetMsgParentId

> RespRsMsgsGetMsgParentId rsMsgsGetMsgParentId(opts)

getMsgParentId

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsGetMsgParentId': {"msgId":"(string)None\n"} // ReqRsMsgsGetMsgParentId | msgId: >              (string)None 
};
apiInstance.rsMsgsGetMsgParentId(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsGetMsgParentId** | [**ReqRsMsgsGetMsgParentId**](ReqRsMsgsGetMsgParentId.md)| msgId: &gt;              (string)None  | [optional] 

### Return type

[**RespRsMsgsGetMsgParentId**](RespRsMsgsGetMsgParentId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsGetPendingChatLobbyInvites

> RespRsMsgsGetPendingChatLobbyInvites rsMsgsGetPendingChatLobbyInvites()

getPendingChatLobbyInvites get a list of all pending chat lobby invites

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsMsgsGetPendingChatLobbyInvites((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsMsgsGetPendingChatLobbyInvites**](RespRsMsgsGetPendingChatLobbyInvites.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsInvitePeerToLobby

> Object rsMsgsInvitePeerToLobby(opts)

invitePeerToLobby invite a peer to join a lobby

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsInvitePeerToLobby': {"lobby_id":"(ChatLobbyId)lobby it to invite into \n","peer_id":"(RsPeerId)peer to invite \n"} // ReqRsMsgsInvitePeerToLobby | lobby_id: >              (ChatLobbyId)lobby it to invite into          peer_id: >              (RsPeerId)peer to invite  
};
apiInstance.rsMsgsInvitePeerToLobby(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsInvitePeerToLobby** | [**ReqRsMsgsInvitePeerToLobby**](ReqRsMsgsInvitePeerToLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby it to invite into          peer_id: &gt;              (RsPeerId)peer to invite   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsJoinVisibleChatLobby

> RespRsMsgsJoinVisibleChatLobby rsMsgsJoinVisibleChatLobby(opts)

joinVisibleChatLobby join a lobby that is visible

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsJoinVisibleChatLobby': {"lobby_id":"(ChatLobbyId)lobby to join to \n","own_id":"(RsGxsId)chat id to use \n"} // ReqRsMsgsJoinVisibleChatLobby | lobby_id: >              (ChatLobbyId)lobby to join to          own_id: >              (RsGxsId)chat id to use  
};
apiInstance.rsMsgsJoinVisibleChatLobby(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsJoinVisibleChatLobby** | [**ReqRsMsgsJoinVisibleChatLobby**](ReqRsMsgsJoinVisibleChatLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby to join to          own_id: &gt;              (RsGxsId)chat id to use   | [optional] 

### Return type

[**RespRsMsgsJoinVisibleChatLobby**](RespRsMsgsJoinVisibleChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageDelete

> RespRsMsgsMessageDelete rsMsgsMessageDelete(opts)

MessageDelete

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageDelete': {"msgId":"(string)None\n"} // ReqRsMsgsMessageDelete | msgId: >              (string)None 
};
apiInstance.rsMsgsMessageDelete(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageDelete** | [**ReqRsMsgsMessageDelete**](ReqRsMsgsMessageDelete.md)| msgId: &gt;              (string)None  | [optional] 

### Return type

[**RespRsMsgsMessageDelete**](RespRsMsgsMessageDelete.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageForwarded

> RespRsMsgsMessageForwarded rsMsgsMessageForwarded(opts)

MessageForwarded

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageForwarded': {"msgId":"(string)None\n","forwarded":"(boolean)None\n"} // ReqRsMsgsMessageForwarded | msgId: >              (string)None         forwarded: >              (boolean)None 
};
apiInstance.rsMsgsMessageForwarded(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageForwarded** | [**ReqRsMsgsMessageForwarded**](ReqRsMsgsMessageForwarded.md)| msgId: &gt;              (string)None         forwarded: &gt;              (boolean)None  | [optional] 

### Return type

[**RespRsMsgsMessageForwarded**](RespRsMsgsMessageForwarded.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageLoadEmbeddedImages

> RespRsMsgsMessageLoadEmbeddedImages rsMsgsMessageLoadEmbeddedImages(opts)

MessageLoadEmbeddedImages

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageLoadEmbeddedImages': {"msgId":"(string)None\n","load":"(boolean)None\n"} // ReqRsMsgsMessageLoadEmbeddedImages | msgId: >              (string)None         load: >              (boolean)None 
};
apiInstance.rsMsgsMessageLoadEmbeddedImages(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageLoadEmbeddedImages** | [**ReqRsMsgsMessageLoadEmbeddedImages**](ReqRsMsgsMessageLoadEmbeddedImages.md)| msgId: &gt;              (string)None         load: &gt;              (boolean)None  | [optional] 

### Return type

[**RespRsMsgsMessageLoadEmbeddedImages**](RespRsMsgsMessageLoadEmbeddedImages.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageRead

> RespRsMsgsMessageRead rsMsgsMessageRead(opts)

MessageRead

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageRead': {"msgId":"(string)None\n","unreadByUser":"(boolean)None\n"} // ReqRsMsgsMessageRead | msgId: >              (string)None         unreadByUser: >              (boolean)None 
};
apiInstance.rsMsgsMessageRead(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageRead** | [**ReqRsMsgsMessageRead**](ReqRsMsgsMessageRead.md)| msgId: &gt;              (string)None         unreadByUser: &gt;              (boolean)None  | [optional] 

### Return type

[**RespRsMsgsMessageRead**](RespRsMsgsMessageRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageReplied

> RespRsMsgsMessageReplied rsMsgsMessageReplied(opts)

MessageReplied

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageReplied': {"msgId":"(string)None\n","replied":"(boolean)None\n"} // ReqRsMsgsMessageReplied | msgId: >              (string)None         replied: >              (boolean)None 
};
apiInstance.rsMsgsMessageReplied(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageReplied** | [**ReqRsMsgsMessageReplied**](ReqRsMsgsMessageReplied.md)| msgId: &gt;              (string)None         replied: &gt;              (boolean)None  | [optional] 

### Return type

[**RespRsMsgsMessageReplied**](RespRsMsgsMessageReplied.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageSend

> RespRsMsgsMessageSend rsMsgsMessageSend(opts)

MessageSend

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageSend': {"info":"(Rs_Msgs_MessageInfo)None\n"} // ReqRsMsgsMessageSend | info: >              (Rs_Msgs_MessageInfo)None 
};
apiInstance.rsMsgsMessageSend(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageSend** | [**ReqRsMsgsMessageSend**](ReqRsMsgsMessageSend.md)| info: &gt;              (Rs_Msgs_MessageInfo)None  | [optional] 

### Return type

[**RespRsMsgsMessageSend**](RespRsMsgsMessageSend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageStar

> RespRsMsgsMessageStar rsMsgsMessageStar(opts)

MessageStar

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageStar': {"msgId":"(string)None\n","mark":"(boolean)None\n"} // ReqRsMsgsMessageStar | msgId: >              (string)None         mark: >              (boolean)None 
};
apiInstance.rsMsgsMessageStar(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageStar** | [**ReqRsMsgsMessageStar**](ReqRsMsgsMessageStar.md)| msgId: &gt;              (string)None         mark: &gt;              (boolean)None  | [optional] 

### Return type

[**RespRsMsgsMessageStar**](RespRsMsgsMessageStar.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageToDraft

> RespRsMsgsMessageToDraft rsMsgsMessageToDraft(opts)

MessageToDraft

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageToDraft': {"info":"(Rs_Msgs_MessageInfo)None\n","msgParentId":"(string)None\n"} // ReqRsMsgsMessageToDraft | info: >              (Rs_Msgs_MessageInfo)None         msgParentId: >              (string)None 
};
apiInstance.rsMsgsMessageToDraft(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageToDraft** | [**ReqRsMsgsMessageToDraft**](ReqRsMsgsMessageToDraft.md)| info: &gt;              (Rs_Msgs_MessageInfo)None         msgParentId: &gt;              (string)None  | [optional] 

### Return type

[**RespRsMsgsMessageToDraft**](RespRsMsgsMessageToDraft.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsMessageToTrash

> RespRsMsgsMessageToTrash rsMsgsMessageToTrash(opts)

MessageToTrash

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsMessageToTrash': {"msgId":"(string)Id of the message to mode to trash box \n","bTrash":"(boolean)Move to trash if true, otherwise remove from trash \n"} // ReqRsMsgsMessageToTrash | msgId: >              (string)Id of the message to mode to trash box          bTrash: >              (boolean)Move to trash if true, otherwise remove from trash  
};
apiInstance.rsMsgsMessageToTrash(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsMessageToTrash** | [**ReqRsMsgsMessageToTrash**](ReqRsMsgsMessageToTrash.md)| msgId: &gt;              (string)Id of the message to mode to trash box          bTrash: &gt;              (boolean)Move to trash if true, otherwise remove from trash   | [optional] 

### Return type

[**RespRsMsgsMessageToTrash**](RespRsMsgsMessageToTrash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsRemoveMessageTagType

> RespRsMsgsRemoveMessageTagType rsMsgsRemoveMessageTagType(opts)

removeMessageTagType

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsRemoveMessageTagType': {"tagId":"(integer)None\n"} // ReqRsMsgsRemoveMessageTagType | tagId: >              (integer)None 
};
apiInstance.rsMsgsRemoveMessageTagType(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsRemoveMessageTagType** | [**ReqRsMsgsRemoveMessageTagType**](ReqRsMsgsRemoveMessageTagType.md)| tagId: &gt;              (integer)None  | [optional] 

### Return type

[**RespRsMsgsRemoveMessageTagType**](RespRsMsgsRemoveMessageTagType.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsResetMessageStandardTagTypes

> RespRsMsgsResetMessageStandardTagTypes rsMsgsResetMessageStandardTagTypes()

resetMessageStandardTagTypes

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsMsgsResetMessageStandardTagTypes((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsMsgsResetMessageStandardTagTypes**](RespRsMsgsResetMessageStandardTagTypes.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsMsgsSendChat

> RespRsMsgsSendChat rsMsgsSendChat(opts)

sendChat send a chat message to a given id

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSendChat': {"id":"(ChatId)id to send the message \n","msg":"(string)message to send \n"} // ReqRsMsgsSendChat | id: >              (ChatId)id to send the message          msg: >              (string)message to send  
};
apiInstance.rsMsgsSendChat(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSendChat** | [**ReqRsMsgsSendChat**](ReqRsMsgsSendChat.md)| id: &gt;              (ChatId)id to send the message          msg: &gt;              (string)message to send   | [optional] 

### Return type

[**RespRsMsgsSendChat**](RespRsMsgsSendChat.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSendLobbyStatusPeerLeaving

> Object rsMsgsSendLobbyStatusPeerLeaving(opts)

sendLobbyStatusPeerLeaving notify friend nodes that we&#39;re leaving a subscribed lobby

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSendLobbyStatusPeerLeaving': {"lobby_id":"(ChatLobbyId)lobby to leave \n"} // ReqRsMsgsSendLobbyStatusPeerLeaving | lobby_id: >              (ChatLobbyId)lobby to leave  
};
apiInstance.rsMsgsSendLobbyStatusPeerLeaving(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSendLobbyStatusPeerLeaving** | [**ReqRsMsgsSendLobbyStatusPeerLeaving**](ReqRsMsgsSendLobbyStatusPeerLeaving.md)| lobby_id: &gt;              (ChatLobbyId)lobby to leave   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSendStatusString

> Object rsMsgsSendStatusString(opts)

sendStatusString send a status string

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSendStatusString': {"id":"(ChatId)chat id to send the status string to \n","status_string":"(string)status string \n"} // ReqRsMsgsSendStatusString | id: >              (ChatId)chat id to send the status string to          status_string: >              (string)status string  
};
apiInstance.rsMsgsSendStatusString(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSendStatusString** | [**ReqRsMsgsSendStatusString**](ReqRsMsgsSendStatusString.md)| id: &gt;              (ChatId)chat id to send the status string to          status_string: &gt;              (string)status string   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSetCustomStateString

> Object rsMsgsSetCustomStateString(opts)

setCustomStateString set your custom status message

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSetCustomStateString': {"status_string":"(string)status message \n"} // ReqRsMsgsSetCustomStateString | status_string: >              (string)status message  
};
apiInstance.rsMsgsSetCustomStateString(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSetCustomStateString** | [**ReqRsMsgsSetCustomStateString**](ReqRsMsgsSetCustomStateString.md)| status_string: &gt;              (string)status message   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSetDefaultIdentityForChatLobby

> RespRsMsgsSetDefaultIdentityForChatLobby rsMsgsSetDefaultIdentityForChatLobby(opts)

setDefaultIdentityForChatLobby set the default identity used for chat lobbies

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSetDefaultIdentityForChatLobby': {"nick":"(RsGxsId)chat identitiy to use \n"} // ReqRsMsgsSetDefaultIdentityForChatLobby | nick: >              (RsGxsId)chat identitiy to use  
};
apiInstance.rsMsgsSetDefaultIdentityForChatLobby(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSetDefaultIdentityForChatLobby** | [**ReqRsMsgsSetDefaultIdentityForChatLobby**](ReqRsMsgsSetDefaultIdentityForChatLobby.md)| nick: &gt;              (RsGxsId)chat identitiy to use   | [optional] 

### Return type

[**RespRsMsgsSetDefaultIdentityForChatLobby**](RespRsMsgsSetDefaultIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSetIdentityForChatLobby

> RespRsMsgsSetIdentityForChatLobby rsMsgsSetIdentityForChatLobby(opts)

setIdentityForChatLobby set the chat identit

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSetIdentityForChatLobby': {"lobby_id":"(ChatLobbyId)lobby to change the chat idnetity for \n","nick":"(RsGxsId)new chat identity \n"} // ReqRsMsgsSetIdentityForChatLobby | lobby_id: >              (ChatLobbyId)lobby to change the chat idnetity for          nick: >              (RsGxsId)new chat identity  
};
apiInstance.rsMsgsSetIdentityForChatLobby(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSetIdentityForChatLobby** | [**ReqRsMsgsSetIdentityForChatLobby**](ReqRsMsgsSetIdentityForChatLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby to change the chat idnetity for          nick: &gt;              (RsGxsId)new chat identity   | [optional] 

### Return type

[**RespRsMsgsSetIdentityForChatLobby**](RespRsMsgsSetIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSetLobbyAutoSubscribe

> Object rsMsgsSetLobbyAutoSubscribe(opts)

setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSetLobbyAutoSubscribe': {"lobby_id":"(ChatLobbyId)lobby to auto (un)subscribe \n","autoSubscribe":"(boolean)set value for auto subscribe \n"} // ReqRsMsgsSetLobbyAutoSubscribe | lobby_id: >              (ChatLobbyId)lobby to auto (un)subscribe          autoSubscribe: >              (boolean)set value for auto subscribe  
};
apiInstance.rsMsgsSetLobbyAutoSubscribe(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSetLobbyAutoSubscribe** | [**ReqRsMsgsSetLobbyAutoSubscribe**](ReqRsMsgsSetLobbyAutoSubscribe.md)| lobby_id: &gt;              (ChatLobbyId)lobby to auto (un)subscribe          autoSubscribe: &gt;              (boolean)set value for auto subscribe   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSetMessageTag

> RespRsMsgsSetMessageTag rsMsgsSetMessageTag(opts)

setMessageTag set &#x3D;&#x3D; false &amp;&amp; tagId &#x3D;&#x3D; 0

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSetMessageTag': {"msgId":"(string)None\n","tagId":"(integer)None\n","set":"(boolean)None\n"} // ReqRsMsgsSetMessageTag | msgId: >              (string)None         tagId: >              (integer)None         set: >              (boolean)None 
};
apiInstance.rsMsgsSetMessageTag(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSetMessageTag** | [**ReqRsMsgsSetMessageTag**](ReqRsMsgsSetMessageTag.md)| msgId: &gt;              (string)None         tagId: &gt;              (integer)None         set: &gt;              (boolean)None  | [optional] 

### Return type

[**RespRsMsgsSetMessageTag**](RespRsMsgsSetMessageTag.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSetMessageTagType

> RespRsMsgsSetMessageTagType rsMsgsSetMessageTagType(opts)

setMessageTagType

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSetMessageTagType': {"tagId":"(integer)None\n","text":"(string)None\n","rgb_color":"(integer)None\n"} // ReqRsMsgsSetMessageTagType | tagId: >              (integer)None         text: >              (string)None         rgb_color: >              (integer)None 
};
apiInstance.rsMsgsSetMessageTagType(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSetMessageTagType** | [**ReqRsMsgsSetMessageTagType**](ReqRsMsgsSetMessageTagType.md)| tagId: &gt;              (integer)None         text: &gt;              (string)None         rgb_color: &gt;              (integer)None  | [optional] 

### Return type

[**RespRsMsgsSetMessageTagType**](RespRsMsgsSetMessageTagType.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsSystemMessage

> RespRsMsgsSystemMessage rsMsgsSystemMessage(opts)

SystemMessage

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsSystemMessage': {"title":"(string)None\n","message":"(string)None\n","systemFlag":"(integer)None\n"} // ReqRsMsgsSystemMessage | title: >              (string)None         message: >              (string)None         systemFlag: >              (integer)None 
};
apiInstance.rsMsgsSystemMessage(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsSystemMessage** | [**ReqRsMsgsSystemMessage**](ReqRsMsgsSystemMessage.md)| title: &gt;              (string)None         message: &gt;              (string)None         systemFlag: &gt;              (integer)None  | [optional] 

### Return type

[**RespRsMsgsSystemMessage**](RespRsMsgsSystemMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsMsgsUnsubscribeChatLobby

> Object rsMsgsUnsubscribeChatLobby(opts)

unsubscribeChatLobby leave a chat lobby

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsMsgsUnsubscribeChatLobby': {"lobby_id":"(ChatLobbyId)lobby to leave \n"} // ReqRsMsgsUnsubscribeChatLobby | lobby_id: >              (ChatLobbyId)lobby to leave  
};
apiInstance.rsMsgsUnsubscribeChatLobby(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsMsgsUnsubscribeChatLobby** | [**ReqRsMsgsUnsubscribeChatLobby**](ReqRsMsgsUnsubscribeChatLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby to leave   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersAcceptInvite

> RespRsPeersAcceptInvite rsPeersAcceptInvite(opts)

Add trusted node from invite

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersAcceptInvite': {"invite":"(string)invite string being it in cert or URL format \n","flags":"(ServicePermissionFlags)service permissions flag \n"} // ReqRsPeersAcceptInvite | invite: >              (string)invite string being it in cert or URL format          flags: >              (ServicePermissionFlags)service permissions flag  
};
apiInstance.rsPeersAcceptInvite(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersAcceptInvite** | [**ReqRsPeersAcceptInvite**](ReqRsPeersAcceptInvite.md)| invite: &gt;              (string)invite string being it in cert or URL format          flags: &gt;              (ServicePermissionFlags)service permissions flag   | [optional] 

### Return type

[**RespRsPeersAcceptInvite**](RespRsPeersAcceptInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersAddFriend

> RespRsPeersAddFriend rsPeersAddFriend(opts)

Add trusted node

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersAddFriend': {"sslId":"(RsPeerId)SSL id of the node to add \n","gpgId":"(RsPgpId)PGP id of the node to add \n","flags":"(ServicePermissionFlags)service permissions flag \n"} // ReqRsPeersAddFriend | sslId: >              (RsPeerId)SSL id of the node to add          gpgId: >              (RsPgpId)PGP id of the node to add          flags: >              (ServicePermissionFlags)service permissions flag  
};
apiInstance.rsPeersAddFriend(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersAddFriend** | [**ReqRsPeersAddFriend**](ReqRsPeersAddFriend.md)| sslId: &gt;              (RsPeerId)SSL id of the node to add          gpgId: &gt;              (RsPgpId)PGP id of the node to add          flags: &gt;              (ServicePermissionFlags)service permissions flag   | [optional] 

### Return type

[**RespRsPeersAddFriend**](RespRsPeersAddFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersAddGroup

> RespRsPeersAddGroup rsPeersAddGroup(opts)

addGroup create a new group

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersAddGroup': {"groupInfo":"(RsGroupInfo)None\n"} // ReqRsPeersAddGroup | groupInfo: >              (RsGroupInfo)None 
};
apiInstance.rsPeersAddGroup(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersAddGroup** | [**ReqRsPeersAddGroup**](ReqRsPeersAddGroup.md)| groupInfo: &gt;              (RsGroupInfo)None  | [optional] 

### Return type

[**RespRsPeersAddGroup**](RespRsPeersAddGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersAddPeerLocator

> RespRsPeersAddPeerLocator rsPeersAddPeerLocator(opts)

Add URL locator for given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersAddPeerLocator': {"sslId":"(RsPeerId)SSL id of the peer, own id is accepted too \n","locator":"(RsUrl)peer url locator \n"} // ReqRsPeersAddPeerLocator | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          locator: >              (RsUrl)peer url locator  
};
apiInstance.rsPeersAddPeerLocator(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersAddPeerLocator** | [**ReqRsPeersAddPeerLocator**](ReqRsPeersAddPeerLocator.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          locator: &gt;              (RsUrl)peer url locator   | [optional] 

### Return type

[**RespRsPeersAddPeerLocator**](RespRsPeersAddPeerLocator.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersAssignPeerToGroup

> RespRsPeersAssignPeerToGroup rsPeersAssignPeerToGroup(opts)

assignPeerToGroup add a peer to a group

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersAssignPeerToGroup': {"groupId":"(RsNodeGroupId)None\n","peerId":"(RsPgpId)None\n","assign":"(boolean)true to assign a peer, false to remove a peer \n"} // ReqRsPeersAssignPeerToGroup | groupId: >              (RsNodeGroupId)None         peerId: >              (RsPgpId)None         assign: >              (boolean)true to assign a peer, false to remove a peer  
};
apiInstance.rsPeersAssignPeerToGroup(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersAssignPeerToGroup** | [**ReqRsPeersAssignPeerToGroup**](ReqRsPeersAssignPeerToGroup.md)| groupId: &gt;              (RsNodeGroupId)None         peerId: &gt;              (RsPgpId)None         assign: &gt;              (boolean)true to assign a peer, false to remove a peer   | [optional] 

### Return type

[**RespRsPeersAssignPeerToGroup**](RespRsPeersAssignPeerToGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersAssignPeersToGroup

> RespRsPeersAssignPeersToGroup rsPeersAssignPeersToGroup(opts)

assignPeersToGroup add a list of peers to a group

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersAssignPeersToGroup': {"groupId":"(RsNodeGroupId)None\n","peerIds":"(list<RsPgpId>)None\n","assign":"(boolean)true to assign a peer, false to remove a peer \n"} // ReqRsPeersAssignPeersToGroup | groupId: >              (RsNodeGroupId)None         peerIds: >              (list<RsPgpId>)None         assign: >              (boolean)true to assign a peer, false to remove a peer  
};
apiInstance.rsPeersAssignPeersToGroup(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersAssignPeersToGroup** | [**ReqRsPeersAssignPeersToGroup**](ReqRsPeersAssignPeersToGroup.md)| groupId: &gt;              (RsNodeGroupId)None         peerIds: &gt;              (list&lt;RsPgpId&gt;)None         assign: &gt;              (boolean)true to assign a peer, false to remove a peer   | [optional] 

### Return type

[**RespRsPeersAssignPeersToGroup**](RespRsPeersAssignPeersToGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersConnectAttempt

> RespRsPeersConnectAttempt rsPeersConnectAttempt(opts)

Trigger connection attempt to given node

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersConnectAttempt': {"sslId":"(RsPeerId)SSL id of the node to connect \n"} // ReqRsPeersConnectAttempt | sslId: >              (RsPeerId)SSL id of the node to connect  
};
apiInstance.rsPeersConnectAttempt(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersConnectAttempt** | [**ReqRsPeersConnectAttempt**](ReqRsPeersConnectAttempt.md)| sslId: &gt;              (RsPeerId)SSL id of the node to connect   | [optional] 

### Return type

[**RespRsPeersConnectAttempt**](RespRsPeersConnectAttempt.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersEditGroup

> RespRsPeersEditGroup rsPeersEditGroup(opts)

editGroup edit an existing group

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersEditGroup': {"groupId":"(RsNodeGroupId)None\n","groupInfo":"(RsGroupInfo)None\n"} // ReqRsPeersEditGroup | groupId: >              (RsNodeGroupId)None         groupInfo: >              (RsGroupInfo)None 
};
apiInstance.rsPeersEditGroup(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersEditGroup** | [**ReqRsPeersEditGroup**](ReqRsPeersEditGroup.md)| groupId: &gt;              (RsNodeGroupId)None         groupInfo: &gt;              (RsGroupInfo)None  | [optional] 

### Return type

[**RespRsPeersEditGroup**](RespRsPeersEditGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersGetFriendList

> RespRsPeersGetFriendList rsPeersGetFriendList()

Get trusted peers list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsPeersGetFriendList((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsPeersGetFriendList**](RespRsPeersGetFriendList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsPeersGetGPGId

> RespRsPeersGetGPGId rsPeersGetGPGId(opts)

Get PGP id for the given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersGetGPGId': {"sslId":"(RsPeerId)SSL id of the peer \n"} // ReqRsPeersGetGPGId | sslId: >              (RsPeerId)SSL id of the peer  
};
apiInstance.rsPeersGetGPGId(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersGetGPGId** | [**ReqRsPeersGetGPGId**](ReqRsPeersGetGPGId.md)| sslId: &gt;              (RsPeerId)SSL id of the peer   | [optional] 

### Return type

[**RespRsPeersGetGPGId**](RespRsPeersGetGPGId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersGetGroupInfo

> RespRsPeersGetGroupInfo rsPeersGetGroupInfo(opts)

getGroupInfo get group information to one group

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersGetGroupInfo': {"groupId":"(RsNodeGroupId)None\n"} // ReqRsPeersGetGroupInfo | groupId: >              (RsNodeGroupId)None 
};
apiInstance.rsPeersGetGroupInfo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersGetGroupInfo** | [**ReqRsPeersGetGroupInfo**](ReqRsPeersGetGroupInfo.md)| groupId: &gt;              (RsNodeGroupId)None  | [optional] 

### Return type

[**RespRsPeersGetGroupInfo**](RespRsPeersGetGroupInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersGetGroupInfoByName

> RespRsPeersGetGroupInfoByName rsPeersGetGroupInfoByName(opts)

getGroupInfoByName get group information by group name

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersGetGroupInfoByName': {"groupName":"(string)None\n"} // ReqRsPeersGetGroupInfoByName | groupName: >              (string)None 
};
apiInstance.rsPeersGetGroupInfoByName(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersGetGroupInfoByName** | [**ReqRsPeersGetGroupInfoByName**](ReqRsPeersGetGroupInfoByName.md)| groupName: &gt;              (string)None  | [optional] 

### Return type

[**RespRsPeersGetGroupInfoByName**](RespRsPeersGetGroupInfoByName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersGetGroupInfoList

> RespRsPeersGetGroupInfoList rsPeersGetGroupInfoList()

getGroupInfoList get list of all groups

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsPeersGetGroupInfoList((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsPeersGetGroupInfoList**](RespRsPeersGetGroupInfoList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsPeersGetOnlineList

> RespRsPeersGetOnlineList rsPeersGetOnlineList()

Get connected peers list

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsPeersGetOnlineList((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsPeersGetOnlineList**](RespRsPeersGetOnlineList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsPeersGetPeerDetails

> RespRsPeersGetPeerDetails rsPeersGetPeerDetails(opts)

Get details details of the given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersGetPeerDetails': {"sslId":"(RsPeerId)id of the peer \n"} // ReqRsPeersGetPeerDetails | sslId: >              (RsPeerId)id of the peer  
};
apiInstance.rsPeersGetPeerDetails(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersGetPeerDetails** | [**ReqRsPeersGetPeerDetails**](ReqRsPeersGetPeerDetails.md)| sslId: &gt;              (RsPeerId)id of the peer   | [optional] 

### Return type

[**RespRsPeersGetPeerDetails**](RespRsPeersGetPeerDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersGetPeersCount

> RespRsPeersGetPeersCount rsPeersGetPeersCount(opts)

Get peers count

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersGetPeersCount': {"countLocations":"(boolean)true to count multiple locations of same owner \n"} // ReqRsPeersGetPeersCount | countLocations: >              (boolean)true to count multiple locations of same owner  
};
apiInstance.rsPeersGetPeersCount(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersGetPeersCount** | [**ReqRsPeersGetPeersCount**](ReqRsPeersGetPeersCount.md)| countLocations: &gt;              (boolean)true to count multiple locations of same owner   | [optional] 

### Return type

[**RespRsPeersGetPeersCount**](RespRsPeersGetPeersCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersGetRetroshareInvite

> RespRsPeersGetRetroshareInvite rsPeersGetRetroshareInvite(opts)

Get RetroShare invite of the given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersGetRetroshareInvite': {"sslId":"(RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned. \n","includeSignatures":"(boolean)true to add key signatures to the invite \n","includeExtraLocators":"(boolean)false to avoid to add extra locators \n"} // ReqRsPeersGetRetroshareInvite | sslId: >              (RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned.          includeSignatures: >              (boolean)true to add key signatures to the invite          includeExtraLocators: >              (boolean)false to avoid to add extra locators  
};
apiInstance.rsPeersGetRetroshareInvite(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersGetRetroshareInvite** | [**ReqRsPeersGetRetroshareInvite**](ReqRsPeersGetRetroshareInvite.md)| sslId: &gt;              (RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned.          includeSignatures: &gt;              (boolean)true to add key signatures to the invite          includeExtraLocators: &gt;              (boolean)false to avoid to add extra locators   | [optional] 

### Return type

[**RespRsPeersGetRetroshareInvite**](RespRsPeersGetRetroshareInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersIsFriend

> RespRsPeersIsFriend rsPeersIsFriend(opts)

Check if given peer is a trusted node

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersIsFriend': {"sslId":"(RsPeerId)id of the peer to check \n"} // ReqRsPeersIsFriend | sslId: >              (RsPeerId)id of the peer to check  
};
apiInstance.rsPeersIsFriend(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersIsFriend** | [**ReqRsPeersIsFriend**](ReqRsPeersIsFriend.md)| sslId: &gt;              (RsPeerId)id of the peer to check   | [optional] 

### Return type

[**RespRsPeersIsFriend**](RespRsPeersIsFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersIsOnline

> RespRsPeersIsOnline rsPeersIsOnline(opts)

Check if there is an established connection to the given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersIsOnline': {"sslId":"(RsPeerId)id of the peer to check \n"} // ReqRsPeersIsOnline | sslId: >              (RsPeerId)id of the peer to check  
};
apiInstance.rsPeersIsOnline(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersIsOnline** | [**ReqRsPeersIsOnline**](ReqRsPeersIsOnline.md)| sslId: &gt;              (RsPeerId)id of the peer to check   | [optional] 

### Return type

[**RespRsPeersIsOnline**](RespRsPeersIsOnline.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersIsPgpFriend

> RespRsPeersIsPgpFriend rsPeersIsPgpFriend(opts)

Check if given PGP id is trusted

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersIsPgpFriend': {"pgpId":"(RsPgpId)PGP id to check \n"} // ReqRsPeersIsPgpFriend | pgpId: >              (RsPgpId)PGP id to check  
};
apiInstance.rsPeersIsPgpFriend(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersIsPgpFriend** | [**ReqRsPeersIsPgpFriend**](ReqRsPeersIsPgpFriend.md)| pgpId: &gt;              (RsPgpId)PGP id to check   | [optional] 

### Return type

[**RespRsPeersIsPgpFriend**](RespRsPeersIsPgpFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersLoadCertificateFromString

> RespRsPeersLoadCertificateFromString rsPeersLoadCertificateFromString(opts)

Import certificate into the keyring

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersLoadCertificateFromString': {"cert":"(string)string representation of the certificate \n"} // ReqRsPeersLoadCertificateFromString | cert: >              (string)string representation of the certificate  
};
apiInstance.rsPeersLoadCertificateFromString(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersLoadCertificateFromString** | [**ReqRsPeersLoadCertificateFromString**](ReqRsPeersLoadCertificateFromString.md)| cert: &gt;              (string)string representation of the certificate   | [optional] 

### Return type

[**RespRsPeersLoadCertificateFromString**](RespRsPeersLoadCertificateFromString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersLoadDetailsFromStringCert

> RespRsPeersLoadDetailsFromStringCert rsPeersLoadDetailsFromStringCert(opts)

Examine certificate and get details without importing into the keyring

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersLoadDetailsFromStringCert': {"cert":"(string)string representation of the certificate \n"} // ReqRsPeersLoadDetailsFromStringCert | cert: >              (string)string representation of the certificate  
};
apiInstance.rsPeersLoadDetailsFromStringCert(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersLoadDetailsFromStringCert** | [**ReqRsPeersLoadDetailsFromStringCert**](ReqRsPeersLoadDetailsFromStringCert.md)| cert: &gt;              (string)string representation of the certificate   | [optional] 

### Return type

[**RespRsPeersLoadDetailsFromStringCert**](RespRsPeersLoadDetailsFromStringCert.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersRemoveFriend

> RespRsPeersRemoveFriend rsPeersRemoveFriend(opts)

Revoke connection trust from to node

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersRemoveFriend': {"pgpId":"(RsPgpId)PGP id of the node \n"} // ReqRsPeersRemoveFriend | pgpId: >              (RsPgpId)PGP id of the node  
};
apiInstance.rsPeersRemoveFriend(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersRemoveFriend** | [**ReqRsPeersRemoveFriend**](ReqRsPeersRemoveFriend.md)| pgpId: &gt;              (RsPgpId)PGP id of the node   | [optional] 

### Return type

[**RespRsPeersRemoveFriend**](RespRsPeersRemoveFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersRemoveFriendLocation

> RespRsPeersRemoveFriendLocation rsPeersRemoveFriendLocation(opts)

Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersRemoveFriendLocation': {"sslId":"(RsPeerId)SSL id of the location to remove \n"} // ReqRsPeersRemoveFriendLocation | sslId: >              (RsPeerId)SSL id of the location to remove  
};
apiInstance.rsPeersRemoveFriendLocation(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersRemoveFriendLocation** | [**ReqRsPeersRemoveFriendLocation**](ReqRsPeersRemoveFriendLocation.md)| sslId: &gt;              (RsPeerId)SSL id of the location to remove   | [optional] 

### Return type

[**RespRsPeersRemoveFriendLocation**](RespRsPeersRemoveFriendLocation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersRemoveGroup

> RespRsPeersRemoveGroup rsPeersRemoveGroup(opts)

removeGroup remove a group

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersRemoveGroup': {"groupId":"(RsNodeGroupId)None\n"} // ReqRsPeersRemoveGroup | groupId: >              (RsNodeGroupId)None 
};
apiInstance.rsPeersRemoveGroup(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersRemoveGroup** | [**ReqRsPeersRemoveGroup**](ReqRsPeersRemoveGroup.md)| groupId: &gt;              (RsNodeGroupId)None  | [optional] 

### Return type

[**RespRsPeersRemoveGroup**](RespRsPeersRemoveGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersSetDynDNS

> RespRsPeersSetDynDNS rsPeersSetDynDNS(opts)

Set (dynamical) domain name associated to the given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersSetDynDNS': {"sslId":"(RsPeerId)SSL id of the peer, own id is accepted too \n","addr":"(string)domain name string representation \n"} // ReqRsPeersSetDynDNS | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)domain name string representation  
};
apiInstance.rsPeersSetDynDNS(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersSetDynDNS** | [**ReqRsPeersSetDynDNS**](ReqRsPeersSetDynDNS.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          addr: &gt;              (string)domain name string representation   | [optional] 

### Return type

[**RespRsPeersSetDynDNS**](RespRsPeersSetDynDNS.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersSetExtAddress

> RespRsPeersSetExtAddress rsPeersSetExtAddress(opts)

Set external IPv4 address for given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersSetExtAddress': {"sslId":"(RsPeerId)SSL id of the peer, own id is accepted too \n","addr":"(string)string representation of the external IPv4 address \n","port":"(integer)external listening port \n"} // ReqRsPeersSetExtAddress | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)string representation of the external IPv4 address          port: >              (integer)external listening port  
};
apiInstance.rsPeersSetExtAddress(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersSetExtAddress** | [**ReqRsPeersSetExtAddress**](ReqRsPeersSetExtAddress.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          addr: &gt;              (string)string representation of the external IPv4 address          port: &gt;              (integer)external listening port   | [optional] 

### Return type

[**RespRsPeersSetExtAddress**](RespRsPeersSetExtAddress.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersSetLocalAddress

> RespRsPeersSetLocalAddress rsPeersSetLocalAddress(opts)

Set local IPv4 address for the given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersSetLocalAddress': {"sslId":"(RsPeerId)SSL id of the peer, own id is accepted too \n","addr":"(string)string representation of the local IPv4 address \n","port":"(integer)local listening port \n"} // ReqRsPeersSetLocalAddress | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)string representation of the local IPv4 address          port: >              (integer)local listening port  
};
apiInstance.rsPeersSetLocalAddress(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersSetLocalAddress** | [**ReqRsPeersSetLocalAddress**](ReqRsPeersSetLocalAddress.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          addr: &gt;              (string)string representation of the local IPv4 address          port: &gt;              (integer)local listening port   | [optional] 

### Return type

[**RespRsPeersSetLocalAddress**](RespRsPeersSetLocalAddress.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersSetNetworkMode

> RespRsPeersSetNetworkMode rsPeersSetNetworkMode(opts)

Set network mode of the given peer

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersSetNetworkMode': {"sslId":"(RsPeerId)SSL id of the peer, own id is accepted too \n","netMode":"(integer)one of RS_NETMODE_* \n"} // ReqRsPeersSetNetworkMode | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          netMode: >              (integer)one of RS_NETMODE_*  
};
apiInstance.rsPeersSetNetworkMode(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersSetNetworkMode** | [**ReqRsPeersSetNetworkMode**](ReqRsPeersSetNetworkMode.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          netMode: &gt;              (integer)one of RS_NETMODE_*   | [optional] 

### Return type

[**RespRsPeersSetNetworkMode**](RespRsPeersSetNetworkMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsPeersSetVisState

> RespRsPeersSetVisState rsPeersSetVisState(opts)

set DHT and discovery modes

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsPeersSetVisState': {"sslId":"(RsPeerId)SSL id of the peer, own id is accepted too \n","vsDisc":"(integer)one of RS_VS_DISC_* \n","vsDht":"(integer)one of RS_VS_DHT_* \n"} // ReqRsPeersSetVisState | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          vsDisc: >              (integer)one of RS_VS_DISC_*          vsDht: >              (integer)one of RS_VS_DHT_*  
};
apiInstance.rsPeersSetVisState(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsPeersSetVisState** | [**ReqRsPeersSetVisState**](ReqRsPeersSetVisState.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          vsDisc: &gt;              (integer)one of RS_VS_DISC_*          vsDht: &gt;              (integer)one of RS_VS_DHT_*   | [optional] 

### Return type

[**RespRsPeersSetVisState**](RespRsPeersSetVisState.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsAutoPositiveOpinionForContacts

> RespRsReputationsAutoPositiveOpinionForContacts rsReputationsAutoPositiveOpinionForContacts()

check if giving automatic positive opinion when flagging as contact is enbaled

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsReputationsAutoPositiveOpinionForContacts((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsReputationsAutoPositiveOpinionForContacts**](RespRsReputationsAutoPositiveOpinionForContacts.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsReputationsBanNode

> Object rsReputationsBanNode(opts)

Enable automatic banning of all identities signed by the given node

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsBanNode': {"id":"(RsPgpId)PGP id of the node \n","b":"(boolean)true to enable, false to disable \n"} // ReqRsReputationsBanNode | id: >              (RsPgpId)PGP id of the node          b: >              (boolean)true to enable, false to disable  
};
apiInstance.rsReputationsBanNode(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsBanNode** | [**ReqRsReputationsBanNode**](ReqRsReputationsBanNode.md)| id: &gt;              (RsPgpId)PGP id of the node          b: &gt;              (boolean)true to enable, false to disable   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsGetOwnOpinion

> RespRsReputationsGetOwnOpinion rsReputationsGetOwnOpinion(opts)

Get own opition about the given identity

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsGetOwnOpinion': {"id":"(RsGxsId)Id of the identity \n"} // ReqRsReputationsGetOwnOpinion | id: >              (RsGxsId)Id of the identity  
};
apiInstance.rsReputationsGetOwnOpinion(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsGetOwnOpinion** | [**ReqRsReputationsGetOwnOpinion**](ReqRsReputationsGetOwnOpinion.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**RespRsReputationsGetOwnOpinion**](RespRsReputationsGetOwnOpinion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsGetReputationInfo

> RespRsReputationsGetReputationInfo rsReputationsGetReputationInfo(opts)

Get reputation data of given identity

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsGetReputationInfo': {"id":"(RsGxsId)Id of the identity \n","ownerNode":"(RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id \n","stamp":"(boolean)if true, timestamo the information \n"} // ReqRsReputationsGetReputationInfo | id: >              (RsGxsId)Id of the identity          ownerNode: >              (RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id          stamp: >              (boolean)if true, timestamo the information  
};
apiInstance.rsReputationsGetReputationInfo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsGetReputationInfo** | [**ReqRsReputationsGetReputationInfo**](ReqRsReputationsGetReputationInfo.md)| id: &gt;              (RsGxsId)Id of the identity          ownerNode: &gt;              (RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id          stamp: &gt;              (boolean)if true, timestamo the information   | [optional] 

### Return type

[**RespRsReputationsGetReputationInfo**](RespRsReputationsGetReputationInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsIsIdentityBanned

> RespRsReputationsIsIdentityBanned rsReputationsIsIdentityBanned(opts)

This method allow fast checking if a GXS identity is banned.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsIsIdentityBanned': {"id":"(RsGxsId)Id of the identity to check \n"} // ReqRsReputationsIsIdentityBanned | id: >              (RsGxsId)Id of the identity to check  
};
apiInstance.rsReputationsIsIdentityBanned(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsIsIdentityBanned** | [**ReqRsReputationsIsIdentityBanned**](ReqRsReputationsIsIdentityBanned.md)| id: &gt;              (RsGxsId)Id of the identity to check   | [optional] 

### Return type

[**RespRsReputationsIsIdentityBanned**](RespRsReputationsIsIdentityBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsIsNodeBanned

> RespRsReputationsIsNodeBanned rsReputationsIsNodeBanned(opts)

Check if automatic banning of all identities signed by the given node is enabled

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsIsNodeBanned': {"id":"(RsPgpId)PGP id of the node \n"} // ReqRsReputationsIsNodeBanned | id: >              (RsPgpId)PGP id of the node  
};
apiInstance.rsReputationsIsNodeBanned(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsIsNodeBanned** | [**ReqRsReputationsIsNodeBanned**](ReqRsReputationsIsNodeBanned.md)| id: &gt;              (RsPgpId)PGP id of the node   | [optional] 

### Return type

[**RespRsReputationsIsNodeBanned**](RespRsReputationsIsNodeBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsOverallReputationLevel

> RespRsReputationsOverallReputationLevel rsReputationsOverallReputationLevel(opts)

Get overall reputation level of given identity

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsOverallReputationLevel': {"id":"(RsGxsId)Id of the identity \n"} // ReqRsReputationsOverallReputationLevel | id: >              (RsGxsId)Id of the identity  
};
apiInstance.rsReputationsOverallReputationLevel(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsOverallReputationLevel** | [**ReqRsReputationsOverallReputationLevel**](ReqRsReputationsOverallReputationLevel.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**RespRsReputationsOverallReputationLevel**](RespRsReputationsOverallReputationLevel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsRememberBannedIdThreshold

> RespRsReputationsRememberBannedIdThreshold rsReputationsRememberBannedIdThreshold()

Get number of days to wait before deleting a banned identity from local storage

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsReputationsRememberBannedIdThreshold((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsReputationsRememberBannedIdThreshold**](RespRsReputationsRememberBannedIdThreshold.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsReputationsSetAutoPositiveOpinionForContacts

> Object rsReputationsSetAutoPositiveOpinionForContacts(opts)

Enable giving automatic positive opinion when flagging as contact

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsSetAutoPositiveOpinionForContacts': {"b":"(boolean)true to enable, false to disable \n"} // ReqRsReputationsSetAutoPositiveOpinionForContacts | b: >              (boolean)true to enable, false to disable  
};
apiInstance.rsReputationsSetAutoPositiveOpinionForContacts(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsSetAutoPositiveOpinionForContacts** | [**ReqRsReputationsSetAutoPositiveOpinionForContacts**](ReqRsReputationsSetAutoPositiveOpinionForContacts.md)| b: &gt;              (boolean)true to enable, false to disable   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsSetOwnOpinion

> RespRsReputationsSetOwnOpinion rsReputationsSetOwnOpinion(opts)

Set own opinion about the given identity

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsSetOwnOpinion': {"id":"(RsGxsId)Id of the identity \n","op":"(RsOpinion)Own opinion \n"} // ReqRsReputationsSetOwnOpinion | id: >              (RsGxsId)Id of the identity          op: >              (RsOpinion)Own opinion  
};
apiInstance.rsReputationsSetOwnOpinion(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsSetOwnOpinion** | [**ReqRsReputationsSetOwnOpinion**](ReqRsReputationsSetOwnOpinion.md)| id: &gt;              (RsGxsId)Id of the identity          op: &gt;              (RsOpinion)Own opinion   | [optional] 

### Return type

[**RespRsReputationsSetOwnOpinion**](RespRsReputationsSetOwnOpinion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsSetRememberBannedIdThreshold

> Object rsReputationsSetRememberBannedIdThreshold(opts)

Set number of days to wait before deleting a banned identity from local storage

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsSetRememberBannedIdThreshold': {"days":"(integer)number of days to wait, 0 means never delete \n"} // ReqRsReputationsSetRememberBannedIdThreshold | days: >              (integer)number of days to wait, 0 means never delete  
};
apiInstance.rsReputationsSetRememberBannedIdThreshold(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsSetRememberBannedIdThreshold** | [**ReqRsReputationsSetRememberBannedIdThreshold**](ReqRsReputationsSetRememberBannedIdThreshold.md)| days: &gt;              (integer)number of days to wait, 0 means never delete   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsSetThresholdForRemotelyNegativeReputation

> Object rsReputationsSetThresholdForRemotelyNegativeReputation(opts)

Set threshold on remote reputation to consider it remotely negative

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsSetThresholdForRemotelyNegativeReputation': {"thresh":"(integer)Threshold value \n"} // ReqRsReputationsSetThresholdForRemotelyNegativeReputation | thresh: >              (integer)Threshold value  
};
apiInstance.rsReputationsSetThresholdForRemotelyNegativeReputation(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsSetThresholdForRemotelyNegativeReputation** | [**ReqRsReputationsSetThresholdForRemotelyNegativeReputation**](ReqRsReputationsSetThresholdForRemotelyNegativeReputation.md)| thresh: &gt;              (integer)Threshold value   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsSetThresholdForRemotelyPositiveReputation

> Object rsReputationsSetThresholdForRemotelyPositiveReputation(opts)

Set threshold on remote reputation to consider it remotely positive

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsReputationsSetThresholdForRemotelyPositiveReputation': {"thresh":"(integer)Threshold value \n"} // ReqRsReputationsSetThresholdForRemotelyPositiveReputation | thresh: >              (integer)Threshold value  
};
apiInstance.rsReputationsSetThresholdForRemotelyPositiveReputation(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsReputationsSetThresholdForRemotelyPositiveReputation** | [**ReqRsReputationsSetThresholdForRemotelyPositiveReputation**](ReqRsReputationsSetThresholdForRemotelyPositiveReputation.md)| thresh: &gt;              (integer)Threshold value   | [optional] 

### Return type

**Object**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsReputationsThresholdForRemotelyNegativeReputation

> RespRsReputationsThresholdForRemotelyNegativeReputation rsReputationsThresholdForRemotelyNegativeReputation()

Get threshold on remote reputation to consider it remotely negative

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsReputationsThresholdForRemotelyNegativeReputation((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsReputationsThresholdForRemotelyNegativeReputation**](RespRsReputationsThresholdForRemotelyNegativeReputation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsReputationsThresholdForRemotelyPositiveReputation

> RespRsReputationsThresholdForRemotelyPositiveReputation rsReputationsThresholdForRemotelyPositiveReputation()

Get threshold on remote reputation to consider it remotely negative

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsReputationsThresholdForRemotelyPositiveReputation((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsReputationsThresholdForRemotelyPositiveReputation**](RespRsReputationsThresholdForRemotelyPositiveReputation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsServiceControlGetOwnServices

> RespRsServiceControlGetOwnServices rsServiceControlGetOwnServices()

get a map off all services.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
apiInstance.rsServiceControlGetOwnServices((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**RespRsServiceControlGetOwnServices**](RespRsServiceControlGetOwnServices.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## rsServiceControlGetPeersConnected

> RespRsServiceControlGetPeersConnected rsServiceControlGetPeersConnected(opts)

getPeersConnected return peers using a service.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsServiceControlGetPeersConnected': {"serviceId":"(integer)service to look up. \n"} // ReqRsServiceControlGetPeersConnected | serviceId: >              (integer)service to look up.  
};
apiInstance.rsServiceControlGetPeersConnected(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsServiceControlGetPeersConnected** | [**ReqRsServiceControlGetPeersConnected**](ReqRsServiceControlGetPeersConnected.md)| serviceId: &gt;              (integer)service to look up.   | [optional] 

### Return type

[**RespRsServiceControlGetPeersConnected**](RespRsServiceControlGetPeersConnected.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsServiceControlGetServiceItemNames

> RespRsServiceControlGetServiceItemNames rsServiceControlGetServiceItemNames(opts)

getServiceItemNames return a map of service item names.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsServiceControlGetServiceItemNames': {"serviceId":"(integer)service to look up \n"} // ReqRsServiceControlGetServiceItemNames | serviceId: >              (integer)service to look up  
};
apiInstance.rsServiceControlGetServiceItemNames(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsServiceControlGetServiceItemNames** | [**ReqRsServiceControlGetServiceItemNames**](ReqRsServiceControlGetServiceItemNames.md)| serviceId: &gt;              (integer)service to look up   | [optional] 

### Return type

[**RespRsServiceControlGetServiceItemNames**](RespRsServiceControlGetServiceItemNames.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsServiceControlGetServiceName

> RespRsServiceControlGetServiceName rsServiceControlGetServiceName(opts)

getServiceName lookup the name of a service.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsServiceControlGetServiceName': {"serviceId":"(integer)service to look up \n"} // ReqRsServiceControlGetServiceName | serviceId: >              (integer)service to look up  
};
apiInstance.rsServiceControlGetServiceName(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsServiceControlGetServiceName** | [**ReqRsServiceControlGetServiceName**](ReqRsServiceControlGetServiceName.md)| serviceId: &gt;              (integer)service to look up   | [optional] 

### Return type

[**RespRsServiceControlGetServiceName**](RespRsServiceControlGetServiceName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsServiceControlGetServicePermissions

> RespRsServiceControlGetServicePermissions rsServiceControlGetServicePermissions(opts)

getServicePermissions return permissions of one service.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsServiceControlGetServicePermissions': {"serviceId":"(integer)service id to look up \n"} // ReqRsServiceControlGetServicePermissions | serviceId: >              (integer)service id to look up  
};
apiInstance.rsServiceControlGetServicePermissions(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsServiceControlGetServicePermissions** | [**ReqRsServiceControlGetServicePermissions**](ReqRsServiceControlGetServicePermissions.md)| serviceId: &gt;              (integer)service id to look up   | [optional] 

### Return type

[**RespRsServiceControlGetServicePermissions**](RespRsServiceControlGetServicePermissions.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsServiceControlGetServicesAllowed

> RespRsServiceControlGetServicesAllowed rsServiceControlGetServicesAllowed(opts)

getServicesAllowed return a mpa with allowed service information.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsServiceControlGetServicesAllowed': {"peerId":"(RsPeerId)peer to look up \n"} // ReqRsServiceControlGetServicesAllowed | peerId: >              (RsPeerId)peer to look up  
};
apiInstance.rsServiceControlGetServicesAllowed(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsServiceControlGetServicesAllowed** | [**ReqRsServiceControlGetServicesAllowed**](ReqRsServiceControlGetServicesAllowed.md)| peerId: &gt;              (RsPeerId)peer to look up   | [optional] 

### Return type

[**RespRsServiceControlGetServicesAllowed**](RespRsServiceControlGetServicesAllowed.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsServiceControlGetServicesProvided

> RespRsServiceControlGetServicesProvided rsServiceControlGetServicesProvided(opts)

getServicesProvided return services provided by a peer.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsServiceControlGetServicesProvided': {"peerId":"(RsPeerId)peer to look up \n"} // ReqRsServiceControlGetServicesProvided | peerId: >              (RsPeerId)peer to look up  
};
apiInstance.rsServiceControlGetServicesProvided(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsServiceControlGetServicesProvided** | [**ReqRsServiceControlGetServicesProvided**](ReqRsServiceControlGetServicesProvided.md)| peerId: &gt;              (RsPeerId)peer to look up   | [optional] 

### Return type

[**RespRsServiceControlGetServicesProvided**](RespRsServiceControlGetServicesProvided.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rsServiceControlUpdateServicePermissions

> RespRsServiceControlUpdateServicePermissions rsServiceControlUpdateServicePermissions(opts)

updateServicePermissions update service permissions of one service.

### Example

```javascript
import RetroShareOpenApiWrapper from 'retro_share_open_api_wrapper';
let defaultClient = RetroShareOpenApiWrapper.ApiClient.instance;
// Configure HTTP basic authorization: BasicAuth
let BasicAuth = defaultClient.authentications['BasicAuth'];
BasicAuth.username = 'YOUR USERNAME';
BasicAuth.password = 'YOUR PASSWORD';

let apiInstance = new RetroShareOpenApiWrapper.DefaultApi();
let opts = {
  'reqRsServiceControlUpdateServicePermissions': {"serviceId":"(integer)service to update \n","permissions":"(RsServicePermissions)new permissions \n"} // ReqRsServiceControlUpdateServicePermissions | serviceId: >              (integer)service to update          permissions: >              (RsServicePermissions)new permissions  
};
apiInstance.rsServiceControlUpdateServicePermissions(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRsServiceControlUpdateServicePermissions** | [**ReqRsServiceControlUpdateServicePermissions**](ReqRsServiceControlUpdateServicePermissions.md)| serviceId: &gt;              (integer)service to update          permissions: &gt;              (RsServicePermissions)new permissions   | [optional] 

### Return type

[**RespRsServiceControlUpdateServicePermissions**](RespRsServiceControlUpdateServicePermissions.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

