# RetroShareOpenApiWrapper.RespRsPeersGetGroupInfoByName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**groupInfo** | [**RsGroupInfo**](RsGroupInfo.md) |  | [optional] 


