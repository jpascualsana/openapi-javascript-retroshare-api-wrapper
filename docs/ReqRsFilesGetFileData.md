# RetroShareOpenApiWrapper.ReqRsFilesGetFileData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**offset** | **Number** |  | [optional] 
**requestedSize** | **Number** |  | [optional] 


