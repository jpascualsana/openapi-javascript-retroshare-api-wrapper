# RetroShareOpenApiWrapper.RespRsMsgsRemoveMessageTagType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


