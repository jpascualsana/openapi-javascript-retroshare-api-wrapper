# RetroShareOpenApiWrapper.ReqRsGxsForumsCreateForumV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**authorId** | **String** |  | [optional] 
**moderatorsIds** | **[String]** |  | [optional] 
**circleType** | [**RsGxsCircleType**](RsGxsCircleType.md) |  | [optional] 
**circleId** | **String** |  | [optional] 


