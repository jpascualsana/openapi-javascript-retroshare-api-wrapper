# RetroShareOpenApiWrapper.RespRsGxsChannelsCreatePostV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**postId** | **String** |  | [optional] 
**errorMessage** | **String** |  | [optional] 


