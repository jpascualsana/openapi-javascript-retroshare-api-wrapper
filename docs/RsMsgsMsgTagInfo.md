# RetroShareOpenApiWrapper.RsMsgsMsgTagInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**tagIds** | **[Number]** |  | [optional] 


