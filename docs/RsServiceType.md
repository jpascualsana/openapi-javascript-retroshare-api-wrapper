# RetroShareOpenApiWrapper.RsServiceType

## Enum


* `NONE` (value: `0`)

* `GOSSIP_DISCOVERY` (value: `17`)

* `CHAT` (value: `18`)

* `MSG` (value: `19`)

* `TURTLE` (value: `20`)

* `TUNNEL` (value: `21`)

* `HEARTBEAT` (value: `22`)

* `FILE_TRANSFER` (value: `23`)

* `GROUTER` (value: `24`)

* `FILE_DATABASE` (value: `25`)

* `SERVICEINFO` (value: `32`)

* `BANDWIDTH_CONTROL` (value: `33`)

* `MAIL` (value: `34`)

* `DIRECT_MAIL` (value: `35`)

* `DISTANT_MAIL` (value: `36`)

* `GWEMAIL_MAIL` (value: `37`)

* `SERVICE_CONTROL` (value: `38`)

* `DISTANT_CHAT` (value: `39`)

* `GXS_TUNNEL` (value: `40`)

* `BANLIST` (value: `257`)

* `STATUS` (value: `258`)

* `NXS` (value: `512`)

* `GXSID` (value: `529`)

* `PHOTO` (value: `530`)

* `WIKI` (value: `531`)

* `WIRE` (value: `532`)

* `FORUMS` (value: `533`)

* `POSTED` (value: `534`)

* `CHANNELS` (value: `535`)

* `GXSCIRCLE` (value: `536`)

* `REPUTATION` (value: `537`)

* `GXS_RECOGN` (value: `544`)

* `GXS_TRANS` (value: `560`)

* `JSONAPI` (value: `576`)

* `FORUMS_CONFIG` (value: `789`)

* `CHANNELS_CONFIG` (value: `791`)

* `RTT` (value: `4113`)

* `PLUGIN_ARADO_ID` (value: `8193`)

* `PLUGIN_QCHESS_ID` (value: `8194`)

* `PLUGIN_FEEDREADER` (value: `8195`)

* `PACKET_SLICING_PROBE` (value: `43707`)

* `PLUGIN_FIDO_GW` (value: `61904`)

* `PLUGIN_ZERORESERVE` (value: `48879`)


