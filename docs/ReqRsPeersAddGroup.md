# RetroShareOpenApiWrapper.ReqRsPeersAddGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupInfo** | [**RsGroupInfo**](RsGroupInfo.md) |  | [optional] 


