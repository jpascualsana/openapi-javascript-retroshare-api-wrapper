# RetroShareOpenApiWrapper.RespRsPeersRemoveGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


