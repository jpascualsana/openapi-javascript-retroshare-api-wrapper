# RetroShareOpenApiWrapper.ReqRsReputationsGetReputationInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**ownerNode** | **String** |  | [optional] 
**stamp** | **Boolean** |  | [optional] 


