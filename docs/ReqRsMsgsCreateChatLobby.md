# RetroShareOpenApiWrapper.ReqRsMsgsCreateChatLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyName** | **String** |  | [optional] 
**lobbyIdentity** | **String** |  | [optional] 
**lobbyTopic** | **String** |  | [optional] 
**invitedFriends** | **[String]** |  | [optional] 
**lobbyPrivacyType** | **Number** |  | [optional] 


