# RetroShareOpenApiWrapper.ReqRsPeersEditGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] 
**groupInfo** | [**RsGroupInfo**](RsGroupInfo.md) |  | [optional] 


