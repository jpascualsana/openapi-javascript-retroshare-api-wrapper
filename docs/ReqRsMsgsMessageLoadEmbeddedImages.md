# RetroShareOpenApiWrapper.ReqRsMsgsMessageLoadEmbeddedImages

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**load** | **Boolean** |  | [optional] 


