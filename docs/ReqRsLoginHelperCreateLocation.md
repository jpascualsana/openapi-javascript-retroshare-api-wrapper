# RetroShareOpenApiWrapper.ReqRsLoginHelperCreateLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | [**RsLoginHelperLocation**](RsLoginHelperLocation.md) |  | [optional] 
**password** | **String** |  | [optional] 
**makeHidden** | **Boolean** |  | [optional] 
**makeAutoTor** | **Boolean** |  | [optional] 


