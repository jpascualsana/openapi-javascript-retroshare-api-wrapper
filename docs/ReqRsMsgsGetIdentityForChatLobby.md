# RetroShareOpenApiWrapper.ReqRsMsgsGetIdentityForChatLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 


