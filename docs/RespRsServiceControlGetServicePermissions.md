# RetroShareOpenApiWrapper.RespRsServiceControlGetServicePermissions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**permissions** | [**RsServicePermissions**](RsServicePermissions.md) |  | [optional] 


