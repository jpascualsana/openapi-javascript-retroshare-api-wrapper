# RetroShareOpenApiWrapper.RespRsGxsCirclesGetCircleRequests

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**requests** | [**[RsGxsCircleMsg]**](RsGxsCircleMsg.md) |  | [optional] 


