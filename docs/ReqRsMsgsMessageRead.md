# RetroShareOpenApiWrapper.ReqRsMsgsMessageRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**unreadByUser** | **Boolean** |  | [optional] 


