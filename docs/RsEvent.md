# RetroShareOpenApiWrapper.RsEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mType** | [**RsEventType**](RsEventType.md) |  | [optional] 
**mTimePoint** | **Number** |  | [optional] 


