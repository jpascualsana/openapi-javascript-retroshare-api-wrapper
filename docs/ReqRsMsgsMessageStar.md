# RetroShareOpenApiWrapper.ReqRsMsgsMessageStar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**mark** | **Boolean** |  | [optional] 


