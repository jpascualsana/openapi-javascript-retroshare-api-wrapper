# RetroShareOpenApiWrapper.RespRsGxsCirclesCreateCircle

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**cData** | [**RsGxsCircleGroup**](RsGxsCircleGroup.md) |  | [optional] 


