# RetroShareOpenApiWrapper.RsGxsCircleMsg

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] 
**stuff** | **String** |  | [optional] 


