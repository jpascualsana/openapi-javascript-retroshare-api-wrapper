# RetroShareOpenApiWrapper.RsEventType

## Enum


* `NONE` (value: `0`)

* `BROADCAST_DISCOVERY_PEER_FOUND` (value: `1`)

* `GOSSIP_DISCOVERY_INVITE_RECEIVED` (value: `2`)

* `AUTHSSL_CONNECTION_AUTENTICATION` (value: `3`)

* `REMOTE_PEER_REFUSED_CONNECTION` (value: `4`)

* `GXS_CHANGES` (value: `5`)

* `MAX` (value: `0`)


