# RetroShareOpenApiWrapper.RsServicePermissions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mServiceId** | **Number** |  | [optional] 
**mServiceName** | **String** |  | [optional] 
**mDefaultAllowed** | **Boolean** |  | [optional] 
**mPeersAllowed** | **[String]** |  | [optional] 
**mPeersDenied** | **[String]** |  | [optional] 


