# RetroShareOpenApiWrapper.RespRsFilesGetFileData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**requestedSize** | **Number** |  | [optional] 
**data** | **Number** |  | [optional] 


