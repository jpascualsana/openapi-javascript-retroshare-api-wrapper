# RetroShareOpenApiWrapper.RespRsIdentityIsOwnId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


