# RetroShareOpenApiWrapper.RespRsFilesGetSharedDirectories

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**dirs** | [**[SharedDirInfo]**](SharedDirInfo.md) |  | [optional] 


