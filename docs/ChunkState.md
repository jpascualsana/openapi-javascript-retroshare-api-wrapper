# RetroShareOpenApiWrapper.ChunkState

## Enum


* `CHUNK_OUTSTANDING` (value: `0`)

* `CHUNK_ACTIVE` (value: `1`)

* `CHUNK_DONE` (value: `2`)

* `CHUNK_CHECKING` (value: `3`)


