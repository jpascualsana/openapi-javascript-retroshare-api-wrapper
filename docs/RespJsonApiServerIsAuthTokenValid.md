# RetroShareOpenApiWrapper.RespJsonApiServerIsAuthTokenValid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


