# RetroShareOpenApiWrapper.ReqRsMsgsJoinVisibleChatLobby

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 
**ownId** | **String** |  | [optional] 


