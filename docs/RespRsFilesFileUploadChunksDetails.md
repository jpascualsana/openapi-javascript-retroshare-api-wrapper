# RetroShareOpenApiWrapper.RespRsFilesFileUploadChunksDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**map** | [**CompressedChunkMap**](CompressedChunkMap.md) |  | [optional] 


