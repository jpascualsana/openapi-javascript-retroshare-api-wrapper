# RetroShareOpenApiWrapper.RespRsMsgsGetMessageTag

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**info** | [**RsMsgsMsgTagInfo**](RsMsgsMsgTagInfo.md) |  | [optional] 


