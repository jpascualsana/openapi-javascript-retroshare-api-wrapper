# RetroShareOpenApiWrapper.RsRecognTag

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tagClass** | **Number** |  | [optional] 
**tagType** | **Number** |  | [optional] 
**valid** | **Boolean** |  | [optional] 


