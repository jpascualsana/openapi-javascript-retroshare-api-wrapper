# RetroShareOpenApiWrapper.ReqRsGxsChannelsSubscribeToChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**subscribe** | **Boolean** |  | [optional] 


