# RetroShareOpenApiWrapper.TurtleFileInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | **Number** |  | [optional] 
**hash** | **String** |  | [optional] 
**name** | **String** |  | [optional] 


