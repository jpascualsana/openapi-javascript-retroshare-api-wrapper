# RetroShareOpenApiWrapper.RespRsGxsCirclesGetCirclesSummaries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**circles** | [**[RsGroupMetaData]**](RsGroupMetaData.md) |  | [optional] 


