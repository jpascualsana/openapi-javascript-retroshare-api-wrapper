# RetroShareOpenApiWrapper.FileChunksInfoPendingSlices

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **Number** |  | [optional] 
**value** | [**[FileChunksInfoSliceInfo]**](FileChunksInfoSliceInfo.md) |  | [optional] 


