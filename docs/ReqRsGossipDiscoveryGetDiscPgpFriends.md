# RetroShareOpenApiWrapper.ReqRsGossipDiscoveryGetDiscPgpFriends

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pgpid** | **String** |  | [optional] 


