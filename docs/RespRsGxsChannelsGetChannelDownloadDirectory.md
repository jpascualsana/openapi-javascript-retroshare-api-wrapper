# RetroShareOpenApiWrapper.RespRsGxsChannelsGetChannelDownloadDirectory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**directory** | **String** |  | [optional] 


