# RetroShareOpenApiWrapper.RsPeerServiceInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mPeerId** | **String** |  | [optional] 
**mServiceList** | [**[RsPeerServiceInfoMServiceList]**](RsPeerServiceInfoMServiceList.md) |  | [optional] 


