# RetroShareOpenApiWrapper.ReqRsGxsChannelsCreatePost

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**post** | [**RsGxsChannelPost**](RsGxsChannelPost.md) |  | [optional] 


