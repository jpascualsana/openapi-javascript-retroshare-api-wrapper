# RetroShareOpenApiWrapper.ReqRsFilesFileControl

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**flags** | **Number** |  | [optional] 


