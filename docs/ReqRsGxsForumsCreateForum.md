# RetroShareOpenApiWrapper.ReqRsGxsForumsCreateForum

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forum** | [**RsGxsForumGroup**](RsGxsForumGroup.md) |  | [optional] 


