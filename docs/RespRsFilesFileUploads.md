# RetroShareOpenApiWrapper.RespRsFilesFileUploads

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**hashs** | **[String]** |  | [optional] 


