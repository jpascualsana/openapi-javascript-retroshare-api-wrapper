# RetroShareOpenApiWrapper.RsTokenServiceGxsRequestStatus

## Enum


* `FAILED` (value: `0`)

* `PENDING` (value: `1`)

* `PARTIAL` (value: `2`)

* `COMPLETE` (value: `3`)

* `DONE` (value: `4`)

* `CANCELLED` (value: `5`)


