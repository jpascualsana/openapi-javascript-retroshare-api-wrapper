# RetroShareOpenApiWrapper.ReqRsMsgsAcceptLobbyInvite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**identity** | **String** |  | [optional] 


