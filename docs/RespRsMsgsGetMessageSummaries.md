# RetroShareOpenApiWrapper.RespRsMsgsGetMessageSummaries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**msgList** | [**[RsMsgsMsgInfoSummary]**](RsMsgsMsgInfoSummary.md) |  | [optional] 


