# RetroShareOpenApiWrapper.ReqRsAccountsExportIdentity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filePath** | **String** |  | [optional] 
**pgpId** | **String** |  | [optional] 


