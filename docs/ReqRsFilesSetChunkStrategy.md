# RetroShareOpenApiWrapper.ReqRsFilesSetChunkStrategy

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**newStrategy** | [**FileChunksInfoChunkStrategy**](FileChunksInfoChunkStrategy.md) |  | [optional] 


