# RetroShareOpenApiWrapper.RespRsFilesGetPrimaryBannedFilesList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**bannedFiles** | [**[RespRsFilesGetPrimaryBannedFilesListBannedFiles]**](RespRsFilesGetPrimaryBannedFilesListBannedFiles.md) |  | [optional] 


