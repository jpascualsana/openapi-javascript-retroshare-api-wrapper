# RetroShareOpenApiWrapper.RsGxsGroupSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mGroupId** | **String** |  | [optional] 
**mGroupName** | **String** |  | [optional] 
**mAuthorId** | **String** |  | [optional] 
**mPublishTs** | **Number** |  | [optional] 
**mNumberOfMessages** | **Number** |  | [optional] 
**mLastMessageTs** | **Number** |  | [optional] 
**mSignFlags** | **Number** |  | [optional] 
**mPopularity** | **Number** |  | [optional] 
**mSearchContext** | **String** |  | [optional] 


