# RetroShareOpenApiWrapper.ReqRsPeersAcceptInvite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invite** | **String** |  | [optional] 
**flags** | **Number** |  | [optional] 


