# RetroShareOpenApiWrapper.ReqRsFilesRemoveSharedDirectory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dir** | **String** |  | [optional] 


