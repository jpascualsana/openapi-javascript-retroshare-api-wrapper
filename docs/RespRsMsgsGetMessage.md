# RetroShareOpenApiWrapper.RespRsMsgsGetMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**msg** | [**RsMsgsMessageInfo**](RsMsgsMessageInfo.md) |  | [optional] 


