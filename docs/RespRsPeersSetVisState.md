# RetroShareOpenApiWrapper.RespRsPeersSetVisState

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


