# RetroShareOpenApiWrapper.ReqRsPeersSetVisState

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] 
**vsDisc** | **Number** |  | [optional] 
**vsDht** | **Number** |  | [optional] 


