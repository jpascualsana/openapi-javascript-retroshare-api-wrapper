# RetroShareOpenApiWrapper.RespRsLoginHelperAttemptLogin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | [**RsInitLoadCertificateStatus**](RsInitLoadCertificateStatus.md) |  | [optional] 


