# RetroShareOpenApiWrapper.RespRsGxsChannelsCreatePost

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**post** | [**RsGxsChannelPost**](RsGxsChannelPost.md) |  | [optional] 


