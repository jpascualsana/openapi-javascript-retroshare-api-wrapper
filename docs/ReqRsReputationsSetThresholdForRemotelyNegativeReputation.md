# RetroShareOpenApiWrapper.ReqRsReputationsSetThresholdForRemotelyNegativeReputation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**thresh** | **Number** |  | [optional] 


