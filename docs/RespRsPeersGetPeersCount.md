# RetroShareOpenApiWrapper.RespRsPeersGetPeersCount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**peersCount** | **Number** |  | [optional] 
**onlinePeersCount** | **Number** |  | [optional] 


