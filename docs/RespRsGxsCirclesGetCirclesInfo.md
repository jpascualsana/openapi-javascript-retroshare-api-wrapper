# RetroShareOpenApiWrapper.RespRsGxsCirclesGetCirclesInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**circlesInfo** | [**[RsGxsCircleGroup]**](RsGxsCircleGroup.md) |  | [optional] 


