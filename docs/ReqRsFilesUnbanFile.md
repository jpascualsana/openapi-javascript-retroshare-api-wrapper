# RetroShareOpenApiWrapper.ReqRsFilesUnbanFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**realFileHash** | **String** |  | [optional] 


