# RetroShareOpenApiWrapper.ReqRsMsgsSendStatusString

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**ChatId**](ChatId.md) |  | [optional] 
**statusString** | **String** |  | [optional] 


