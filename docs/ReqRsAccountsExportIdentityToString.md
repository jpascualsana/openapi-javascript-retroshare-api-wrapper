# RetroShareOpenApiWrapper.ReqRsAccountsExportIdentityToString

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pgpId** | **String** |  | [optional] 
**includeSignatures** | **Boolean** |  | [optional] 


