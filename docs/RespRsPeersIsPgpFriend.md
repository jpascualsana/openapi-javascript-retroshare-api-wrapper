# RetroShareOpenApiWrapper.RespRsPeersIsPgpFriend

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


