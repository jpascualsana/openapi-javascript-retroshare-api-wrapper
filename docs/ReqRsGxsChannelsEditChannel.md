# RetroShareOpenApiWrapper.ReqRsGxsChannelsEditChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel** | [**RsGxsChannelGroup**](RsGxsChannelGroup.md) |  | [optional] 


