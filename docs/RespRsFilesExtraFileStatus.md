# RetroShareOpenApiWrapper.RespRsFilesExtraFileStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**info** | [**FileInfo**](FileInfo.md) |  | [optional] 


