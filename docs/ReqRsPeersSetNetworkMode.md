# RetroShareOpenApiWrapper.ReqRsPeersSetNetworkMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] 
**netMode** | **Number** |  | [optional] 


