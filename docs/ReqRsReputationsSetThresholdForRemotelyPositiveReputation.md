# RetroShareOpenApiWrapper.ReqRsReputationsSetThresholdForRemotelyPositiveReputation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**thresh** | **Number** |  | [optional] 


