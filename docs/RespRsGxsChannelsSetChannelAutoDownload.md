# RetroShareOpenApiWrapper.RespRsGxsChannelsSetChannelAutoDownload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


