# RetroShareOpenApiWrapper.ReqRsPeersGetGPGId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] 


