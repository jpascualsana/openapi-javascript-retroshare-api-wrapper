# RetroShareOpenApiWrapper.FileChunksInfoActiveChunks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first** | **Number** |  | [optional] 
**second** | **Number** |  | [optional] 


