# RetroShareOpenApiWrapper.ReqRsGxsChannelsMarkRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**postId** | [**RsGxsGrpMsgIdPair**](RsGxsGrpMsgIdPair.md) |  | [optional] 
**read** | **Boolean** |  | [optional] 


