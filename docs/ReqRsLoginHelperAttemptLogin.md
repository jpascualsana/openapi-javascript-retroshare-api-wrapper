# RetroShareOpenApiWrapper.ReqRsLoginHelperAttemptLogin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **String** |  | [optional] 
**password** | **String** |  | [optional] 


