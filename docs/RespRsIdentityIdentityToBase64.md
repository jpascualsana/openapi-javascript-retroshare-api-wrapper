# RetroShareOpenApiWrapper.RespRsIdentityIdentityToBase64

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**base64String** | **String** |  | [optional] 


