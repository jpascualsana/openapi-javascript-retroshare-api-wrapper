# RetroShareOpenApiWrapper.RespRsGxsChannelsGetChannelsInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**channelsInfo** | [**[RsGxsChannelGroup]**](RsGxsChannelGroup.md) |  | [optional] 


