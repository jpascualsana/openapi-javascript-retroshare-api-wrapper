# RetroShareOpenApiWrapper.RsIdentityDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mId** | **String** |  | [optional] 
**mNickname** | **String** |  | [optional] 
**mFlags** | **Number** |  | [optional] 
**mPgpId** | **String** |  | [optional] 
**mRecognTags** | [**[RsRecognTag]**](RsRecognTag.md) |  | [optional] 
**mReputation** | [**RsReputationInfo**](RsReputationInfo.md) |  | [optional] 
**mAvatar** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**mPublishTS** | **Number** |  | [optional] 
**mLastUsageTS** | **Number** |  | [optional] 
**mUseCases** | [**[RsIdentityDetailsMUseCases]**](RsIdentityDetailsMUseCases.md) |  | [optional] 


