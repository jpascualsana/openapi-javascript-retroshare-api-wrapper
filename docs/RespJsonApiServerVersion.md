# RetroShareOpenApiWrapper.RespJsonApiServerVersion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**major** | **Number** |  | [optional] 
**minor** | **Number** |  | [optional] 
**mini** | **Number** |  | [optional] 
**extra** | **String** |  | [optional] 
**human** | **String** |  | [optional] 


