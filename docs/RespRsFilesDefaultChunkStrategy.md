# RetroShareOpenApiWrapper.RespRsFilesDefaultChunkStrategy

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | [**FileChunksInfoChunkStrategy**](FileChunksInfoChunkStrategy.md) |  | [optional] 


