# RetroShareOpenApiWrapper.ReqRsGxsForumsGetForumContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forumId** | **String** |  | [optional] 
**msgsIds** | **[String]** |  | [optional] 


