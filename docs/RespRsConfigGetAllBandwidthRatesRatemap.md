# RetroShareOpenApiWrapper.RespRsConfigGetAllBandwidthRatesRatemap

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | [optional] 
**value** | [**RsConfigDataRates**](RsConfigDataRates.md) |  | [optional] 


