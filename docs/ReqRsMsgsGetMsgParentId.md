# RetroShareOpenApiWrapper.ReqRsMsgsGetMsgParentId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 


