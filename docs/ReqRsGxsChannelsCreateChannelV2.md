# RetroShareOpenApiWrapper.ReqRsGxsChannelsCreateChannelV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**thumbnail** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**authorId** | **String** |  | [optional] 
**circleType** | [**RsGxsCircleType**](RsGxsCircleType.md) |  | [optional] 
**circleId** | **String** |  | [optional] 


