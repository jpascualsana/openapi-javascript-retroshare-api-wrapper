# RetroShareOpenApiWrapper.RespRsConfigGetConfigNetStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Number** |  | [optional] 
**status** | [**RsConfigNetStatus**](RsConfigNetStatus.md) |  | [optional] 


