# RetroShareOpenApiWrapper.RespRsMsgsGetMessageTagTypes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**tags** | [**RsMsgsMsgTagType**](RsMsgsMsgTagType.md) |  | [optional] 


