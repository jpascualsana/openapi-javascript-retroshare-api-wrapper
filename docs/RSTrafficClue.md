# RetroShareOpenApiWrapper.RSTrafficClue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TS** | **Number** |  | [optional] 
**size** | **Number** |  | [optional] 
**priority** | **Number** |  | [optional] 
**serviceId** | **Number** |  | [optional] 
**serviceSubId** | **Number** |  | [optional] 
**peerId** | **String** |  | [optional] 
**count** | **Number** |  | [optional] 


