# RetroShareOpenApiWrapper.RsGxsImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mSize** | **Number** |  | [optional] 
**mData** | **String** |  | [optional] 


