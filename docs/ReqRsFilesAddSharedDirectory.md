# RetroShareOpenApiWrapper.ReqRsFilesAddSharedDirectory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dir** | [**SharedDirInfo**](SharedDirInfo.md) |  | [optional] 


