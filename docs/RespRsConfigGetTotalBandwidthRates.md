# RetroShareOpenApiWrapper.RespRsConfigGetTotalBandwidthRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Number** |  | [optional] 
**rates** | [**RsConfigDataRates**](RsConfigDataRates.md) |  | [optional] 


