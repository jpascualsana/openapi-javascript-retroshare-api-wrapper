# RetroShareOpenApiWrapper.RespRsGxsChannelsCreateChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**channel** | [**RsGxsChannelGroup**](RsGxsChannelGroup.md) |  | [optional] 


