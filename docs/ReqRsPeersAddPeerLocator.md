# RetroShareOpenApiWrapper.ReqRsPeersAddPeerLocator

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] 
**locator** | [**RsUrl**](RsUrl.md) |  | [optional] 


