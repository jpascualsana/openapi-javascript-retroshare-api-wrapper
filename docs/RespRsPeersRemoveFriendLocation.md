# RetroShareOpenApiWrapper.RespRsPeersRemoveFriendLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


