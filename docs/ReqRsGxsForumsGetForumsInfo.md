# RetroShareOpenApiWrapper.ReqRsGxsForumsGetForumsInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forumIds** | **[String]** |  | [optional] 


