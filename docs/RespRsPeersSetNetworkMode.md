# RetroShareOpenApiWrapper.RespRsPeersSetNetworkMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


