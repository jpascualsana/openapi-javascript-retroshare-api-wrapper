# RetroShareOpenApiWrapper.RsConfigNetStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ownId** | **String** |  | [optional] 
**ownName** | **String** |  | [optional] 
**localAddr** | **String** |  | [optional] 
**localPort** | **Number** |  | [optional] 
**extAddr** | **String** |  | [optional] 
**extPort** | **Number** |  | [optional] 
**extDynDns** | **String** |  | [optional] 
**firewalled** | **Boolean** |  | [optional] 
**forwardPort** | **Boolean** |  | [optional] 
**dHTActive** | **Boolean** |  | [optional] 
**uPnPActive** | **Boolean** |  | [optional] 
**uPnPState** | **Number** |  | [optional] 
**netLocalOk** | **Boolean** |  | [optional] 
**netUpnpOk** | **Boolean** |  | [optional] 
**netDhtOk** | **Boolean** |  | [optional] 
**netStunOk** | **Boolean** |  | [optional] 
**netExtAddressOk** | **Boolean** |  | [optional] 
**netDhtNetSize** | **Number** |  | [optional] 
**netDhtRsNetSize** | **Number** |  | [optional] 


