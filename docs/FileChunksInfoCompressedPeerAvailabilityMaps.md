# RetroShareOpenApiWrapper.FileChunksInfoCompressedPeerAvailabilityMaps

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | [optional] 
**value** | [**CompressedChunkMap**](CompressedChunkMap.md) |  | [optional] 


