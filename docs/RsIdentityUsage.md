# RetroShareOpenApiWrapper.RsIdentityUsage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mServiceId** | [**RsServiceType**](RsServiceType.md) |  | [optional] 
**mUsageCode** | [**UsageCode**](UsageCode.md) |  | [optional] 
**mGrpId** | **String** |  | [optional] 
**mMsgId** | **String** |  | [optional] 
**mAdditionalId** | **Number** |  | [optional] 
**mComment** | **String** |  | [optional] 
**mHash** | **String** |  | [optional] 


