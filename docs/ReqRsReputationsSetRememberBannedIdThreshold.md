# RetroShareOpenApiWrapper.ReqRsReputationsSetRememberBannedIdThreshold

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days** | **Number** |  | [optional] 


