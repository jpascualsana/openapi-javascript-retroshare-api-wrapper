# RetroShareOpenApiWrapper.RespRsGxsForumsCreateForumV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**forumId** | **String** |  | [optional] 
**errorMessage** | **String** |  | [optional] 


