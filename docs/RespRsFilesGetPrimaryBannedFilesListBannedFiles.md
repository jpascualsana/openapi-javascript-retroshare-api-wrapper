# RetroShareOpenApiWrapper.RespRsFilesGetPrimaryBannedFilesListBannedFiles

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | [optional] 
**value** | [**BannedFileEntry**](BannedFileEntry.md) |  | [optional] 


