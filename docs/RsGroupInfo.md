# RetroShareOpenApiWrapper.RsGroupInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**flag** | **Number** |  | [optional] 
**peerIds** | **[String]** |  | [optional] 


