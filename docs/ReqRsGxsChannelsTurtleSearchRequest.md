# RetroShareOpenApiWrapper.ReqRsGxsChannelsTurtleSearchRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matchString** | **String** |  | [optional] 
**maxWait** | **Number** |  | [optional] 


