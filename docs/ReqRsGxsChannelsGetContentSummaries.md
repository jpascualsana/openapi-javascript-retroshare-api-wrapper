# RetroShareOpenApiWrapper.ReqRsGxsChannelsGetContentSummaries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 


