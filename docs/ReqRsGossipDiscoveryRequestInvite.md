# RetroShareOpenApiWrapper.ReqRsGossipDiscoveryRequestInvite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inviteId** | **String** |  | [optional] 
**toSslId** | **String** |  | [optional] 


