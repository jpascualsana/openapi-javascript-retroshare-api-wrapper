# RetroShareOpenApiWrapper.RsMsgsMsgTagTypeTypes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **Number** |  | [optional] 
**value** | [**RsMsgsMsgTagTypeValue**](RsMsgsMsgTagTypeValue.md) |  | [optional] 


