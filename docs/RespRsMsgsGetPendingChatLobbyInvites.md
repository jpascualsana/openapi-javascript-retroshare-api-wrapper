# RetroShareOpenApiWrapper.RespRsMsgsGetPendingChatLobbyInvites

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invites** | [**[ChatLobbyInvite]**](ChatLobbyInvite.md) |  | [optional] 


