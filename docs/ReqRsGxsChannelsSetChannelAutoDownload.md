# RetroShareOpenApiWrapper.ReqRsGxsChannelsSetChannelAutoDownload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 
**enable** | **Boolean** |  | [optional] 


