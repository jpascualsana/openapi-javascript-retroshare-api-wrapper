# RetroShareOpenApiWrapper.RespRsBanListIpFilteringEnabled

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


