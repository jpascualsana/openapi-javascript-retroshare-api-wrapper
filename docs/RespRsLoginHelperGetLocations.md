# RetroShareOpenApiWrapper.RespRsLoginHelperGetLocations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locations** | [**[RsLoginHelperLocation]**](RsLoginHelperLocation.md) |  | [optional] 


