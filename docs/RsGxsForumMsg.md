# RetroShareOpenApiWrapper.RsGxsForumMsg

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] 
**mMsg** | **String** |  | [optional] 


