# RetroShareOpenApiWrapper.ReqRsGxsChannelsCreateComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | [**RsGxsComment**](RsGxsComment.md) |  | [optional] 


