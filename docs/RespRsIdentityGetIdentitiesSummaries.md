# RetroShareOpenApiWrapper.RespRsIdentityGetIdentitiesSummaries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**ids** | [**[RsGroupMetaData]**](RsGroupMetaData.md) |  | [optional] 


