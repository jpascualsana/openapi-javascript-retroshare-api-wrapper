# RetroShareOpenApiWrapper.RespRsGxsForumsCreatePost

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**postMsgId** | **String** |  | [optional] 
**errorMessage** | **String** |  | [optional] 


