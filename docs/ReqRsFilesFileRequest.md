# RetroShareOpenApiWrapper.ReqRsFilesFileRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileName** | **String** |  | [optional] 
**hash** | **String** |  | [optional] 
**size** | **Number** |  | [optional] 
**destPath** | **String** |  | [optional] 
**flags** | **Number** |  | [optional] 
**srcIds** | **[String]** |  | [optional] 


