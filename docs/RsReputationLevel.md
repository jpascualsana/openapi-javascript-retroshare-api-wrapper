# RetroShareOpenApiWrapper.RsReputationLevel

## Enum


* `LOCALLY_NEGATIVE` (value: `0`)

* `REMOTELY_NEGATIVE` (value: `1`)

* `NEUTRAL` (value: `2`)

* `REMOTELY_POSITIVE` (value: `3`)

* `LOCALLY_POSITIVE` (value: `4`)

* `UNKNOWN` (value: `5`)


