# RetroShareOpenApiWrapper.FileChunksInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileSize** | **Number** |  | [optional] 
**chunkSize** | **Number** |  | [optional] 
**strategy** | [**FileChunksInfoChunkStrategy**](FileChunksInfoChunkStrategy.md) |  | [optional] 
**chunks** | [**[ChunkState]**](ChunkState.md) |  | [optional] 
**compressedPeerAvailabilityMaps** | [**[FileChunksInfoCompressedPeerAvailabilityMaps]**](FileChunksInfoCompressedPeerAvailabilityMaps.md) |  | [optional] 
**activeChunks** | [**[FileChunksInfoActiveChunks]**](FileChunksInfoActiveChunks.md) |  | [optional] 
**pendingSlices** | [**[FileChunksInfoPendingSlices]**](FileChunksInfoPendingSlices.md) |  | [optional] 


