# RetroShareOpenApiWrapper.ReqRsFilesFileUploadChunksDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**peerId** | **String** |  | [optional] 


