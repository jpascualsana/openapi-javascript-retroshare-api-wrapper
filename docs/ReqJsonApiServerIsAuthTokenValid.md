# RetroShareOpenApiWrapper.ReqJsonApiServerIsAuthTokenValid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  | [optional] 


