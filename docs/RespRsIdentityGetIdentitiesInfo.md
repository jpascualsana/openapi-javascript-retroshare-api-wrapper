# RetroShareOpenApiWrapper.RespRsIdentityGetIdentitiesInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**idsInfo** | [**[RsGxsIdGroup]**](RsGxsIdGroup.md) |  | [optional] 


