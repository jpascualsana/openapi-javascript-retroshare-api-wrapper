# RetroShareOpenApiWrapper.ReqRsFilesSetDestinationName

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  | [optional] 
**newName** | **String** |  | [optional] 


