# RetroShareOpenApiWrapper.ReqRsMsgsSystemMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**systemFlag** | **Number** |  | [optional] 


