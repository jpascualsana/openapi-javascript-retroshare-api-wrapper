# RetroShareOpenApiWrapper.ReqRsGxsForumsCreateMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | [**RsGxsForumMsg**](RsGxsForumMsg.md) |  | [optional] 


