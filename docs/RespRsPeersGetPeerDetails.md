# RetroShareOpenApiWrapper.RespRsPeersGetPeerDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**det** | [**RsPeerDetails**](RsPeerDetails.md) |  | [optional] 


