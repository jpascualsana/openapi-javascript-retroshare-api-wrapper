# RetroShareOpenApiWrapper.RsMsgsMessageInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**rspeeridSrcId** | **String** |  | [optional] 
**rsgxsidSrcId** | **String** |  | [optional] 
**msgflags** | **Number** |  | [optional] 
**rspeeridMsgto** | **[String]** |  | [optional] 
**rspeeridMsgcc** | **[String]** |  | [optional] 
**rspeeridMsgbcc** | **[String]** |  | [optional] 
**rsgxsidMsgto** | **[String]** |  | [optional] 
**rsgxsidMsgcc** | **[String]** |  | [optional] 
**rsgxsidMsgbcc** | **[String]** |  | [optional] 
**title** | **String** |  | [optional] 
**msg** | **String** |  | [optional] 
**attachTitle** | **String** |  | [optional] 
**attachComment** | **String** |  | [optional] 
**files** | [**[FileInfo]**](FileInfo.md) |  | [optional] 
**size** | **Number** |  | [optional] 
**count** | **Number** |  | [optional] 
**ts** | **Number** |  | [optional] 


