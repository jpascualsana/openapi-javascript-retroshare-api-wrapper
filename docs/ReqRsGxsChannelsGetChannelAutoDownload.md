# RetroShareOpenApiWrapper.ReqRsGxsChannelsGetChannelAutoDownload

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] 


