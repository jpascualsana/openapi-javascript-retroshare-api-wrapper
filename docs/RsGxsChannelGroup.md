# RetroShareOpenApiWrapper.RsGxsChannelGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] 
**mDescription** | **String** |  | [optional] 
**mImage** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**mAutoDownload** | **Boolean** |  | [optional] 


