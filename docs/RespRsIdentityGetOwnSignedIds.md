# RetroShareOpenApiWrapper.RespRsIdentityGetOwnSignedIds

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**ids** | **[String]** |  | [optional] 


