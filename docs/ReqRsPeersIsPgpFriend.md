# RetroShareOpenApiWrapper.ReqRsPeersIsPgpFriend

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pgpId** | **String** |  | [optional] 


