# RetroShareOpenApiWrapper.RespRsGxsChannelsGetChannelsSummaries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**channels** | [**[RsGroupMetaData]**](RsGroupMetaData.md) |  | [optional] 


