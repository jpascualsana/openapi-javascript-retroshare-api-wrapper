# RetroShareOpenApiWrapper.RsGxsChannelPost

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] 
**mOlderVersions** | **[String]** |  | [optional] 
**mMsg** | **String** |  | [optional] 
**mFiles** | [**[RsGxsFile]**](RsGxsFile.md) |  | [optional] 
**mCount** | **Number** |  | [optional] 
**mSize** | **Number** |  | [optional] 
**mThumbnail** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 


