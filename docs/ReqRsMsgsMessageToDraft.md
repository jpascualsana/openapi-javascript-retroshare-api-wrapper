# RetroShareOpenApiWrapper.ReqRsMsgsMessageToDraft

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | [**RsMsgsMessageInfo**](RsMsgsMessageInfo.md) |  | [optional] 
**msgParentId** | **String** |  | [optional] 


