# RetroShareOpenApiWrapper.RespRsIdentityCreateIdentity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**id** | **String** |  | [optional] 


