# RetroShareOpenApiWrapper.ReqRsGxsCirclesGetCircleRequests

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**circleId** | **String** |  | [optional] 


