# RetroShareOpenApiWrapper.ReqRsMsgsMessageSend

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | [**RsMsgsMessageInfo**](RsMsgsMessageInfo.md) |  | [optional] 


