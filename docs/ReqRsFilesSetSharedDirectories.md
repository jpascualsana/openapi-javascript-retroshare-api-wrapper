# RetroShareOpenApiWrapper.ReqRsFilesSetSharedDirectories

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dirs** | [**[SharedDirInfo]**](SharedDirInfo.md) |  | [optional] 


