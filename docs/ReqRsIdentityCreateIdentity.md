# RetroShareOpenApiWrapper.ReqRsIdentityCreateIdentity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**avatar** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] 
**pseudonimous** | **Boolean** |  | [optional] 
**pgpPassword** | **String** |  | [optional] 


