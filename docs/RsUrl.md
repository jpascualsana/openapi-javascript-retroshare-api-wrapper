# RetroShareOpenApiWrapper.RsUrl

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schemeSeparator** | **String** |  | [optional] 
**ipv6WrapOpen** | **String** |  | [optional] 
**ipv6Separator** | **String** |  | [optional] 
**ipv6WrapClose** | **String** |  | [optional] 
**portSeparator** | **String** |  | [optional] 
**pathSeparator** | **String** |  | [optional] 
**querySeparator** | **String** |  | [optional] 
**queryAssign** | **String** |  | [optional] 
**queryFieldSep** | **String** |  | [optional] 
**fragmentSeparator** | **String** |  | [optional] 


