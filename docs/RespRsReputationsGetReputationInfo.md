# RetroShareOpenApiWrapper.RespRsReputationsGetReputationInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**info** | [**RsReputationInfo**](RsReputationInfo.md) |  | [optional] 


