# RetroShareOpenApiWrapper.ReqRsGxsChannelsCreateChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel** | [**RsGxsChannelGroup**](RsGxsChannelGroup.md) |  | [optional] 


