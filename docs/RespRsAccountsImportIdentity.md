# RetroShareOpenApiWrapper.RespRsAccountsImportIdentity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**pgpId** | **String** |  | [optional] 
**errorMsg** | **String** |  | [optional] 


