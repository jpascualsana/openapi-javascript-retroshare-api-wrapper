# RetroShareOpenApiWrapper.RsLoginHelperLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mLocationId** | **String** |  | [optional] 
**mPgpId** | **String** |  | [optional] 
**mLocationName** | **String** |  | [optional] 
**mPpgName** | **String** |  | [optional] 


