# RetroShareOpenApiWrapper.ChatLobbyInvite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 
**peerId** | **String** |  | [optional] 
**lobbyName** | **String** |  | [optional] 
**lobbyTopic** | **String** |  | [optional] 
**lobbyFlags** | **Number** |  | [optional] 


