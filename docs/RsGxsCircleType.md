# RetroShareOpenApiWrapper.RsGxsCircleType

## Enum


* `UNKNOWN` (value: `0`)

* `PUBLIC` (value: `1`)

* `EXTERNAL` (value: `2`)

* `NODES_GROUP` (value: `3`)

* `LOCAL` (value: `4`)

* `EXT_SELF` (value: `5`)

* `YOUR_EYES_ONLY` (value: `6`)


