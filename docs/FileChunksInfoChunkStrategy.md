# RetroShareOpenApiWrapper.FileChunksInfoChunkStrategy

## Enum


* `CHUNK_STRATEGY_STREAMING` (value: `0`)

* `CHUNK_STRATEGY_RANDOM` (value: `1`)

* `CHUNK_STRATEGY_PROGRESSIVE` (value: `2`)


