# RetroShareOpenApiWrapper.VisibleChatLobbyRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | **Number** |  | [optional] 
**lobbyName** | **String** |  | [optional] 
**lobbyTopic** | **String** |  | [optional] 
**participatingFriends** | **[String]** |  | [optional] 
**totalNumberOfPeers** | **Number** |  | [optional] 
**lastReportTime** | **Number** |  | [optional] 
**lobbyFlags** | **Number** |  | [optional] 


