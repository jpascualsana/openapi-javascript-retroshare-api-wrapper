# RetroShareOpenApiWrapper.RespRsGxsForumsRequestStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | [**RsTokenServiceGxsRequestStatus**](RsTokenServiceGxsRequestStatus.md) |  | [optional] 


