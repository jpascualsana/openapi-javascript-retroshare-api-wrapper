# RetroShareOpenApiWrapper.RespRsFilesFileDownloadChunksDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**info** | [**FileChunksInfo**](FileChunksInfo.md) |  | [optional] 


