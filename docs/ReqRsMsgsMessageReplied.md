# RetroShareOpenApiWrapper.ReqRsMsgsMessageReplied

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**replied** | **Boolean** |  | [optional] 


