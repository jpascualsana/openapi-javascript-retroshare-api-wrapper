# RetroShareOpenApiWrapper.RsPeerServiceInfoMServiceList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **Number** |  | [optional] 
**value** | [**RsServiceInfo**](RsServiceInfo.md) |  | [optional] 


