# RetroShareOpenApiWrapper.RespRsGxsCirclesCancelCircleMembership

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


