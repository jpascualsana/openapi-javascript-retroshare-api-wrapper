# RetroShareOpenApiWrapper.ReqRsMsgsGetMessageTag

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 


