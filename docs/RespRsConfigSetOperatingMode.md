# RetroShareOpenApiWrapper.RespRsConfigSetOperatingMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


