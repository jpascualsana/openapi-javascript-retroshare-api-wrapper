# RetroShareOpenApiWrapper.RespRsGxsForumsGetForumMsgMetaData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 
**msgMetas** | [**[RsMsgMetaData]**](RsMsgMetaData.md) |  | [optional] 


