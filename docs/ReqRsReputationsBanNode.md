# RetroShareOpenApiWrapper.ReqRsReputationsBanNode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**b** | **Boolean** |  | [optional] 


