# RetroShareOpenApiWrapper.FileChunksInfoSliceInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **Number** |  | [optional] 
**size** | **Number** |  | [optional] 
**peerId** | **String** |  | [optional] 


