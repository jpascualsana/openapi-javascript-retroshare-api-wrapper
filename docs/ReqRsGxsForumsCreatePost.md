# RetroShareOpenApiWrapper.ReqRsGxsForumsCreatePost

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forumId** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**mBody** | **String** |  | [optional] 
**authorId** | **String** |  | [optional] 
**parentId** | **String** |  | [optional] 
**origPostId** | **String** |  | [optional] 


