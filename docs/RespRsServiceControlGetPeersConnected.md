# RetroShareOpenApiWrapper.RespRsServiceControlGetPeersConnected

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peerSet** | **[String]** |  | [optional] 


