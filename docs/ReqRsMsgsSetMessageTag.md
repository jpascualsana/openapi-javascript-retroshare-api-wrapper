# RetroShareOpenApiWrapper.ReqRsMsgsSetMessageTag

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] 
**tagId** | **Number** |  | [optional] 
**set** | **Boolean** |  | [optional] 


