# RetroShareOpenApiWrapper.RespJsonApiServerRevokeAuthToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **Boolean** |  | [optional] 


