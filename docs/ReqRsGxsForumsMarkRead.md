# RetroShareOpenApiWrapper.ReqRsGxsForumsMarkRead

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messageId** | [**RsGxsGrpMsgIdPair**](RsGxsGrpMsgIdPair.md) |  | [optional] 
**read** | **Boolean** |  | [optional] 


