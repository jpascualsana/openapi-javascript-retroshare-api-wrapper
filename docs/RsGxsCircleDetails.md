# RetroShareOpenApiWrapper.RsGxsCircleDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mCircleId** | **String** |  | [optional] 
**mCircleName** | **String** |  | [optional] 
**mCircleType** | **Number** |  | [optional] 
**mRestrictedCircleId** | **String** |  | [optional] 
**mAmIAllowed** | **Boolean** |  | [optional] 
**mAllowedGxsIds** | **[String]** |  | [optional] 
**mAllowedNodes** | **[String]** |  | [optional] 
**mSubscriptionFlags** | [**[RsGxsCircleDetailsMSubscriptionFlags]**](RsGxsCircleDetailsMSubscriptionFlags.md) |  | [optional] 


